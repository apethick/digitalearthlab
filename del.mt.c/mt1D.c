#include <stdio.h>      /* Standard Library of Input and Output */
#include <stdlib.h>
#include <complex.h>
#include <math.h>
#include <string.h>
#include <unistd.h>
int main(int argc, char *argv[])
{
	FILE* earthf;
    FILE* dataf;
    FILE* datas;
    FILE* datalog = fopen("log.txt", "w");;
    
	if(argc == 1) {
		earthf= fopen("earth.txt", "r");
	    dataf = fopen("data.txt", "r");
	    datas = fopen("data_synth.txt", "w");
	    fprintf(datalog,"Earth File %s\n","earth.txt");
	    fprintf(datalog,"Data File %s\n","data.txt");
	    fprintf(datalog,"Data Out Synthetic File %s\n","data_synth.txt");
	    fclose(datalog);
	} else {
		char* filenameEF = malloc(strlen(argv[1])+9);
		char* filenameDF = malloc(strlen(argv[1])+8);
		char* filenameDS = malloc(strlen(argv[1])+14);
		strcpy(filenameEF,argv[1]);
		strcpy(filenameDF,argv[1]);
		strcpy(filenameDS,argv[1]);
		strcat(filenameEF,"earth.txt");
		strcat(filenameDF,"data.txt");
		strcat(filenameDS,"data_synth.txt");
		
	    earthf= fopen(filenameEF, "r");
	    dataf = fopen(filenameDF, "r");
	    datas = fopen(filenameDS, "w");
		fprintf(datalog,"Earth File %s\n",filenameEF);
	    fprintf(datalog,"Data File %s\n",filenameDF);
	    fprintf(datalog,"Data Out Synthetic File %s\n",filenameDS);
	    fclose(datalog);
	}
	

    
 



    //IMPORT EARTH
    int nlayers;
    fscanf(earthf, "%d", &nlayers);
    printf("Number of Layers Found : %ld\n",nlayers);
    int i = 0;
    float resistivities[nlayers];
    float thicknesses[nlayers-1];
    for (i = 0; i < nlayers; i++)
    {
		if(i != nlayers - 1) {
			fscanf(earthf, "%f\t%f", &resistivities[i], &thicknesses[i]);
			printf("%ld:\t%lf\t%lf\n",i, thicknesses[i],resistivities[i]);
		} else {
			fscanf(earthf, "%f\t%f", &resistivities[i]);
			printf("%ld:\t%lf\n",i, resistivities[i]);
		}
		
    }

    //IMPORT DATA
    int ndat;
    fscanf(dataf, "%d\n", &ndat);
    printf("Number of Data Points Found : %ld\n",ndat);
    float frequencies[ndat];
    float aresxy[ndat];
    float aphasexy[ndat];
    float aresyx[ndat];
    float aphaseyx[ndat];
    char line [ 512 ]; /* or other suitable maximum line size */
    int nth = 0;
    while ( fgets ( line, sizeof line, dataf ) != NULL ) /* read a line */
    {
    	float freq = -1;
    	float resxy = -1;
    	float phasexy = -1;
    	float resyx = -1;
    	float phaseyx = -1;
    	sscanf(line,"%f\t%f\t%f\t%f\t%f", &freq, &resxy, &phasexy, &resyx, &phaseyx);
    	//printf(line);

    	frequencies[nth] = freq;
    	aresxy[nth] = resxy;
    	aphasexy[nth] = phasexy;
    	aresyx[nth] = resyx;
    	aphaseyx[nth] = phaseyx;
    	//printf("%f\t%f\t%f\t%f\t%f\n", &frequencies[nth], &aresxy[nth], &aphasexy[nth], &aresyx[nth], &aphaseyx[nth]);
    	nth++;
    	//printf("%f\n",frequencies[nth-1]);

    }

    //printf("%d Frequency : %f Hz\n",1,&frequencies[1]);
        //compute model

    float complex j = 1.0 * I;
    float mu = 4*M_PI*1E-7;
    float frequency;
    fprintf(datas, "%d\n", ndat);
    for (i = 0 ; i < ndat ; i++) {
        //printf("%d Frequency : %f Hz\n",i,frequencies[i]);
    	frequency = frequencies[i];

    	float w =  2* M_PI *frequency;
        float complex impedances [nlayers];
        //compute basement impedance
        impedances[nlayers-1] = (csqrt(w*mu*resistivities[nlayers-1]*j));



        float resistivity;
        float thickness;
        int l;
        for (l = nlayers-2 ; l >= 0 ; l--) {
        	resistivity = resistivities[l];
        	thickness = thicknesses[l];

        	//printf("\t%d Res   : %f \n",i,resistivities[l]);
        	//printf("\t%d Thick : %f \n",i,thicknesses[l]);
        	// 3. Compute apparent resistivity from top layer impedance
        	//Step 2. Iterate from bottom layer to top(not the basement)
        	// Step 2.1 Calculate the intrinsic impedance of current layer
        	float complex dl = csqrt((w * mu * (1.0/resistivity))*j);
        	float complex wl = dl * resistivity;
        	// Step 2.2 Calculate Exponential factor from intrinsic impedance
        	float complex el = cexp(-2*thickness*dl);
        	// Step 2.3 Calculate reflection coeficient using current layer
        	//          intrinsic impedance and the below layer impedance
        	float complex belowImpedance = impedances[l + 1];
        	float complex rl = (wl - belowImpedance)/(wl + belowImpedance);
        	float complex re = rl*el;
        	float complex Zl = wl * ((1 - re)/(1 + re));
        	impedances[l] = Zl;

        }
    	// Step 3. Compute apparent resistivity from top layer impedance
    	float complex Z = impedances[0];
    	float absZ = cabs(Z);
    	float apparentResistivity = (absZ * absZ)/(mu * w);
    	float phase = 360*(atan2(cimag(Z), creal(Z)))/(2*M_PI);

    	fprintf(datas, "%f\t%f\t%f\t%f\t%f\t%f\n", frequency, apparentResistivity, phase, apparentResistivity, phase, absZ);

    }





    fclose(earthf);
    fclose(dataf);
    fclose(datas);
}
