package del.utils.perpetualfileapp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.PauseTransition;
import javafx.animation.SequentialTransition;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextArea;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

public class PerpetualFileViewerPane extends StackPane{
	/*
	 * Licensed under WTFPL
	 * Digital Earth Lab 2014
	 * visit digitalearthlab.com for more details
	 *
	 * Contributors:
	 * Andrew Pethick
	 */

	public final TextArea t = new TextArea();
	Label label = new Label();
	private File f;
	private BufferedReader in;
	private int preload, bufferSize;	//preload 'preload' lines and load bufferSize number of subsequent lines on request 
	public ArrayList<String> buffer = new ArrayList<String>();
	public int bufferIndexStart, bufferIndexEnd;
	
	
	ProgressIndicator indicator = new ProgressIndicator(-1.0);
	Button loadDocument = new Button("LOAD");
	
	private boolean dataFinished = false;
	
	public PerpetualFileViewerPane(int preload, int bufferSize) {
		this.bufferSize = bufferSize;
		this.preload = preload;
		this.setMaxSize(Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE);

		StackPane.setAlignment(t, Pos.CENTER);
		StackPane.setMargin(t, new Insets(0));
		StackPane.setAlignment(indicator, Pos.BOTTOM_CENTER);
		StackPane.setAlignment(loadDocument, Pos.BOTTOM_RIGHT);
		indicator.setMaxSize(25, 25);
		this.getChildren().add(t);
		this.getChildren().add(indicator);
		this.getChildren().add(loadDocument);
		
		t.setEditable(false); //no editing, read only for now.
		
		addUpdateListener();


		loadDocument.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent arg0) {
				loadAll();
			}

			
		});
	}
	
	
	public void load(File f) throws Exception{
		this.f = f;		
		this.dataFinished = false;
		BufferedReader in = new BufferedReader(new FileReader(f));
		this. in = in;
		preload();
	}
	



	private void preload() throws Exception{
		Platform.runLater(new Thread(new Runnable() {
			
			@Override
			public void run() {
				showIndicators();
				appendLine(bufferSize);
				hideIndicators();
				t.positionCaret(0);
			}
		}));
		
	}
	private void appendLine(int n) {
		try {
				String append = "";
				
				for(int i = 0 ; i < n ; i++){					
					String line = in.readLine();				
					if(line == null) {
						dataFinished  = true;
						in.close();
						i = n;
						break;
					}
					else append += (line + "\n");
				}
				
				t.appendText(append);
			} catch (IOException e) {
				e.printStackTrace();
			}

	}
	private void loadAll() {
		loadDocument.setText("Loading: " + getFileSizeString());
		t.positionCaret(t.getLength());
		int pos = new Integer(t.getCaretPosition());
		showIndicators();
		appendLine(Integer.MAX_VALUE);
		hideIndicators();
		t.positionCaret(pos);
	}
	private void loadNextData() {
	if(!dataFinished)
	Platform.runLater(new Thread(new Runnable() {
			
			@Override
			public void run() {
				t.positionCaret(t.getLength());
				int pos = new Integer(t.getCaretPosition());
				showIndicators();
				appendLine(preload);		
				hideIndicators();
				t.positionCaret(pos);
			}


		}));
		
	}
	public void addUpdateListener() {
		
		t.setOnScroll(new EventHandler<ScrollEvent>() {

			@Override
			public void handle(ScrollEvent event) {
		
				if(event.getDeltaY() < 0) {
					loadNextData();
				}
			}

			
		});
//		System.out.println(t);
//		ScrollPane scrollPane = (ScrollPane) t.lookup(".scroll-pane");
//		System.out.println(scrollPane);
//		scrollPane.vvalueProperty().addListener(new ChangeListener<Number>() {
//
//			@Override
//			public void changed(ObservableValue<? extends Number> observable,	Number oldValue, Number newValue) {
//				System.out.println("WHA?");
//			};
//		});
//		label.textProperty().bind(Bindings.format("Vertical scroll at %.3f", scrollPane.vvalueProperty()));  
	}

	private void hideIndicators() {
		
		FadeTransition t = new FadeTransition(new Duration(1000),indicator);		
		t.setFromValue(1.0);
		t.setToValue(0.0);
		

		PauseTransition pt = new PauseTransition(new Duration(2000));
		FadeTransition lt = new FadeTransition(new Duration(1000),loadDocument);		
//		lt.setFromValue(1.0);
		lt.setToValue(0.0);
		
		SequentialTransition t2 = new SequentialTransition(pt,lt);
		ParallelTransition transition = new ParallelTransition(t,t2);
		transition.play();
	}

	private void showIndicators() {
		FadeTransition t = new FadeTransition(new Duration(1000),indicator);
		FadeTransition t2 = new FadeTransition(new Duration(1000),loadDocument);
		loadDocument.setText("Load: " + getFileSizeString());
//		t.setFromValue(0.0);
		t.setToValue(1.0);
		t2.setToValue(1.0);
		ParallelTransition transition = new ParallelTransition(t,t2);
		transition.play();
	}
	 
   DecimalFormat df = new DecimalFormat("#.##");
     
	private String getFileSizeString() {
		double size = ((double) f.length())/1024/1024;		
		String s = df.format(size) + "MBytes";
		return s;
	}



	
}
