package del.utils.perpetualfileapp;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

public class CreateLargeFile {
	public static void main(String[] args) throws Exception{
		BufferedWriter out = new BufferedWriter(new FileWriter(new File("Large_geophysical_dataset.dat")));
		out.write("x\ty\tz\tC1\tC2\tC3\tC4\tC5\tC6\n");
		
		for(int i = 0 ; i < 1E7 ; i++) {
			if(i%10000 == 0) System.out.println("Line " + i);
			out.write((i%10)*10 + "\t" + ((int) (i/10)*100) + "\t" + Math.random() * 100+ "\t" + Math.random() + "\t" + Math.random() + "\t" + Math.random() + "\t" + Math.random() + "\t" + Math.random() +"\n");			
		}
		out.close();
	}
}
