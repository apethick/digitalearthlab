package del.utils.perpetualfileapp;
	
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javax.swing.JFileChooser;

/*
 * Licensed under WTFPL
 * Digital Earth Lab 2014
 * visit digitalearthlab.com for more details
 *
 * Contributors:
 * Andrew Pethick
 */

public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		
		try { 
			int [] props = loadProperties();
			int initial = props[0];
			int buffer = props[1];				
			PerpetualFileViewerPane viewer = new PerpetualFileViewerPane(initial, buffer);
			Scene scene = new Scene(viewer,600,800);
			scene.getStylesheets().add(getClass().getResource("metroStyleLight.css").toExternalForm());
			primaryStage.setScene(scene);
			
			ArrayList<File> files = filechooser(true, false);
			viewer.load(files.get(0));
			primaryStage.setTitle("PFV \u00a9 2014 - A. Pethick - Digital Earth Lab - " + files.get(0).getName());
			primaryStage.getIcons().add(new Image(this.getClass().getResourceAsStream("icon.png")));
			primaryStage.show();	
			
			
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	private int[] loadProperties() throws Exception{
	
			File propertiesFile = new File("props.properties");
			int initial = 100;
			int buffer = 50;
			if(!propertiesFile.exists()) { 
				propertiesFile.createNewFile();
				Properties properties = new Properties();
				properties.load(new FileInputStream(propertiesFile));
			
				properties.setProperty("preload.key", initial + "");
				properties.setProperty("buffer.key", buffer + "");
				
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(propertiesFile));
				properties.store(stream, "AMP - Digital Earth Lab Perpetual File Viewer");
				stream.close();
			} else {
				Properties properties = new Properties();
				properties.load(new FileInputStream(propertiesFile));
				try {
				initial = Integer.valueOf(properties.getProperty("preload.key"));
				buffer = Integer.valueOf(properties.getProperty("buffer.key"));
				} catch(Exception e) {
					properties.load(new FileInputStream(propertiesFile));				
					properties.setProperty("preload.key", initial + "");
					properties.setProperty("buffer.key", buffer + "");					
					BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(propertiesFile));
					properties.store(stream, "AMP - Digital Earth Lab Perpetual File Viewer");
					stream.close();
				}
			}
			return new int []{initial,buffer};
	
	}


	public ArrayList<File> filechooser(boolean isFile, boolean enableMultipleSelection)throws Exception {
		File propertiesFile = new File("props.properties");
		if(!propertiesFile.exists()) propertiesFile.createNewFile();
		Properties properties = new Properties();
		String path = null;
		String resourcekey = "omn.previous";

		try {
			properties.load(new FileInputStream(propertiesFile));
			path = getDefaultDir(resourcekey, properties);

			if (isFile) {
				FileChooser c = new FileChooser();
				String title = "Load ";
				title += "files";
				c.setTitle(title);
				if (path != null)
					c.setInitialDirectory(new File(path));
				ArrayList<File> list = new ArrayList<File>();
				if (enableMultipleSelection) {
					List<File> l = c.showOpenMultipleDialog(null);
					if (l == null)
						return list;
					for (File f : l) {
						list.add(f);
					}
				} else {
					File f = c.showOpenDialog(null);
					if (f == null)
						return list;
					list.add(f);
				}

				if (list.get(0) == null)
					return list;

				String dir = (list.get(0).isDirectory()) ? list.get(0).getCanonicalPath() : list.get(0).getParentFile().getCanonicalPath();

				properties.setProperty(resourcekey, dir);
				properties.setProperty(previouskey, dir);
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(propertiesFile));
				properties.store(stream, "Omnium File Chooser History");
				stream.close();
				return list;
			} else {
				ArrayList<File> list = new ArrayList<File>();
				if (enableMultipleSelection) {
					JFileChooser c = new JFileChooser(new File(path));
					c.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
					c.setMultiSelectionEnabled(true);
					int returnVal = c.showOpenDialog(null);
					if (returnVal == JFileChooser.APPROVE_OPTION) {
						for (File f : c.getSelectedFiles()) {
							list.add(f);
						}
					}
				} else {
					DirectoryChooser c = new DirectoryChooser();
					String title = "Load " + (enableMultipleSelection ? " Directories" : " Directory");
					c.setTitle(title);
					c.setInitialDirectory(new File(path));
					File f = c.showDialog(null);
					if (f == null)
						return list;
					list.add(f);
				}

				if (list.get(0) == null)
					return list;

				String dir = (list.get(0).isDirectory()) ? list.get(0).getCanonicalPath() : list.get(0).getParentFile().getCanonicalPath();

				properties.setProperty(resourcekey, dir);
				properties.setProperty(previouskey, dir);
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(propertiesFile));
				properties.store(stream, "Omnium File Chooser History");
				stream.close();

				return list;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new ArrayList<File>();

	}
	private static String previouskey = "previousdir";
	private static String defaultdir = null;

	private String getDefaultDir(String resourcekey, Properties properties) {

		if (properties.getProperty(resourcekey) != null) {

			if (new File(properties.getProperty(resourcekey)).exists())
				return properties.getProperty(resourcekey);
		}
		if (properties.getProperty(previouskey) != null) {
			if (new File(properties.getProperty(previouskey)).exists())
				return properties.getProperty(previouskey);
		}
		return defaultdir;
	}
	public static void main(String[] args) {
		launch(args);
	}
}
