	<?php
	/**
	 * Plugin Name: Digital Earth Lab 1D MT Forward Modelling
	 * Plugin URI: http://digitalearthlab.com
	 * Description: 1D Magnetotelluric Forward Modelling
	 * Version: 0.0
	 * Author: Andrew Pethick
	 * Author URI: http://digitalearthlab.com
	 * License: WTFPL
	 */
	 
	
	add_shortcode( 'python', 'initialise');
	
	    add_action('fwd','fwd');
	    add_action('scase','select_case');
	    add_action('run','run');
	
	    $response = "";
	   
	  function initialise( $attributes ){     
			ob_start();
			global $response;


	 	switch ($_REQUEST['forward']){
			case "Forward Model":
		
			break;
			case "Select Case 1":
			do_action('scase','1');
			
			break;
			case "Select Case 2":
			do_action('scase','2');
			break;
		}
	 	include("form.php");
	 	include("google_chart.php");
	
		do_action('run');	 
		
		$output_string=ob_get_contents();;
		ob_end_clean();
		return $output_string;	

	}
	
	function run() {
		$resistivities = filter_var($_POST['message_resistivities']);
		$thicknesses = filter_var($_POST['message_thicknesses']);
		$frequencies = '[0.0014,0.0017,0.0020,0.0023,0.0028,0.0034,0.0040,0.0046,0.0055,0.0067,0.0079,0.0092,0.0110,0.0134,0.0159,0.0183,0.0220,0.0269,0.0320,0.0370,0.0440,0.0540,0.0630,0.0730,0.0880,0.1070,0.1270,0.1460,0.1760,0.2150,0.2540,0.2930,0.3500,0.4300,0.5100,0.5900,0.7000,0.8600,1.0200,1.1700,1.4100,1.7200,2.0300,2.3400,2.8100,3.4000,4.1000,4.7000,5.6000,6.9000,8.1000,9.4000,11.2000,13.7000,16.2000,18.8000,22.5000,27.5000,33.0000,40.0000,49.0000,57.0000,66.0000,79.0000,97.0000,115.0000,132.0000,159.0000,194.0000,229.0000,265.0000,320.0000,390.0000,460.0000,530.0000,640.0000,780.0000,900.0000,1100.0000,1300.0000,1500.0000,1800.0000,2200.0000,2600.0000,3000.0000,3600.0000,4400.0000,5200.0000,6000.0000,7200.0000,8800.0000,10400.0000]';

	  	$string = $frequencies.' '.$resistivities.' '.$thicknesses;

	  	
	  	do_action('fwd',$string);		  
	}
	
	  
 	function select_case($input) {
		switch ($input){		
			case "1":
		     $_POST['message_resistivities'] = '[1,10,100]';
			 $_POST['message_thicknesses'] = '[100,500]';

			break;
			case "2":
		     $_POST['message_resistivities'] = '[1000,10,1000]';
			 $_POST['message_thicknesses'] = '[50,100]';

			break;
			case "3":
		     $_POST['message_resistivities'] = '[1.5,30,1.5]';
			 $_POST['message_thicknesses'] = '[1000,50]';

			break;			
		  }

	}

	
	function flip($arr){
	    $out = array();
	
	    foreach ($arr as $key => $subarr)
	    {
	            foreach ($subarr as $subkey => $subvalue)
	            {
	                 $out[$subkey][$key] = $subvalue;
	            }
	    }
	
	    return $out;
	}
	
	function fwd($string) {			
	     global  $mt_output;
	     $file = 'mt.py';
	     $output = shell_exec('python ' . __DIR__ . '/' . $file . ' ' . $string); 

		 preg_match_all("/\[(.*?)\]/", $output, $matches);
		 $freqs = array_map('floatval', explode(',',substr($matches[1][0],1)));
		 $ares = array_map('floatval', explode(',',$matches[1][1]));
		 $phases = array_map('floatval', explode(',',$matches[1][2]));
		 $freqdat = flip(array($freqs,$ares));
		
		 $freqVsAres = new GoogleChart;		 
		 $freqVsAres->type='lc';
		 $freqVsAres->SetImageSize(540,200);
		 $freqVsAres->SetEncode('simple');
		
		/* $freqVsAres->AddData($ares,'0077CC','','Apparent Resistivity');*/
		$freqVsAres->AddData($ares);

		 $minares = floor(min($ares));
		 $maxares = ceil(max($ares));
		 $dres = round((50*($maxares - $minares))/500);
		 $minfreqs = floor(min($freqs));
		 $maxfreqs = ceil(max($freqs));
		 $dfreqs = round((50*($maxfreqs - $minfreqs))/500);
		 $freqVsAres->AddChartColor('0077CC');		 
		 $freqVsAres->AddLineStyle(2,1,0);
		 $freqVsAres->AddDataLineStyle('0077CC',1,'0:1',3);
		 $freqVsAres->AddFillArea('B','E6F2FA7F',0,1);
		 $freqVsAres->AddAxisRange(min($ares),max($ares),$dres,0);
		$freqVsAres->AddAxisRange(min($freqs),max($freqs),$dfreqs,1);
		
		$freqVsAres->AddAxis('y,x');
		$freqVsAres->SetTitle('Log10 Apparent Resistivity (Ohm.m) versus Log10 Frequency (Hz)');
		$freqVsAres->SetGrid(round(100/11,2),round(100/7,2),1,3);

		 $freqVsPhase = new GoogleChart;		 
		 $freqVsPhase->type='lc';
		 $freqVsPhase->SetImageSize(540,200);
		 $freqVsPhase->SetEncode('simple');
		
		/* $freqVsAres->AddData($phases,'0077CC','','Apparent Resistivity');*/
		$freqVsPhase->AddData($phases);
		 $freqVsPhase->AddChartColor('0077CC');		 

		 $freqVsPhase->AddDataLineStyle('0077CC',1,'0:1',3);
		 $freqVsPhase->AddFillArea('B','E6F2FA7F',0,1);
		 $freqVsPhase->AddAxisRange(min($phases),max($phases),$dres,0);
		  $freqVsPhase->AddAxisRange(min($freqs),max($freqs),$dfreqs,1);
		
		$freqVsPhase->AddAxis('y,x');
		$freqVsPhase->SetTitle('Phase (Rad) versus Log10 Frequency (Hz)');
		$freqVsPhase->SetGrid(round(100/11,2),round(100/7,2),1,3);


		 echo '<h1>Graphs</h1>';
		 echo $freqVsAres->GetImg();
		 echo $freqVsPhase->GetImg();				 
		
		 echo '<br><br><h1>Raw Output Data</h1><table  style="width:100%"><tr><td>Frequency  </td><td>A.Res.</td><td>Phase(Rad)</td></tr>';

		 for ($i = 0; $i < count($freqs); ++$i) {
      		$f = $freqs[$i];
      		$a = $ares[$i];
      		$p = $phases[$i];
      		 echo '<tr><td>';
			 echo pow(10,$f);
			 echo '</td><td>';
			 echo pow(10,$a);
			 echo '</td><td>';
			 echo $p;
			 echo '</td></tr>';
    	 }
		
 		echo '</table>';
		 

 	}
