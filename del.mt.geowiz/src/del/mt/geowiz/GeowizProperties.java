package del.mt.geowiz;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class GeowizProperties {
    public final static Properties props = loadProperties();

	private static Properties loadProperties() {			
		
	    File f = new File("resources.properties");
	    Properties p = new Properties();
	    try {
	    	p.load(new FileInputStream(f));
	    } catch(Exception e) {
	    	e.printStackTrace();
	    }
	    return p;
	}
    
    public final static String SCRATCH = props.getProperty("scratch");
    
}
