package del.mt.geowiz;

import java.awt.Desktop;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;

import javax.imageio.ImageIO;

import del.mt.geowiz.dat.Data;
import del.mt.geowiz.dat.DataInstanceNode;
import del.mt.geowiz.dat.DataView;
import del.mt.geowiz.dat.Utils;
import del.mt.geowiz.dev.DevelopersCustomFunctions;
import del.mt.geowiz.fontawesome.FontAwesome;
import del.mt.geowiz.fwd.MTForward1D;
import del.mt.geowiz.fwd.MTForward1DJava;
import del.mt.geowiz.fwd.MTForward1DMT1DC;
import del.mt.geowiz.fwd.MTForward1DPy;
import del.mt.geowiz.geoelectric.GeoelectricalEarth;
import del.mt.geowiz.geoelectric.Layer;
import del.mt.geowiz.geoelectric.LayerEntry;
import del.mt.geowiz.omnium.pictonic.Pictonic;
import del.mt.geowiz.opts.Options;
import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class MTApplication extends BorderPane {
	//set preferences for application
	Preferences prefs = Preferences.userRoot().node(getClass().getName());
	private final String LAST_USED_FOLDER = "datafolder.last";


	public static boolean IS_GUI_MODE = true;
	public static boolean IS_DEVELOPERS_MODE = true;

	public Options options = new Options(this);

	/*
	 * Data
	 */
	public final ObjectProperty<Data> activeData = new SimpleObjectProperty<Data>();
	public final ObjectProperty<Data> syntheticData = new SimpleObjectProperty<Data>();
	public final ObjectProperty<Data> rmsData = new SimpleObjectProperty<Data>();
	public ArrayList<Data> importedData = new ArrayList<>();

	public final ObjectProperty<LayerEntry> activeLayering = new SimpleObjectProperty<LayerEntry>();

	/*
	 * VISUAL
	 */
	public DataView dataView;
	public VBox dataBoxes = new VBox();


	private static final float DEFAULT_ICON_SIZE = 2.0f;


	TextField fileField = new TextField("resources\\TestData.txt");
	Button loadButton = new Button("Load Data");
	Button clearButton = new Button("Clear Data");
	Button runOccamButton = new Button("Run All OCCAM");

	TabPane tabpane = new TabPane();

	private Stage stage;
	SplitPane sp = new SplitPane();

	public IntegerProperty algorithm = new SimpleIntegerProperty(MTForward1DJava.ID);

	public BorderPane modelPane = new BorderPane();

	public MTApplication(Stage s) {
		this.stage = s;
		LayerEntry layering = new LayerEntry(this);
		this.activeLayering.set(layering);
		setLayeringPane();
		this.dataView = new DataView(this);

		tabpane.getTabs().add(createTab("Data", createDataPane(),Pictonic.CODEPEN_02(DEFAULT_ICON_SIZE)));
		tabpane.getTabs().add(createTab("Geo-Electrical Model", modelPane,FontAwesome.GLOBE()));
		tabpane.getTabs().add(createTab("Options", options.createOptionsPane(),FontAwesome.BARS(DEFAULT_ICON_SIZE)));
		tabpane.setPrefWidth(700);

		sp.getItems().addAll(tabpane, dataView.createViewPane());
		this.setCenter(sp);
		this.setTop(getMenuBar());
		addLayeringListener();


	}







	private void addLayeringListener() {
		this.activeLayering.addListener(new ChangeListener<LayerEntry>() {

			@Override
			public void changed(ObservableValue<? extends LayerEntry> observable, LayerEntry oldValue,
					LayerEntry newValue) {
				setLayeringPane();
			}

		});
	}

	public void setLayeringPane() {
		this.modelPane.setTop(createModelOptionsPane());
		this.modelPane.setCenter(createModelPane());
	}







	private Node createModelOptionsPane() {
		Menu opts = new Menu("Options");
		MenuItem setAllEarths = new MenuItem("Set Active to all earth models");
		MenuItem setAllSelectedEarths = new MenuItem("Set Active to all selected earth models");
		opts.getItems().add(setAllEarths);
		opts.getItems().add(setAllSelectedEarths);
		MenuBar bar = new MenuBar(opts);


		setAllEarths.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				ArrayList<DataInstanceNode> nodes = getDataInstances();
				DataInstanceNode.setAllEarths(nodes, activeLayering);
			}
		});
		setAllSelectedEarths.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				ArrayList<DataInstanceNode> nodes = getDataInstances();
				DataInstanceNode.setAllSelectedEarths(nodes, activeLayering);
			}
		});

		return bar;
	}

	public ArrayList<DataInstanceNode> getDataInstances() {
		ArrayList<DataInstanceNode> nodes = new ArrayList<>();
		for(Node n : dataBoxes.getChildren()) {
			if (n instanceof DataInstanceNode) {
				DataInstanceNode node = (DataInstanceNode) n;
				nodes.add(node);
			}
		}
		return nodes;
	}





	private Node getMenuBar() {
		MenuBar b = new MenuBar();
		Menu file = new Menu("File");
		Menu save = new Menu("Save");

		MenuItem saveEarth = new MenuItem("Save Earth");
		Menu load = new Menu("Load");
		MenuItem loadEarth = new MenuItem("Load Earth");

		Menu algorithm = new Menu("Algorithm");
		MenuItem algorithmJava = new MenuItem("MT1D Java");
		MenuItem algorithmPy = new MenuItem("MT1D Python (Future Addition)");
		MenuItem algorithmC = new MenuItem("MT1D C (Future Addition)");

		Menu developers = new Menu("Developers");
		MenuItem runDevelopersFunction = new MenuItem("Run Dev Function #1");


		b.getMenus().add(file);

		file.getItems().add(save);
		file.getItems().add(load);
		save.getItems().add(saveEarth);
		load.getItems().add(loadEarth);

		Menu export = new Menu("Export");
		MenuItem exportEarthAtIntervals = new MenuItem("Export Earth (interval 1m)");
		MenuItem exportDataCSV = new MenuItem("Export Data (CSV)");
		MenuItem exportDataPNG = new MenuItem("Export Data (PNG)");
		MenuItem exportEarthCSV = new MenuItem("Export Earth (CSV)");
		MenuItem exportEarthPNG = new MenuItem("Export Earth (PNG)");
		b.getMenus().add(export);
		export.getItems().add(exportDataCSV);
		export.getItems().add(exportDataPNG);
		export.getItems().add(exportEarthCSV);
		export.getItems().add(exportEarthPNG);
		export.getItems().add(exportEarthAtIntervals);

		algorithm.getItems().add(algorithmJava);
		algorithm.getItems().add(algorithmPy);
		algorithm.getItems().add(algorithmC);
		b.getMenus().add(algorithm);

		initLoadEarth(loadEarth);
		initSaveEarth(saveEarth);
		initExportDataCSV(exportDataCSV);
		initExportDataPNG(exportDataPNG);
		initExportEarthCSV(exportEarthCSV);
		initExportEarthPNG(exportEarthPNG);
		initExportEarthInter(exportEarthAtIntervals);
		initAlgorithm(algorithmJava,MTForward1DJava.ID);
		initAlgorithm(algorithmPy,MTForward1DPy.ID);
		initAlgorithm(algorithmC,MTForward1DMT1DC.ID);
		algorithmPy.setDisable(true);
		algorithmC.setDisable(true);


		if(IS_DEVELOPERS_MODE) {
			developers.getItems().add(runDevelopersFunction);
			b.getMenus().add(developers);
			DevelopersCustomFunctions.initDevFunc1(this, runDevelopersFunction);
		}
		return b;
	}


	private void initExportEarthInter(MenuItem exportEarthAtIntervals) {
		exportEarthAtIntervals.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				activeLayering.get().earth.saveIntervals();
			}
		});

	}







	private void initAlgorithm(MenuItem button, int id) {
		button.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				algorithm.set(id);
				activeLayering.get().refreshView();
			}
		});
	}



	private void initExportEarthPNG(MenuItem exportEarthPNG) {
		exportEarthPNG.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				activeLayering.get().earthView.exportPNG();
			}
		});
	}



	private void initExportEarthCSV(MenuItem exportEarthCSV) {
		exportEarthCSV.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				activeLayering.get().earth.exportCSV();
			}
		});
	}



	private void initExportDataPNG(MenuItem exportDataPNG) {
		exportDataPNG.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				BorderPane p = ((BorderPane) sp.getItems().get(1));
				WritableImage image = p.snapshot(new SnapshotParameters(), null);

			    File file = new File("data_"+GeoelectricalEarth.getCompactDate()+".png");

			    try {
			        ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
			        Desktop.getDesktop().open(file);
			    } catch (IOException e) {
			       e.printStackTrace();
			    }
			}
		});
	}



	private void initExportDataCSV(MenuItem exportDataCSV) {
		exportDataCSV.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				exportData();
			}


		});
	}

	private void exportData() {
		  File file = new File("data"+GeoelectricalEarth.getCompactDate()+".csv");

		    try {

				BufferedWriter out = new BufferedWriter(new FileWriter(file));
				double depth = 0;
				out.write("Frequency,Rho_xy,phi_xy,rho_yx,phi_yx,RhoSynth,PhiSynth");
				out.newLine();
				fwd();
//				layering.refreshView();
				Data field = activeData.get();
				Data synth = syntheticData.get();
				int N = synth.frequencies.size();
				for(int i = 0 ; i < N ; i++) {
					double freq = synth.frequencies.get(i);
					double rxys = field == null ? 0 : field.rhoXY.get(i);
					double ryxs = field == null ? 0 : field.rhoYX.get(i);
					double pxys = field == null ? 0 : field.phaseXY.get(i);
					double pyxs = field == null ? 0 : field.phaseYX.get(i);
					double af = synth == null ? 0 : synth.rhoXY.get(i);
					double pf = synth == null ? 0 : synth.phaseXY.get(i);
					out.write(freq + "," + rxys + "," + ryxs + "," + pxys + "," + pyxs + "," + af + "," + pf);
					out.newLine();
				}


				out.close();
				Desktop.getDesktop().open(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
	}

	private void initLoadEarth(MenuItem loadEarth) {
		loadEarth.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				activeLayering.get().earth.load();
			}
		});
	}
	private void initSaveEarth(MenuItem saveEarth) {
		saveEarth.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				activeLayering.get().earth.save();
			}
		});
	}





	private Node createInversionPane() {
		return null;
	}

	private Node createInversionSetupPane() {
		VBox b = new VBox();

		Button button = new Button("print");
		button.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {				;
				for (ArrayList<Double> l : getLayering()) {
					for (Double d : l) {
						System.out.print(d + "\t");
					}
					System.out.println();
				}

			}
		});
		b.getChildren().add(button);
		return b;
	}

	private Node createModelPane() {
		BorderPane b = new BorderPane();
		b.setCenter(activeLayering.get());
		return b;
	}

	public ArrayList<ArrayList<Double>> getLayering() {
		ArrayList<ArrayList<Double>> layering = new ArrayList<ArrayList<Double>>();

		for (Layer l : this.activeLayering.get().earth.layering) {
			ArrayList<Double> values = new ArrayList<Double>();
			double resType = l.getResistivity(options.UNITS_OHM_M);
			double thickType = l.getThickness(options.UNITS_M);

			values.add(resType);
			values.add(thickType);
			layering.add(values);
		}

		return layering;
	}




	private Tab createTab(String string, Node content, Node icon) {
		Tab t = new Tab(string);
		t.setContent(content);
		t.setClosable(false);
		t.setGraphic(icon);
		return t;
	}

	private Node createDataPane() {

		BorderPane p = new BorderPane();

		Button openButton = new Button("", FontAwesome.FOLDER_OPEN(DEFAULT_ICON_SIZE));
		loadButton.setMaxHeight(Double.MAX_VALUE);
		fileField.setMaxHeight(Double.MAX_VALUE);
		fileField.setMaxWidth(Double.MAX_VALUE);
//		clearButton.setMaxWidth(Double.MAX_VALUE);
		clearButton.setMaxHeight(Double.MAX_VALUE);
		runOccamButton.setMaxHeight(Double.MAX_VALUE);
//		runOccamButton.setWidth(Double.MAX_VALUE);
		HBox b = new HBox(fileField, openButton, loadButton);
		b.setMaxWidth(Double.MAX_VALUE);
		VBox vb = new VBox(b,clearButton,runOccamButton);

		p.setTop(vb);
		p.setCenter(new ScrollPane(dataBoxes));



		loadButton.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				loadData();
			}

		});
		clearButton.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				clearData();
			}


		});
		runOccamButton.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {

				for(Object o : dataBoxes.getChildren()) {


					Thread t= new Thread(new Runnable() {

						@Override
						public void run() {
							DataInstanceNode n = (DataInstanceNode) o;
							n.runOccam(MTApplication.this);
						}
					});
					t.start();
				}
			}
		});
		openButton.setOnAction(new EventHandler<ActionEvent>() {



			@Override
			public void handle(ActionEvent arg0) {
				FileChooser c = new FileChooser();
				FileChooser fileChooser = new FileChooser();
				FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("MT Text File", "*.txt");
				fileChooser.getExtensionFilters().add(extFilter);
				File lastFolder = new File(prefs.get(LAST_USED_FOLDER, "./"));
				if(!lastFolder.exists()) {
					fileChooser.setInitialDirectory(new File("./"));
				} else{
					fileChooser.setInitialDirectory(lastFolder);
				}



				List<File> files = fileChooser.showOpenMultipleDialog(stage);

				if (files != null) {

					String path = "";
					for(File f : files) {
						path += f.getAbsolutePath() + ";";
					}
					if(files.size() > 0) {
						prefs.put(LAST_USED_FOLDER, files.get(0).getParent());

					}
					fileField.textProperty().set(path);
				}
			}
		});
		return p;
	}
	private void clearData() {
		dataView.clearData();
		importedData.clear();
		activeData.set(null);
		activeLayering.get().refreshView();
		dataBoxes.getChildren().clear();

	}
	private void loadData() {
		// tabpane.getSelectionModel().select(1); //open up data view
		String [] path = fileField.getText().split(";");
		for(String s : path) {
			File f = new File(s);
			if(f.exists()) {
				if(f.isFile()) {
					Data d = Data.loadData(f);
					if(activeData.get() != null) {
						dataView.clearData();
					}
					dataView.setData(d);
//					System.out.println(f.getName());
					d.name = f.getName().split("\\.")[0];
					d.associatedEarth = new LayerEntry(this);
					activeData.set(d);
					this.importedData.add(d);
					DataInstanceNode node = new DataInstanceNode(dataBoxes, this, d);
					Platform.runLater(new Runnable() {

						@Override
						public void run() {
							dataBoxes.getChildren().add(node);
						}

					});

				}
			}
		}
		activeLayering.get().refreshView();
	}





	boolean fwdInit = false;



	public void fwd() {
		ArrayList<ArrayList<Double>> layering = getLayering();
		ArrayList<Double> freqs = activeData.get() == null ? getDefaultFrequencies() : activeData.get().frequencies;
		ArrayList<Double> res = Utils.getColumn(layering, 0);
		ArrayList<Double> thick = Utils.getColumn(layering, 1);
		double[] sig = new double[res.size()];
		double[] d = new double[res.size() - 1];
		double[] w = new double[freqs.size()];
		for (int i = 0; i < res.size(); i++) {
			sig[i] = 1 / res.get(i);
			if (i != res.size() - 1)
				d[i] = thick.get(i);
		}
		for (int i = 0; i < freqs.size(); i++) {
			w[i] = freqs.get(i) * 2 * Math.PI;
		}
		MTForward1D fwd = new MTForward1D(algorithm.get(),sig, d, w);
		Data data = fwd.calcPaPhi();
		dataView.updateCharts(data, importedData);
		syntheticData.set(data);
		fwdInit = true;
	}





	private ArrayList<Double> getDefaultFrequencies() {
		double minLogFreq = Math.log10(options.forwardOpts.minFrequency.get().get());
		double maxLogFreq = Math.log10(options.forwardOpts.maxFrequency.get().get());
		int nth = options.forwardOpts.logIncrements.get().get();
		double df = (maxLogFreq - minLogFreq)/nth;
		ArrayList<Double> frequencies = new ArrayList<Double>();
		for (double f = minLogFreq; f < maxLogFreq; f += df) {
			frequencies.add(Math.pow(10, f));
		}
		return frequencies;
	}

}