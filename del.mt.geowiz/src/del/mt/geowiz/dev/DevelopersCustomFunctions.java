package del.mt.geowiz.dev;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import del.mt.geowiz.MTApplication;
import del.mt.geowiz.dat.CSVReader;
import del.mt.geowiz.dat.DataInstanceNode;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.MenuItem;

public class DevelopersCustomFunctions {

	public static void initDevFunc1(MTApplication app, MenuItem runDevelopersFunction) {
		runDevelopersFunction.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				try {
					ArrayList<DataInstanceNode> nodes = app.getDataInstances();

					LinkedHashMap<String, DataInstanceNode> nodemap = new LinkedHashMap<String, DataInstanceNode>();
					for(DataInstanceNode n : nodes) {
						String id = n.name.getText();
//						System.out.println(id);
						nodemap.put(id, n);
					}


					File dirLines = new File("C:\\temp\\BHP\\Coordinates\\MT_Lines_Out");
					File coordsFile = new File("C:\\temp\\BHP\\Coordinates\\bhp_utm.txt");
					BufferedReader in = new BufferedReader(new FileReader(coordsFile));
					String headerLine = in.readLine();
//					System.out.println(headerLine);
					String line = "";


					LinkedHashMap<String, ArrayList<Double>> meta51 = new LinkedHashMap<String, ArrayList<Double>>();
					LinkedHashMap<String, ArrayList<Double>> meta52 = new LinkedHashMap<String, ArrayList<Double>>();
					LinkedHashMap<String, ArrayList<Double>> meta53 = new LinkedHashMap<String, ArrayList<Double>>();

					while((line = in.readLine()) != null) {
//						System.out.println("test" + line);
						String [] vals = line.split(",");
						String name = vals[0].split("\\.")[0];
						Double eString =  Double.valueOf(vals[1]);
						Double nString =  Double.valueOf(vals[2]);
						Double zString =  Double.valueOf(vals[3]);
						String zoneString =  vals[4];

						ArrayList<Double> metainfo = new ArrayList<Double>();
						metainfo.add(eString);
						metainfo.add(nString);
						metainfo.add(zString);
//						metainfo.add(zoneString);
						if(zoneString.contains("51")) {
							meta51.put(name, metainfo);
						} else if(zoneString.contains("52")) {
							meta52.put(name, metainfo);
						} else if(zoneString.contains("53")) {
							meta53.put(name, metainfo);
						}
					}




					String outPath = "C:\\temp\\BHP\\Coordinates\\SeismicASCII";

					double inc = 2;
					double minDepth = 0;
					double maxDepth = 2500;
					ArrayList<Double> depthLevels = getDepths(minDepth,maxDepth,inc);



					for(File f : dirLines.listFiles()) {
						File outTEFile = new File(outPath +"\\" + f.getName() + "_TE.ASCII");
						File outTMFile = new File(outPath +"\\"+ f.getName() + "_TM.ASCII");
						System.out.println(outTEFile.getAbsolutePath());
						BufferedReader inLine = new BufferedReader(new FileReader(f));
						BufferedWriter outTE = new BufferedWriter(new FileWriter(outTEFile));
						BufferedWriter outTM = new BufferedWriter(new FileWriter(outTMFile));

						int datum = 0;
						double dz = inc;
						int size = depthLevels.size();
						String header = datum + " " + (dz) + " " + size;
						outTE.write(header);
						outTM.write(header);
						outTE.newLine();
						outTM.newLine();
						String l = "";
						int zone = 51;
						LinkedHashMap<String, ArrayList<Double>> meta = meta51;

						if(f.getAbsolutePath().contains("51")) {
							zone = 51;
							meta = meta51;
						} else if(f.getAbsolutePath().contains("52")) {
							zone = 52;
							meta = meta52;
						} else if(f.getAbsolutePath().contains("53")) {
							zone = 53;
							meta = meta53;
						}

						int traceid = 1;
						while((l = inLine.readLine()) != null) {
							String [] sp = l.split("\t");
							double e = Double.valueOf(sp[0]);
							double n = Double.valueOf(sp[1]);

							String id = findClosest(e,n,nodes, zone, meta);
							DataInstanceNode node = nodemap.get(id);

							double [] depths = node.fieldData.invertedDepths;
							Double [] te = node.fieldData.invertedTERes;
							Double [] tm = node.fieldData.invertedTMRes;
							writeTrace(traceid, e,n, depthLevels,depths,te,tm,outTE,outTM);
							traceid++;
						}




//						if(f.getAbsolutePath().contains("51")) {
//
//							for(DataInstanceNode n : nodes) {
//								double [] depths = n.fieldData.invertedDepths;
//								Double [] te = n.fieldData.invertedTERes;
//								Double [] tm = n.fieldData.invertedTMRes;
////								writeTrace(depthLevels,depths,te,tm,outTE,outTM);
//
//
//
//
//							}
//						} else if(f.getAbsolutePath().contains("51")) {
//							for(DataInstanceNode n : nodes) {
//								double [] depths = n.fieldData.invertedDepths;
//								Double [] te = n.fieldData.invertedTERes;
//								Double [] tm = n.fieldData.invertedTMRes;
//
//							}
//						}  else if(f.getAbsolutePath().contains("52")) {
//							for(DataInstanceNode n : nodes) {
//								double [] depths = n.fieldData.invertedDepths;
//								Double [] te = n.fieldData.invertedTERes;
//								Double [] tm = n.fieldData.invertedTMRes;
//
//							}
//						}
						outTE.close();
						outTM.close();
						inLine.close();
					}
//
					in.close();
				} catch(Exception e) {
					e.printStackTrace();
				}

			}

			private String findClosest(double e, double n, ArrayList<DataInstanceNode> nodes, int zone,LinkedHashMap<String, ArrayList<Double>> meta) {
				double minOffset = Double.MAX_VALUE;
				String minID = meta.keySet().iterator().next();
				for(String id : meta.keySet()) {
					 ArrayList<Double> m = meta.get(id);
					 double e2 = m.get(0);
					 double n2 = m.get(1);
					 double offset = Math.pow(e - e2, 2) + Math.pow(n - n2, 2);
					 if(offset < minOffset) {
						 minID = id;
						 minOffset = offset;
					 }
				}
				return minID;
			}

			private void writeTrace(int id, double e, double n, ArrayList<Double> depthLevels, double[] depths, Double[] te, Double[] tm, BufferedWriter outTE, BufferedWriter outTM) throws Exception{
				String traceheader = id + "\t" + e + "\t" + n;
				outTE.write(traceheader);
				outTM.write(traceheader);
				for(double depth : depthLevels) {
					int nth = 0;
					while(depths[nth]<= depth && nth < depths.length) {
						nth++;
					}
					outTE.write("\t" + te[nth]);
					outTM.write("\t" + tm[nth]);
				}
				outTE.newLine();
				outTM.newLine();

			}

			private ArrayList<Double> getDepths(double minDepth, double maxDepth, double inc) {
				ArrayList<Double> vals = new ArrayList<>();

				for(double z = minDepth ; z <= maxDepth ; z+=inc) {
					vals.add(new Double(z));
				}
				return vals;
			}
		});
	}

}

