package del.mt.geowiz.fwd;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import javafx.application.Platform;
import jdk.nashorn.internal.runtime.arrays.ArrayLikeIterator;
import del.mt.geowiz.dat.Data;
import del.mt.geowiz.dat.Utils;
import del.mt.geowiz.exec.FileExecution;
import del.mt.geowiz.execution.AlgorithmStandaloneExecution;

public class MTForward1DMT1DC extends MTForward1DInstance{

//	public final static String EXECUTION_DIR = "execution/mt1dc/";
//	public final static String EXECUTION_INSTANCE = EXECUTION_DIR + "mt1d.exe";
//	public final static String EARTH_FILE = EXECUTION_DIR + "earth.txt";
//	public final static String DATA_FILE = EXECUTION_DIR + "data.txt";
//	public final static String DATA_FILE_OUT = EXECUTION_DIR + "data_synth.txt";
	
	public final static int ID = 300;
	private Data data;
	
	public MTForward1DMT1DC(double[] sig, double[] d, double[] w) {
		super(sig, d, w);
	}

	@Override
	public Data calcPaPhi() {
		try {
			AlgorithmStandaloneExecution e = new AlgorithmStandaloneExecution(this, new ArrayList<>(Arrays.asList(getAlgorithmExecutionCommands())));
			e.setCWD(TEMP_DIR);
			e.start(0);
			return data;
//			writeEarth(new File(EXECUTION_DIR));
//			writeFrequencies(new File(EXECUTION_DIR));
//			runScript();
//			return readData(new File(EXECUTION_DIR));
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private Data readData(File workingDirectory) throws Exception{
		File f = new File(workingDirectory.getAbsolutePath() + File.separator + "data_synth.txt");		
		
		ArrayList<Double> rhoXY = new ArrayList<Double>();
		ArrayList<Double> rhoYX = new ArrayList<Double>();
		ArrayList<Double> phaseXY = new ArrayList<Double>();
		ArrayList<Double> phaseYX = new ArrayList<Double>();
		ArrayList<Double> frequencies = new ArrayList<Double>();
		ArrayList<Double> impedances = new ArrayList<Double>();
		ArrayList<Double> weightings = new ArrayList<Double>();
		
		if(f.exists()) {
			try {
			BufferedReader  in = new BufferedReader(new FileReader(f));
			String line = "";
			in.readLine(); //skip header
			while((line = in.readLine()) != null) {
				String [] split = line.split("\t");
				double freq = Double.valueOf(split[0]);
				double aresXY = Double.valueOf(split[1]);
				double phXY = Double.valueOf(split[2]);
				double aresYX = Double.valueOf(split[3]);
				double phYX = Double.valueOf(split[4]);
				double Z = Double.valueOf(split[5]);
				
				frequencies.add(freq);
				rhoXY.add(aresXY);
				rhoYX.add(aresYX);
				phaseXY.add(phXY);
				phaseYX.add(phYX);
				impedances.add(Z);
				weightings.add(1.0);
				
			}
			in.close();
			} catch(Exception e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("File does not exist " + f.getAbsolutePath());
		}
		

		
		
	

		Data d = new Data(Data.MODE_SYNTHETIC,rhoXY, rhoXY, rhoYX, phaseXY, phaseYX, frequencies, weightings);
		this.data = d;
		return d;
	}
	@Override
	public String getAlgorithmExecutionFile() {		
		return "mt1D.exe";
	}
	@Override
	public String[] getAlgorithmExecutionCommands() {
		String [] commands = {TEMP_DIR};
		return commands;
	}
	@Override
	public boolean isLocal() {
		return false;
	}
	@Override
	public File getAlgorithmDirectory() {
		return new File("execution/mt1dc/");
	}


	@Override
	public void writeFiles(File workingDirectory) {
		try {
			writeFrequencies(workingDirectory);
			writeEarth(workingDirectory);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void readFiles(File workingDirectory) {
		try {
			readData(workingDirectory);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	private void writeFrequencies(File workingDirectory) throws Exception{
		File f = new File(workingDirectory.getAbsolutePath() + File.separator + "data.txt");
		if(f.exists()) f.delete();		
		BufferedWriter out = new BufferedWriter(new FileWriter(f));

		out.write("" + w.length);
		out.newLine();
		for(double d : w) {
			out.write("" + 360*(d/(2*Math.PI)) + "\t" + 0.0 + "\t" + 0.0 + "\t" + 0.0 + "\t" + 0.0);
			out.newLine();
		}
		out.close();
		
	}

	private void writeEarth(File workingDirectory) throws Exception{
		File f = new File(workingDirectory.getAbsolutePath() + File.separator + "earth.txt");
		if(f.exists()) f.delete();
		BufferedWriter out = new BufferedWriter(new FileWriter(f));
		out.write("" + sig.length);
		out.newLine();
		for(int i = 0 ; i < sig.length ; i++) {
			out.write(sig[i] + (i == sig.length - 1 ? "\t0.0" : "\t" + d[i]));
			out.newLine();
		}
		out.close();
	}

}
