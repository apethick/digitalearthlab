/**
 * Created by colton on 10/25/14.
 */
package del.mt.geowiz.fwd;

import static java.lang.Math.PI;
import static java.lang.Math.pow;

import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.commons.math3.complex.Complex;

import del.mt.geowiz.MTApplication;
import del.mt.geowiz.dat.Data;

//import edu.mines.jtk.util.*;

public class MTForward1D {

//	public int mode = MTForward1DJava.ID;
	public int mode = MTForward1DPy.ID;
	private double[] w;
	private double[] d;
	private double[] sig;
	
	public MTForward1D(int mode, double[] sig, double[] d, double[] w) {
		this.mode = mode;
		this.sig = sig;
		this.d = d;
		this.w = w;
	}

	public Data calcPaPhi() {
		
		switch (mode) {
		case MTForward1DJava.ID: return new MTForward1DJava(sig, d, w).calcPaPhi();			
		case MTForward1DPy.ID: return new MTForward1DPy(sig, d, w).calcPaPhi();
		case MTForward1DMT1DC.ID: return new MTForward1DMT1DC(sig, d, w).calcPaPhi();

		default:
			break;
		}
		return null;
	}



	

//
//    public static void main(String[] args){
////    	for(int nfreqs = 1 ; nfreqs < 750 ; nfreqs = (int) Math.ceil(nfreqs*1.5)) {
////    		double[] w = new double[nfreqs];
////    		double df = 20/nfreqs;
////    		double freq = -10;
////            for(int i=0; i<w.length; ++i){
////                w[i] = pow(10.0, freq);
////                freq+=df;
////            }
////    		for(int nlayers = 1 ; nlayers < 1000 ; nlayers=(int)Math.ceil(nlayers*1.5)) {
////    			 double[] d = new double[nlayers-1];
////    			 double[] sig = new double[nlayers];
////    		     for(int i = 0; i < nlayers ; i++) {
////    		    	 if(i < nlayers-1) d[i] = Math.random()*10;
////    		    	 sig[i] = Math.random()*10;
////    		     }
////    		     MTForward mtf = new MTForward(sig, d, w);
////    		     long start = System.nanoTime();
////    		     mtf.calcPaPhi();
////    		     long end = System.nanoTime();
////    		     double length = (((double) end) - ((double) start))/1E9;
////    		     System.out.println("serial" + "\t" + nfreqs + "\t" + nlayers + "\t" + length);
////    		}	
////    	}
//      
//    	int nfreqs = 20;
//    	int nlayers = 50;
//    	MTApplication.IS_GUI_MODE = false;
//    	double[] w = new double[nfreqs];
//		double df = 20/nfreqs;
//		double freq = -10;
//        for(int i=0; i<w.length; ++i){
//            w[i] = pow(10.0, freq);
//            freq+=df;
//        }
//    		
//        double[] d = new double[nlayers-1];
//		 double[] sig = new double[nlayers];
//	     for(int i = 0; i < nlayers ; i++) {
//	    	 if(i < nlayers-1) d[i] = Math.random()*10;
//	    	 sig[i] = Math.random()*10;
//	     }
//	     MTForward1D mtf = new MTForward1D(sig, d, w);
//	     
//	     mtf.calcPaPhi();
//	   
//	    
//	     
//	     for(int it = 1 ; it < 1000 ; it = (int) Math.ceil(it*1.5)) {
//	    	 long start = System.nanoTime();
//	    	 for(int i = 0; i < it ; i++) {
//	    		
//	 		     mtf.calcPaPhi();
//	 		
//	    	 }
//	         long end = System.nanoTime();
//	 		    double length = (((double) end) - ((double) start))/1E9;
//	 		     System.out.println("serial" + "\t" + it + "\t" + length);
//	     }
//
//
//	     for(int it = 1 ; it < 1E5 ; it = (int) Math.ceil(it*1.5)) {
//	    	 long start = System.nanoTime();
//		     ArrayList<MTForward1D> fwds = new ArrayList<MTForward1D>();
//		     ArrayList<Thread> threads = new ArrayList<Thread>();
//	    	 for(int i = 0; i < it ; i++) {
//	    		fwds.add(new MTForward1D(sig, d, w));
//	    	 }
//	    	 for(MTForward1D f : fwds) {
//	    		 Thread t = new Thread(new Runnable() {
//					
//					@Override
//					public void run() {
//						f.calcPaPhi();
//					}
//				});	    	
//	    		 threads.add(t);
//	    	 }
//	    	 int maxThreads = 8;
//	    	 
//	    	 ExecutorService executorService = new ThreadPoolExecutor(maxThreads, maxThreads,  1, TimeUnit.MINUTES,new ArrayBlockingQueue<Runnable>(maxThreads, true), new ThreadPoolExecutor.CallerRunsPolicy());
//	    	 for(Thread t : threads) {
//	    		 executorService.submit(t);
//	    	 }
//	         long end = System.nanoTime();
//	 		 double length = (((double) end) - ((double) start))/1E9;
//	 		 System.out.println("parallel" + "\t" + it + "\t" + length);
//	     }
        
//        mtf.calcPaPhi();
//        double[] phi = mtf.getPhi();
//        double[] pa = mtf.getPa();
//
//        System.out.println("PA");
//        for(int i=0; i<pa.length; ++i){
//            System.out.println(pa[i]);
//        }
//
//        System.out.println("PHI");
//        for(int i=0; i<phi.length; ++i){
//            System.out.println(phi[i]);
//        }
//    }
//
//    public MTForward1D(double[] sig, double[] d, double[] w){
//        this.sig=sig;
//        this.d = d;
//        this.w = w;
//        m = w.length;
//        n = sig.length;
//
//    }
//
//    public Data calcPaPhi() {
//    	Complex [] zn = new Complex[n];
////        Cdouble[] zn = new Cdouble[n];
//    	ArrayList<Double> freqs = new ArrayList<Double>();
//    	ArrayList<Double> res = new ArrayList<Double>();
//    	ArrayList<Double> phase = new ArrayList<Double>();
//        for (int i = 0; i < m; ++i) {        	
//        	Complex a = new Complex(0,w[i] * mu * sig[n - 1]).sqrt();
//            zn[n - 1] = (a.negate()).divide(sig[n - 1]);
//
//            for (int k = n - 2; k >= 0; --k) {
//                a = new Complex(0,w[i] * mu * sig[k]).sqrt();
//                Complex p1 = a.divide(sig[k]);
//                Complex p2 = (a.multiply(d[k]).tanh().negate()).add(zn[k+1].divide(a).multiply(sig[k]));
//                Complex p3 = ((zn[k+1].multiply((a.multiply(d[k])).tanh()).divide(a).multiply(sig[k])).negate()).add(1.0);
//                zn[k] = p1.multiply(p2).divide(p3);
//            }
//            	
//                res.add(pow(zn[0].abs(), 2) / (w[i] * mu));
//                phase.add(Math.toDegrees(Math.atan(zn[0].getImaginary()/zn[0].getReal())));
//                freqs.add(w[i]/(2*Math.PI)); //conversion from angular freq
//
//        }
//
//		return new Data(Data.MODE_SYNTHETIC, res, res, phase, phase, freqs, null);
//
//    }



}