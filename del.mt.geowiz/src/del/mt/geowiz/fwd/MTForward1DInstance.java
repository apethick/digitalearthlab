package del.mt.geowiz.fwd;

import static java.lang.Math.PI;
import static java.lang.Math.pow;

import java.io.File;
import java.util.ArrayList;

import org.apache.commons.math3.complex.Complex;

import del.mt.geowiz.dat.Data;

public abstract class MTForward1DInstance {
	protected double mu = 4.*PI*pow(10.0, -7.0);
    protected double[] sig, d, w;
 
    public final static String TEMP_DIR = System.getProperty("java.io.tmpdir") + File.separator + "mt1d" + File.separator;
    
    protected int m;
    protected int n;
    
    
    public MTForward1DInstance(double[] sig, double[] d, double[] w){
        this.sig=sig;
        this.d = d;
        this.w = w;
        m = w.length;
        n = sig.length;
        new File(TEMP_DIR).mkdirs();
    }
	public abstract String getAlgorithmExecutionFile();
	public abstract String [] getAlgorithmExecutionCommands();
	public abstract File getAlgorithmDirectory();
	public abstract boolean isLocal();

    public abstract Data calcPaPhi();
	public abstract void writeFiles(File workingDirectory);
	public abstract void readFiles(File workingDirectory);
	
    
}
