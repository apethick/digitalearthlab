package del.mt.geowiz.fwd;

import static java.lang.Math.pow;

import java.io.File;
import java.util.ArrayList;

import org.apache.commons.math3.complex.Complex;

import del.mt.geowiz.dat.Data;

public class MTForward1DJava extends MTForward1DInstance{

	public static final int ID = 100;

	public MTForward1DJava(double[] sig, double[] d, double[] w) {
		super(sig, d, w);
	}

	@Override
	 public Data calcPaPhi() {
    	Complex [] zn = new Complex[n];
//        Cdouble[] zn = new Cdouble[n];
    	ArrayList<Double> freqs = new ArrayList<Double>();
    	ArrayList<Double> res = new ArrayList<Double>();
    	ArrayList<Double> phase = new ArrayList<Double>();
    	ArrayList<Double> imp = new ArrayList<Double>();
        for (int i = 0; i < m; ++i) {        	
        	Complex a = new Complex(0,w[i] * mu * sig[n - 1]).sqrt();
            zn[n - 1] = (a.negate()).divide(sig[n - 1]);

            for (int k = n - 2; k >= 0; --k) {
                a = new Complex(0,w[i] * mu * sig[k]).sqrt();
                Complex p1 = a.divide(sig[k]);
                Complex p2 = (a.multiply(d[k]).tanh().negate()).add(zn[k+1].divide(a).multiply(sig[k]));
                Complex p3 = ((zn[k+1].multiply((a.multiply(d[k])).tanh()).divide(a).multiply(sig[k])).negate()).add(1.0);
                zn[k] = p1.multiply(p2).divide(p3);
            }
            	
            	imp.add(zn[0].abs());
                res.add(pow(zn[0].abs(), 2) / (w[i] * mu));
                phase.add(Math.toDegrees(Math.atan(zn[0].getImaginary()/zn[0].getReal())));
                freqs.add(w[i]/(2*Math.PI)); //conversion from angular freq

        }

		return new Data(Data.MODE_SYNTHETIC, imp, res, res, phase, phase, freqs, null);

    }

	@Override
	public String getAlgorithmExecutionFile() {		
		return null;
	}
	@Override
	public String[] getAlgorithmExecutionCommands() {
		return null;
	}
	@Override
	public boolean isLocal() {
		return true;
	}

	@Override
	public File getAlgorithmDirectory() {
		return null;
	}

	@Override
	public void writeFiles(File workingDirectory) {
		
	}

	@Override
	public void readFiles(File workingDirectory) {
		
	}

	

}
