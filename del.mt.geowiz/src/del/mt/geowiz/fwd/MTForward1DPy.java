package del.mt.geowiz.fwd;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import javafx.application.Platform;
import jdk.nashorn.internal.runtime.arrays.ArrayLikeIterator;
import del.mt.geowiz.dat.Data;
import del.mt.geowiz.dat.Utils;
import del.mt.geowiz.exec.FileExecution;
import del.mt.geowiz.execution.AlgorithmStandaloneExecution;

public class MTForward1DPy extends MTForward1DInstance{

	public final static String EXECUTION_DIR = "execution/py/";
	public final static String EXECUTION_INSTANCE = EXECUTION_DIR + "mt1dfwd.pyc";
	public final static String EARTH_FILE = EXECUTION_DIR + "earth.txt";
	public final static String DATA_FILE = EXECUTION_DIR + "data.txt";
	public final static String DATA_FILE_OUT = EXECUTION_DIR + "data_synth.txt";
	
	public final static int ID = 200;
	
	Data data;
	
	public MTForward1DPy(double[] sig, double[] d, double[] w) {
		super(sig, d, w);
	}

	@Override
	public Data calcPaPhi() {
		try {
			AlgorithmStandaloneExecution e = new AlgorithmStandaloneExecution(this, new ArrayList<>(Arrays.asList(getAlgorithmExecutionCommands())));
			e.setCWD(TEMP_DIR);
			e.start(0);
			return data;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private Data readData(File workingDirectory) throws Exception{
		File f = new File(workingDirectory.getAbsolutePath() + File.separator + "data_synth.txt");	

		ArrayList<Double> rhoXY = new ArrayList<Double>();
		ArrayList<Double> rhoYX = new ArrayList<Double>();
		ArrayList<Double> phaseXY = new ArrayList<Double>();
		ArrayList<Double> phaseYX = new ArrayList<Double>();
		ArrayList<Double> frequencies = new ArrayList<Double>();
		ArrayList<Double> weightings = new ArrayList<Double>();
		if(f.exists()) {
			System.out.println("true");
			BufferedReader  in = new BufferedReader(new FileReader(f));
			String line = "";
			in.readLine(); //skip header
			while((line = in.readLine()) != null) {
				String [] split = line.split("\t");
				double freq = Double.valueOf(split[0]);
				double ares = Double.valueOf(split[1]);
				double ares_err = Double.valueOf(split[2]);
				double phase = Double.valueOf(split[3]);
				double phase_err = Double.valueOf(split[4]);
				frequencies.add(freq);
				rhoXY.add(ares);
				rhoYX.add(ares);
				phaseXY.add(phase);
				phaseYX.add(phase);
				weightings.add(1.0);
				
			}
			in.close();
		} else {
			System.out.println("false");
		}
		

		
		
	

		Data d = new Data(Data.MODE_SYNTHETIC, rhoXY,rhoXY, rhoYX, phaseXY, phaseYX, frequencies, weightings);
		this.data = d;
		return d;
	}
	
	
	@Override
	public String getAlgorithmExecutionFile() {		
		return "python";
	}
	@Override
	public String[] getAlgorithmExecutionCommands() {
		String [] commands = {"mtrun_fwd.py", new File(EXECUTION_DIR).getAbsolutePath() + "/"};
		return commands;
	}
	@Override
	public boolean isLocal() {
		return false;
	}
	@Override
	public File getAlgorithmDirectory() {
		return new File(EXECUTION_DIR);
	}

	private void runScript() {
		String cmd = "python" + " ";
		cmd += (new File(EXECUTION_DIR).getAbsolutePath() + "/" + "mtrun_fwd.py") + " ";
		cmd +=  ("" + new File(EXECUTION_DIR).getAbsolutePath() + "/");
		try {
			Process p = Runtime.getRuntime().exec(cmd);
			p.waitFor();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void writeFrequencies(File workingDirectory) throws Exception{
		File f = new File(workingDirectory.getAbsolutePath() + File.separator + "data.txt");	
		if(f.exists()) f.delete();
		BufferedWriter out = new BufferedWriter(new FileWriter(f));
		
//        freq = float(line[0]);
//        ares = float(line[1]);
//        ares_err = float(line[2]);
//        phase = float(line[3]);
//        phase_err = float(line[4]);    
//		out.write("Frequency\tAres\tAresErr\tPhase\tPhaseErr");
//		out.newLine();
		for(double d : w) {
			out.write("" + 360*(d/(2*Math.PI)) + "\t" + 0.0 + "\t" + 0.0 + "\t" + 0.0 + "\t" + 0.0);
			out.newLine();
		}
		out.close();
		
	}

	private void writeEarth(File workingDirectory) throws Exception{
		File f = new File(workingDirectory.getAbsolutePath() + File.separator + "earth.txt");
		if(f.exists()) f.delete();
		BufferedWriter out = new BufferedWriter(new FileWriter(f));
		for(int i = 0 ; i < sig.length ; i++) {
			out.write(sig[i] + (i == sig.length - 1 ? "\t0.0" : "\t" + d[i]));
			out.newLine();
		}
		out.close();
	}

	@Override
	public void writeFiles(File workingDirectory) {
		try {
			writeFrequencies(workingDirectory);
			writeEarth(workingDirectory);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void readFiles(File workingDirectory) {
		try {
			readData(workingDirectory);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}

}
