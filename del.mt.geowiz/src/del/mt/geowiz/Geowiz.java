package del.mt.geowiz;


import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import open.opengl.viewer2d.Viewer2D;


public class Geowiz extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			setUserAgentStylesheet(STYLESHEET_CASPIAN);
			BorderPane root = new BorderPane();
//			BorderPane welcomePane = new BorderPane();
			root.setCenter(new MTApplication(primaryStage));


//			WritableImage snapshot = FontAwesome.MAGIC(32).snapshot(new SnapshotParameters(), null);
			primaryStage.getIcons().add(new Image(getClass().getResource("icon.png").toExternalForm()));
			root.setStyle("-fx-background-color: #0000000;");

			Scene scene = new Scene(root,1280,768);
//			Styles.getStyle().set(root);


			primaryStage.setScene(scene);
			primaryStage.show();
			primaryStage.setTitle("eMTeeBlackBox Developed by " + " Josh Poirier" +" Colton Kohnke" + " Katerina Gonzales" + " Elijah Thomas" + " Andrew Pethick : Agile Geoscience Hackathon 2014");

		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}
}