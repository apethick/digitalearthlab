package del.mt.geowiz.opts;

import javax.measure.quantity.Dimensionless;
import javax.measure.unit.SI;
import javax.measure.unit.Unit;

import del.mt.geowiz.MTApplication;
import del.mt.geowiz.fontawesome.FontAwesome;
import del.mt.geowiz.omnium.parameters.Information;
import del.mt.geowiz.omnium.parameters.InformationUtils;
import del.mt.geowiz.omnium.parameters.ParameterBoolean;
import del.mt.geowiz.omnium.parameters.ParameterDouble;
import del.mt.geowiz.omnium.parameters.ParameterInteger;
import del.mt.geowiz.omnium.parameters.PropertyInteger;
import del.mt.geowiz.omnium.parameters.representations.ParameterRepresentation;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.layout.VBox;

public class OptionsPlotting implements OptionsConstants{

	public final StringProperty FREQ_AXIS_TITLE = new SimpleStringProperty("Log Frequency");
	public final StringProperty RES_AXIS_TITLE = new SimpleStringProperty("Log Resistivity (Ohm m)");
	public final StringProperty PHASE_AXIS_TITLE = new SimpleStringProperty("Phase (degrees)");
	public final StringProperty IMPEDANCE_AXIS_TITLE = new SimpleStringProperty("Impedance (Ohm)");

	public ParameterInteger frequencyUnits = new ParameterInteger(new Information("Frequency Units","frequnits","Frequency Units"), Dimensionless.UNIT, UNITS_LOG_HERTZ, UNITS_LOG_HERTZ, false, ParameterRepresentation.SINGLE_COMBO);
	public PropertyInteger frequencyUnitsHertz = new PropertyInteger(new Information("Hz", "hz", "Frequency in Hertz"), UNITS_HERTZ);
	public PropertyInteger frequencyUnitsPeriod = new PropertyInteger(new Information("T(s)", "ts", "Period in Seconds"), UNITS_PERIOD);
	public PropertyInteger frequencyUnitsRootPeriod = new PropertyInteger(new Information("sqrt(T(s))", "logrp", "Root Period"), UNITS_ROOT_PERIOD);
	public PropertyInteger frequencyUnitsLogHertz = new PropertyInteger(new Information("Log(Hz)", "loghz", "Log Frequency in Hertz"), UNITS_LOG_HERTZ);
	public PropertyInteger frequencyUnitsLogPeriod = new PropertyInteger(new Information("Log(T(s))", "logts", "Log Period in Seconds"), UNITS_LOG_PERIOD);
	public PropertyInteger frequencyUnitsLogRootPeriod = new PropertyInteger(new Information("Log(sqrt(T(s)))", "logrp", "Log Root Period"), UNITS_LOG_ROOT_PERIOD);

	public ParameterInteger resistivityUnits = new ParameterInteger(new Information("Apparent Resistivity Units","appresunits","Apparent Resistivity Units"), Dimensionless.UNIT, UNITS_LOG_RESISTIVITY, UNITS_LOG_RESISTIVITY, false, ParameterRepresentation.SINGLE_COMBO);
	public PropertyInteger resistivityUnitsLogRes = new PropertyInteger(new Information("Log(Ohm.m)", "logohmm", "Apparent Resistivity in Log(Ohm.m)"), UNITS_LOG_RESISTIVITY);
	public PropertyInteger resistivityUnitsRes = new PropertyInteger(new Information("Ohm.m", "ohmm", "Apparent Resistivity in Ohm.m"), UNITS_RESISTIVITY);
	public PropertyInteger resistivityUnitsLogCond = new PropertyInteger(new Information("Log(S/m)", "logsm", "Apparent Conductivity in Log(S/m)"), UNITS_LOG_CONDUCTIVITY);
	public PropertyInteger resistivityUnitsCond = new PropertyInteger(new Information("S/m", "logs", "Apparent Conductivity in S/m"), UNITS_CONDUCTIVITY);

	public ParameterInteger phaseUnits = new ParameterInteger(new Information("Phase Units","phaseunits","Phase Units"), Dimensionless.UNIT, UNITS_DEGREES, UNITS_DEGREES, false, ParameterRepresentation.SINGLE_COMBO);
	public PropertyInteger phaseUnitsDegrees = new PropertyInteger(new Information("Degrees", "degrees", "Phase in degrees"), UNITS_DEGREES);
	public PropertyInteger phaseUnitsRadians = new PropertyInteger(new Information("Radians", "rad", "Phase in radians"), UNITS_RADIANS);


	public ParameterBoolean enableFixedFreq = new ParameterBoolean(new Information("Enable Fixed Freq Axis", "enableFixedFreq", "EnableFixedFreqAxis"), false, false, false, ParameterRepresentation.SINGLE_CHECKBOX);
	public ParameterDouble minFixedFreq = new ParameterDouble(new  Information("Fixed Min Freq", "minfixedfreq", "Min Fixed Freq"), SI.HERTZ, 0.0001, 0.0001, true, ParameterRepresentation.CUSTOM_TEXT);
	public ParameterDouble maxFixedFreq= new ParameterDouble(new  Information("Fixed Max Freq", "maxfixedfreq", "Max Fixed Freq"), SI.HERTZ, 10000, 10000, true, ParameterRepresentation.CUSTOM_TEXT);
	public ParameterDouble stepFixedFreq= new ParameterDouble(new  Information("Fixed Freq Step", "stepfixedfreq", "Fixed Freq Step"), SI.HERTZ, 1000, 1000, true, ParameterRepresentation.CUSTOM_TEXT);


	public ParameterBoolean enableFixedPhase = new ParameterBoolean(new Information("Enable Fixed Phase Axis", "enableFixedPhase", "EnableFixedPhaseAxis"), false, false, false, ParameterRepresentation.SINGLE_CHECKBOX);
	public ParameterDouble minFixedPhase = new ParameterDouble(new  Information("Fixed Min Phase", "minfixedphase", "Min Fixed Phase"), Dimensionless.UNIT, 0, 0, true, ParameterRepresentation.CUSTOM_TEXT);
	public ParameterDouble maxFixedPhase = new ParameterDouble(new  Information("Fixed Max Phase", "maxfixedphase", "Max Fixed Phase"), Dimensionless.UNIT, 60, 60, true, ParameterRepresentation.CUSTOM_TEXT);
	public ParameterDouble stepFixedPhase = new ParameterDouble(new  Information("Phase Step", "stepfixedphase", "Fixed Phase Step"), Dimensionless.UNIT, 5, 5, true, ParameterRepresentation.CUSTOM_TEXT);

	public ParameterBoolean enableFixedRho = new ParameterBoolean(new Information("Enable Fixed Rho Axis", "enableFixedRho", "EnableFixedRhoAxis"), false, false, false, ParameterRepresentation.SINGLE_CHECKBOX);
	public ParameterDouble minFixedRho = new ParameterDouble(new  Information("Fixed Min Rho", "minfixedrho", "Min Fixed Rho"),Dimensionless.UNIT, 0.0001, 0.0001, true, ParameterRepresentation.CUSTOM_TEXT);
	public ParameterDouble maxFixedRho = new ParameterDouble(new  Information("Fixed Max Rho", "maxfixedrho", "Max Fixed Rho"),Dimensionless.UNIT, 10000, 10000, true, ParameterRepresentation.CUSTOM_TEXT);
	public ParameterDouble stepFixedRho = new ParameterDouble(new  Information("Rho Step", "stepfixedrho", "Step Fixed Rho"),Dimensionless.UNIT, 100, 100, true, ParameterRepresentation.CUSTOM_TEXT);







	private MTApplication app;
	private Information info;

	public OptionsPlotting(MTApplication app) {
//		super(OptionsPlotting.class, , false, null);
		this.info = new Information("Data Plotting Options", "optsplot", "Data Plotting Options");
		this.app = app;
		frequencyUnits.addOptions(frequencyUnitsLogHertz, frequencyUnitsLogPeriod, frequencyUnitsLogRootPeriod, frequencyUnitsHertz,frequencyUnitsPeriod,frequencyUnitsRootPeriod);
		resistivityUnits.addOptions(resistivityUnitsLogRes,resistivityUnitsRes,resistivityUnitsLogCond,resistivityUnitsCond);
		phaseUnits.addOptions(phaseUnitsDegrees,phaseUnitsRadians);
		frequencyUnits.setOption(frequencyUnitsLogHertz);
		resistivityUnits.setOption(resistivityUnitsLogRes);
		phaseUnits.setOption(phaseUnitsDegrees);
		addListeners();
	}
	private void addListeners() {
		frequencyUnits.get().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				FREQ_AXIS_TITLE.set(getFrequencyUnitName());
				app.fwd();

			}
		});
		resistivityUnits.get().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				RES_AXIS_TITLE.set(getResistivitiesUnitName());
				app.fwd();
				System.out.println(RES_AXIS_TITLE.get() + " " + resistivityUnits.get().get());

			}
		});
		phaseUnits.get().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				PHASE_AXIS_TITLE.set(getPhaseUnitName());
				app.fwd();
			}
		});

		addPlottingListener(app,enableFixedFreq);
		addPlottingListener(app,minFixedFreq);
		addPlottingListener(app,maxFixedFreq);

		addPlottingListener(app,stepFixedFreq);
		addPlottingListener(app,stepFixedRho);
		addPlottingListener(app,stepFixedPhase);


		addPlottingListener(app,enableFixedPhase);
		addPlottingListener(app,minFixedPhase);
		addPlottingListener(app,maxFixedPhase);

		addPlottingListener(app,enableFixedRho);
		addPlottingListener(app,minFixedRho);
		addPlottingListener(app,maxFixedRho);


	}

	private void addPlottingListener(MTApplication app, ParameterDouble d) {
		d.get().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				app.fwd();
			}

		});
	}

	private void addPlottingListener(MTApplication app, ParameterBoolean b) {
		b.get().addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				app.fwd();
			}

		});
	}
	public Unit getFrequencyUnit() {
		switch (frequencyUnits.get().get()) {
		case UNITS_HERTZ: return SI.HERTZ;
		case UNITS_PERIOD: return SI.SECOND;
		case UNITS_ROOT_PERIOD: return SI.SECOND.pow(1/2);
		case UNITS_LOG_HERTZ: return SI.HERTZ;
		case UNITS_LOG_PERIOD: return SI.SECOND;
		case UNITS_LOG_ROOT_PERIOD: return SI.SECOND.pow(1/2);
		default:
			break;
		}
		 return SI.HERTZ;
	}
	public String getFrequencyUnitName() {
		switch (frequencyUnits.get().get()) {
		case UNITS_HERTZ: return "Frequency (Hz)";
		case UNITS_PERIOD: return "Period (s)";
		case UNITS_ROOT_PERIOD: return "Root Period (sqrt(s))";
		case UNITS_LOG_HERTZ: return "Log Frequency (Hz)";
		case UNITS_LOG_PERIOD: return "Log Period (s)";
		case UNITS_LOG_ROOT_PERIOD: return "Log Root Period (sqrt(s))";
		default:
			break;
		}
		 return "Frequency (Hz)";
	}
	public double convertFrequencyToUnit(double f) {
		switch (frequencyUnits.get().get()) {
		case UNITS_HERTZ: return f;
		case UNITS_PERIOD: return (1/f);
		case UNITS_ROOT_PERIOD: return Math.sqrt(1/f);
		case UNITS_LOG_HERTZ: return Math.log10(f);
		case UNITS_LOG_PERIOD: return Math.log10(1/f);
		case UNITS_LOG_ROOT_PERIOD: return Math.log10(Math.sqrt(1/f));
		default:
			break;
		}
		 return f;
	}
	public double convertResistivityToUnit(double v) {
		switch (resistivityUnits.get().get()) {
		case UNITS_LOG_RESISTIVITY: return Math.log10(v);
		case UNITS_RESISTIVITY: return (v);
		case UNITS_LOG_CONDUCTIVITY: return Math.log10(1/v);
		case UNITS_CONDUCTIVITY: return (1/v);

		default:
			break;
		}
		 return v;
	}
	public double convertPhaseToUnit(double v) {
		switch (phaseUnits.get().get()) {
		case UNITS_DEGREES: return v;
		case UNITS_RADIANS: return (v/360)*(Math.PI*2);

		default:
			break;
		}
		 return v;
	}
	public Node getIcon() {
		return FontAwesome.LINE_CHART();
	}



	public Node createNode() {
		Node l = InformationUtils.getLabel(info);
		Node n1 = frequencyUnits.createNode();
		Node n2 = resistivityUnits.createNode();
		Node n3 = phaseUnits.createNode();

		Node l2 = Options.createHeader("Fixed Plotting Options");
		Node n4 = enableFixedFreq.createNode();
		Node n5 = minFixedFreq.createNode();
		Node n6 = maxFixedFreq.createNode();
		Node n7 = stepFixedFreq.createNode();

		Node n8 = enableFixedPhase.createNode();
		Node n9 = minFixedPhase.createNode();
		Node n10 = maxFixedPhase.createNode();
		Node n11 = stepFixedPhase.createNode();

		Node n12 = enableFixedRho.createNode();
		Node n13 = minFixedRho.createNode();
		Node n14 = maxFixedRho.createNode();
		Node n15 = stepFixedRho.createNode();




		VBox b = new VBox(l, n1,n2,n3,l2, n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14,n15);
		return b;
	}






	public Series<Number, Number> convertFrequencies(Series seriesrxy, boolean isPhase) {
		Series<Number, Number> s = new XYChart.Series();
		for(int i = 0 ; i < seriesrxy.getData().size() ; i++) {
			 XYChart.Data dat = (XYChart.Data) seriesrxy.getData().get(i);
			 Number logf = (Number) dat.getXValue();
			 Number v = (Number) dat.getYValue();
			 Number convv = v;

			 Number timev = convertFrequencyToUnit(Math.pow(10, logf.doubleValue()));
			 if(isPhase) {
				  convv = convertPhaseToUnit(v.doubleValue());
			 } else {
				  convv = convertResistivityToUnit(v.doubleValue());
			 }
			 XYChart.Data sdat = new XYChart.Data(timev, convv);
			s.getData().add(sdat);

		}
		return s;

	}
	public String getResistivitiesUnitName() {
		switch (resistivityUnits.get().get()) {
		case UNITS_LOG_RESISTIVITY: return resistivityUnitsLogRes.information.getName().get();
		case UNITS_RESISTIVITY: return resistivityUnitsRes.information.getName().get();
		case UNITS_LOG_CONDUCTIVITY: return resistivityUnitsLogCond.information.getName().get();
		case UNITS_CONDUCTIVITY: return resistivityUnitsCond.information.getName().get();

		default:
			break;
		}
		return resistivityUnits.getSelectedOption().information.getName().get();
	}
	public String getPhaseUnitName() {

		switch (phaseUnits.get().get()) {
		case UNITS_DEGREES: return phaseUnitsDegrees.information.getName().get();
		case UNITS_RADIANS: return phaseUnitsRadians.information.getName().get();

		default:
			break;
		}
		return "Degrees";
	}
}