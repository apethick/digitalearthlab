package del.mt.geowiz.opts;

import javax.measure.quantity.Dimensionless;
import javax.measure.unit.NonSI;
import javax.measure.unit.SI;
import javax.measure.unit.Unit;

import del.mt.geowiz.MTApplication;
import del.mt.geowiz.omnium.parameters.Information;
import del.mt.geowiz.omnium.parameters.InformationUtils;
import del.mt.geowiz.omnium.parameters.ParameterBoolean;
import del.mt.geowiz.omnium.parameters.ParameterInteger;
import del.mt.geowiz.omnium.parameters.PropertyInteger;
import del.mt.geowiz.omnium.parameters.representations.ParameterRepresentation;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class OptionsGeoelectrical implements OptionsConstants{
	
	
	public ParameterInteger resistivityUnits = new ParameterInteger(new Information("Layer Resistivity Units","resunits","Layer Resistivity Units"), Dimensionless.UNIT, UNITS_OHM_M, UNITS_OHM_M, false, ParameterRepresentation.SINGLE_COMBO);
	public PropertyInteger resistivityUnitsOhmm = new PropertyInteger(new Information("Ohm.m", "ohmm", "Layer Resistivity Values in ohm meters"), UNITS_OHM_M);
	public PropertyInteger resistivityUnitsSM = new PropertyInteger(new Information("S/m", "sm", "Layer Electrical conductivity Values in seimens per meter"), UNITS_S_M);
	public PropertyInteger resistivityUnitsMSM = new PropertyInteger(new Information("mS/m", "msm", "Layer Electrical conductivity Values in milli Seimens per meter"), UNITS_MS_M);
	
	public ParameterBoolean resistivityLog = new ParameterBoolean(new Information("Log Resistivity","logres","Log Resistivity"),  true, true, false, ParameterRepresentation.SINGLE_CHECKBOX);
	
	public ParameterInteger thicknessUnits = new ParameterInteger(new Information("Thickness Units","thickunits","Thickness Units"), Dimensionless.UNIT,  UNITS_M, UNITS_M, false, ParameterRepresentation.SINGLE_COMBO);
	public PropertyInteger thicknessUnitsM = new PropertyInteger(new Information("m", "thickm", "Layer Thicknesses and Depth in Meters"), UNITS_M);
	public PropertyInteger thicknessUnitsF = new PropertyInteger(new Information("ft", "thickm", "Layer Thicknesses and Depth in Feet"), UNITS_FT);
	private Information information;
	
	
	public OptionsGeoelectrical(MTApplication app) {
		this.information = new Information("Geo-Electrical Options","geoopts","Geo-Electrircal Plotting Options");		
		resistivityUnits.addOptions(resistivityUnitsOhmm,resistivityUnitsSM, resistivityUnitsMSM);
		thicknessUnits.addOptions(thicknessUnitsM,thicknessUnitsF);
	
	}
	public Unit getResistivityUnit() {
		switch (resistivityUnits.get().get()) {
		case UNITS_OHM_M: return SI.OHM.times(SI.METER);
		case UNITS_S_M: return SI.SIEMENS.divide(SI.METER);
		case UNITS_MS_M: return SI.MILLI(SI.SIEMENS).divide(SI.METER);			
		default:			
		}
		return SI.OHM.times(SI.METER);
	}
	public Unit getThicknessUnit() {
		switch (thicknessUnits.get().get()) {
		case UNITS_M: return SI.METER;
		case UNITS_FT: return NonSI.FOOT;			
		default:
			break;
		}
		return SI.METER;
	}


	public Node createNode() {
		Node l = InformationUtils.getLabel(information);
		Node n1 = resistivityUnits.createNode();
		Node n2 = resistivityLog.createNode();
		Node n3 = thicknessUnits.createNode();
	
		VBox b = new VBox(l, n1,n2,n3);
		return b;
	}
}
