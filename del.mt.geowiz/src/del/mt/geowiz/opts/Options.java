package del.mt.geowiz.opts;

import del.mt.geowiz.MTApplication;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.effect.InnerShadow;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;



public class Options implements OptionsConstants {







	public final OptionsGeoelectrical geoOpts;
	public final OptionsForwardModelling forwardOpts;
	public final OptionsPlotting plottingOpts;

	private MTApplication app;






	public Options(MTApplication app) {
		this.forwardOpts = new OptionsForwardModelling(app);
		this.plottingOpts = new OptionsPlotting(app);
		this.geoOpts = new OptionsGeoelectrical(app);
		this.app = app;


			}




	public static Node createHeader(String l) {
		final Separator sepHor1 = new Separator();
		final Separator sepHor2 = new Separator();
		BorderPane p = new BorderPane(new Label(l));
		p.setTop(sepHor1);
		p.setBottom(sepHor2);
		InnerShadow rectangleShadow = new InnerShadow();
//		rectangleShadow.offsetXProperty().bind(1);
//		rectangleShadow.offsetYProperty().bind(1);
		rectangleShadow.setColor(Color.GRAY);

		p.setEffect(rectangleShadow);
		return p;
	}


	public Node createOptionsPane() {
		VBox b = new VBox();
		b.getChildren().add(createHeader("Geo-electrical Options"));
		b.getChildren().add(geoOpts.createNode());
		b.getChildren().add(createHeader("Plotting Options"));
		b.getChildren().add(plottingOpts.createNode());
		b.getChildren().add(createHeader("Forward Modelling Options"));
		b.getChildren().add(forwardOpts.createNode());

		return new ScrollPane(b);
	}

}