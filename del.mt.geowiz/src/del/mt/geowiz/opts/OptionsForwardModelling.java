package del.mt.geowiz.opts;

import javax.measure.quantity.Dimensionless;
import javax.measure.unit.SI;

import org.controlsfx.control.RangeSlider;

import del.mt.geowiz.MTApplication;
import del.mt.geowiz.fontawesome.FontAwesome;
import del.mt.geowiz.omnium.parameters.Information;
import del.mt.geowiz.omnium.parameters.InformationUtils;
import del.mt.geowiz.omnium.parameters.ParameterBoolean;
import del.mt.geowiz.omnium.parameters.ParameterDouble;
import del.mt.geowiz.omnium.parameters.ParameterInteger;
import del.mt.geowiz.omnium.parameters.representations.ParameterRepresentation;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class OptionsForwardModelling {

	public ParameterBoolean matchDataExtents = new ParameterBoolean(new Information("Match Frequency Extents","matchext","Match the frequency range of the loaded dataset"), false , false, false, ParameterRepresentation.SINGLE_CHECKBOX);
	public ParameterInteger logIncrements = new ParameterInteger(new Information("N Log Increments", "nloginc", "Number of frequencies to forward model spaced logarithmicaly"), Dimensionless.UNIT,30,30,false,ParameterRepresentation.CUSTOM_TEXT);
	public ParameterDouble minFrequency = new ParameterDouble(new Information("Min Frequency","minf","Minimum Frequency to Model"), SI.HERTZ, 0.0001, 0.0001, false, ParameterRepresentation.CUSTOM_TEXT);
	public ParameterDouble maxFrequency = new ParameterDouble(new Information("Max Frequency","maxf","Maximum Frequency to Model"), SI.HERTZ, 10000, 10000, false, ParameterRepresentation.CUSTOM_TEXT);
	private MTApplication app;
	private Information information;

	public OptionsForwardModelling(MTApplication app) {
		this.information = new Information("Forward Options","fwdopts","Forward Modelling Options");
		this.app = app;
		logIncrements.get().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				app.activeLayering.get().refreshView();
			}
		});

	}

	public Node getFrequencyRangeSlider() {

		RangeSlider range = new RangeSlider(-10, 10,-4, 4);

		range.lowValueProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				minFrequency.set(Math.pow(10,range.getLowValue()));
				app.activeLayering.get().refreshView();
			}
		});
		range.highValueProperty().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				maxFrequency.set(Math.pow(10,range.getHighValue()));
				app.activeLayering.get().refreshView();
			}
		});
		minFrequency.get().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				if(oldValue != newValue) {
					range.setLowValue(Math.log10(minFrequency.get().get()));
				}
			}
		});
		maxFrequency.get().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				if(oldValue != newValue) {
					range.setHighValue(Math.log10(maxFrequency.get().get()));
				}
			}
		});

		return range;
	}


	public Node getIcon() {
		return FontAwesome.FORWARD();
	}


	public Node createNode() {
		Node l = InformationUtils.getLabel(information);
		Node n1 = matchDataExtents.createNode();
		Node n2 = logIncrements.createNode();
		HBox b2 = new HBox(minFrequency.createNode(),maxFrequency.createNode());
		VBox b = new VBox(l, n1,n2,b2,getFrequencyRangeSlider());
		return b;
	}

}