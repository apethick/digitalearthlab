package del.mt.geowiz.opts;

import del.mt.geowiz.omnium.parameters.ParameterInteger;

public interface OptionsConstants {
	public final static int UNITS_OHM_M = 1;
	public final static int UNITS_S_M = 2;
	public final static int UNITS_MS_M = 3;
	
	public final static int UNITS_M = 1;
	public final static int UNITS_FT = 2;
	
	public final static int UNITS_HERTZ = 1;
	public final static int UNITS_PERIOD = 2;
	public final static int UNITS_ROOT_PERIOD = 3;
	public final static int UNITS_LOG_HERTZ = 4;
	public final static int UNITS_LOG_PERIOD = 5;
	public final static int UNITS_LOG_ROOT_PERIOD = 6;
	
	public static final int UNITS_LOG_RESISTIVITY = 1;
	public static final int UNITS_RESISTIVITY = 2;
	public static final int UNITS_LOG_CONDUCTIVITY = 3;
	public static final int UNITS_CONDUCTIVITY = 4;
	
	public static final int UNITS_DEGREES = 1;
	public static final int UNITS_RADIANS = 2;
}