package del.mt.geowiz.dat;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import Text.Text;
import del.mt.geowiz.MTApplication;
import del.mt.geowiz.geoelectric.LayerEntry;
import javafx.scene.chart.XYChart;

public class Data {

	public final static int MODE_SYNTHETIC = 1;
	public final static int MODE_FIELD = 2;
	public final static int MODE_RMS = 3;
	public double rms = 0;

	public ArrayList<Double> rhoXY;
	public ArrayList<Double> rhoYX;
	public ArrayList<Double> phaseXY;
	public ArrayList<Double> phaseYX;
	public ArrayList<Double> impedances;
	public ArrayList<Double> frequencies;
	public ArrayList<Double> weightings;


	public XYChart.Series seriesrxy = new XYChart.Series();
	public XYChart.Series seriesryx = new XYChart.Series();
	public XYChart.Series seriespxy = new XYChart.Series();
	public XYChart.Series seriespyx = new XYChart.Series();
	public XYChart.Series seriesimpedance = new XYChart.Series();
	private int mode;
	public String name = "";


	public LayerEntry associatedEarth; //only for field data, this way we can link a dataset with a 1d model, yes, not a good way, but good enough for now.
	public double [] invertedDepths;
	public Double [] invertedTERes;
	public Double [] invertedTMRes;


	public Data(int mode, ArrayList<Double>impedances, ArrayList<Double> rhoXY, ArrayList<Double> rhoYX, ArrayList<Double> phaseXY, ArrayList<Double> phaseYX, ArrayList<Double> frequencies, ArrayList<Double> weightings) {
		this.rhoXY = rhoXY;
		this.rhoYX = mode == MODE_SYNTHETIC ? rhoYX : rhoYX;
		this.phaseXY = phaseXY;
		this.phaseYX = mode == MODE_SYNTHETIC ? phaseYX : phaseYX;
		this.frequencies = frequencies;
		this.weightings = weightings;
		this.impedances = impedances;
		this.mode = mode;
		if(MTApplication.IS_GUI_MODE) generateSeries();
	}

	public static Data computeRMS(Data field, Data synth) {
		ArrayList<Double> rhoXY = computeMisfitData(field.rhoXY, synth.rhoXY,true);
		ArrayList<Double> rhoYX = computeMisfitData(field.rhoYX, synth.rhoYX,true);
		ArrayList<Double> phaseXY = computeMisfitData(field.phaseXY, synth.phaseXY,false);
		ArrayList<Double> phaseYX = computeMisfitData(field.phaseYX, synth.phaseYX,false);;
		ArrayList<Double> frequencies = field.frequencies;
		ArrayList<Double> weightings = field.weightings;
		ArrayList<Double> impedances = computeMisfitData(field.impedances, synth.impedances,true);
		Data rms = new Data(MODE_RMS, impedances, rhoXY, rhoYX, phaseXY, phaseYX, frequencies, weightings);
		rms.rms = computeTotalRMS(field,synth);
		return rms;

	}
	private static double computeTotalRMS(Data field, Data synth) {
		double rmsAR = computeAResRMS(field,synth);
		double rmsPh = computePhaseRMS(field,synth);
//		return rmsAR;
		return (rmsAR+rmsPh)/2;
	}

	private static double computePhaseRMS(Data field, Data synth) {
		double sumsq = 0;
		int N = field.rhoXY.size();
		double REL_ERR = 0.1;

		for(int i = 0;  i < N ; i++) {
			double dx2 = Math.pow((field.phaseXY.get(i)) - (synth.phaseXY.get(i))/(REL_ERR*field.phaseXY.get(i)),2);
			double dy2 = Math.pow((field.phaseYX.get(i)) - (synth.phaseYX.get(i))/(REL_ERR*field.phaseYX.get(i)),2);
			sumsq += dx2;
			sumsq += dy2;
		}
		double avgsq = sumsq/(4*N);
		return Math.sqrt(avgsq);
	}

	private static double computeAResRMS(Data field, Data synth) {
		double sumsq = 0;
		int N = field.rhoXY.size();
		double REL_ERR = 0.1;

		for(int i = 0;  i < N ; i++) {
			double dx2 = Math.pow((field.rhoXY.get(i)) - (synth.rhoXY.get(i))/(REL_ERR*field.rhoXY.get(i)),2);
			double dy2 = Math.pow((field.rhoYX.get(i)) - (synth.rhoYX.get(i))/(REL_ERR*field.rhoYX.get(i)),2);
			sumsq += dx2;
			sumsq += dy2;
		}
		double avgsq = sumsq/(4*N);
		return Math.sqrt(avgsq);
	}

	public static ArrayList<Double> computeMisfitData(ArrayList<Double> d1, ArrayList<Double> d2, boolean isLog) {
		ArrayList<Double> array = new ArrayList<Double>();
		int N  = d1.size();
		double rel_err = 10;
		for(int i = 0 ; i < N ; i++) {
			double val = isLog ? Math.pow((Math.log10(d1.get(i))-Math.log10(d2.get(i))),2)/(0.01*rel_err*Math.log10(d2.get(i))) : Math.pow((d1.get(i)-d2.get(i)),2)/(0.01*rel_err*d2.get(i));
			array.add(val);
		}
		return array;
	}


	public void update(Data d) {
		this.rhoXY = d.rhoXY;
		this.rhoYX = mode==MODE_SYNTHETIC ? d.rhoXY : d.rhoYX;
		this.phaseXY = d.phaseXY;
		this.phaseYX = mode==MODE_SYNTHETIC ? d.phaseXY : d.phaseYX;
		this.impedances = d.impedances;
		this.frequencies = d.frequencies;
		this.weightings = d.weightings;
		this.mode = d.mode;
		updateSeries();
	}

	public void generateSeries() {
		seriesrxy = new XYChart.Series();
		seriesryx = new XYChart.Series();
		seriespxy = new XYChart.Series();
		seriespyx = new XYChart.Series();
		seriesimpedance = new XYChart.Series();
		updateSeries();


	}



	private void updateSeries() {


		String seriesrxyName = "Apparent Resistivity 1D Synthetic";

		String seriesryxName = "NA";
		String seriespxyName = "Phase 1D Synthetic";
		String seriespyxName = "NA";
		String seriesimpedanceName = "Impedance";
		switch (mode) {
		case MODE_FIELD: {
			seriesrxyName = "Apparent Resistivity XY Data";
			seriesryxName = "Apparent Resistivity YX Data";
			seriespxyName = "Phase XY Data";
			seriespyxName = "Phase YX Data";
			seriesimpedanceName = "Impedance Data";
			break;
		}
		case MODE_SYNTHETIC : {
			seriesrxyName = "Apparent Resistivity 1D Synthetic";
			seriesryxName = "NA";
			seriespxyName = "Phase 1D Synthetic";
			seriespyxName = "NA";
			seriesimpedanceName = "Impedance Data";
			break;
		}
		case MODE_RMS : {
			seriesrxyName = "A.Res. RMS XY";
			seriesryxName = "A.Res. RMS YX";
			seriespxyName = "Phase RMS XY";
			seriespyxName = "Phase RMS YX";
			seriesimpedanceName = "Impedance RMS Data";
			break;
		}
		default:
			break;
		}

		seriesrxy.setName(seriesrxyName);
		seriesryx.setName(seriesryxName);
		seriespxy.setName(seriespxyName);
		seriespyx.setName(seriespyxName);
		seriesimpedance.setName(seriesimpedanceName);
		seriesrxy.getData().clear();
		seriesryx.getData().clear();
		seriespxy.getData().clear();
		seriespyx.getData().clear();
		seriesimpedance.getData().clear();
		for (int i = 0; i < frequencies.size(); i++) {
			seriesrxy.getData().add(new XYChart.Data(Math.log10(frequencies.get(i)), rhoXY.get(i)));
			if(mode != MODE_SYNTHETIC)seriesryx.getData().add(new XYChart.Data(Math.log10(frequencies.get(i)), rhoYX.get(i)));
			seriespxy.getData().add(new XYChart.Data(Math.log10(frequencies.get(i)), (phaseXY.get(i))));
			if(mode != MODE_SYNTHETIC)seriespyx.getData().add(new XYChart.Data(Math.log10(frequencies.get(i)), (phaseYX.get(i))));

			seriesimpedance.getData().add(new XYChart.Data(Math.log10(frequencies.get(i)), (impedances.get(i))));
			if(mode != MODE_SYNTHETIC)seriesimpedance.getData().add(new XYChart.Data(Math.log10(frequencies.get(i)), (impedances.get(i))));

		}
	}

	public final static Data loadData(File f) {
		System.out.println("Loading...." + f.getAbsolutePath());
		int ncols = -1;
		try {
			BufferedReader in = new BufferedReader(new FileReader(f));
			in.readLine();
			ncols = Text.split(in.readLine()).size();

			in.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
		if(ncols == 5) {
			ArrayList<ArrayList<Double>> data = importData(f);
			ArrayList<Double> freqs = Utils.getColumn(data, 0);
			ArrayList<Double> rhoxy = Utils.getColumn(data, 1);
			ArrayList<Double> phasexy = Utils.getColumn(data, 2);
			ArrayList<Double> rhoyx = Utils.getColumn(data, 3);
			ArrayList<Double> phaseyx = Utils.getColumn(data, 4);
			Data d = new Data(MODE_FIELD, rhoxy, rhoxy, rhoyx, phasexy, phaseyx, freqs, null);
			return d;
		}
		if(ncols == 34) {
//			0	FREQ
//			1	sitelongitude
//			2	sitelatitude
//			3	elevation
//			4	RHOXY
//			5	RHOXYVAR
//			6	PHSXY
//			7	PHSXYVAR
//			8	RHOYX
//			9	RHOYXVAR
//			10	PHSYX
//			11	PHSYXVAR
//			12	RHOXX
//			13	RHOXXVAR
//			14	PHSXX
//			15	PHSXXVAR
//			16	RHOYY
//			17	RHOYYVAR
//			18	PHSYY
//			19	PHSYYVAR
//			20	ZXYR
//			21	ZXYI
//			22	ZXYVAR
//			23	ZYXR
//			24	ZYXI
//			25	ZYXVAR
//			26	ZXXR
//			27	ZXXRVAR
//			28	ZXXI
//			29	ZXXIVAR
//			30	ZYYR
//			31	ZYYRVAR
//			32	ZYYI
//			33	ZYYIVAR

			ArrayList<ArrayList<Double>> data = importData(f);
			ArrayList<Double> freqs = Utils.getColumn(data, 0);
			ArrayList<Double> rhoxy = Utils.getColumn(data, 4);
			ArrayList<Double> phasexy = Utils.getColumn(data, 6);
			ArrayList<Double> rhoyx = Utils.getColumn(data, 8);
			ArrayList<Double> phaseyx = Utils.getColumn(data, 10);
			Data d = new Data(MODE_FIELD, rhoxy, rhoxy, rhoyx, phasexy, phaseyx, freqs, null);
			return d;
		}

		return null;

	}

	private static ArrayList<ArrayList<Double>> importData(File f) {
		ArrayList<ArrayList<Double>> frequencies = new ArrayList<ArrayList<Double>>();

		try {
			String line = "";
			BufferedReader in = new BufferedReader(new FileReader(f));
			in.readLine();// skip header
			while ((line = in.readLine()) != null) {
				ArrayList<Double> frequencyEntry = new ArrayList<Double>();

				String[] split = line.split("\t");
				for (String s : split) {
					frequencyEntry.add((Double.valueOf(s)));
				}
				frequencies.add(frequencyEntry);
			}
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Collections.sort(frequencies, new Comparator<ArrayList<Double>>() {

			@Override
			public int compare(ArrayList<Double> lhs, ArrayList<Double> rhs) {
				Double a = lhs.get(0);
				Double b = rhs.get(0);
				return a.compareTo(b);
			}
		});
		return frequencies;
	}



}