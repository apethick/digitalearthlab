package del.mt.geowiz.dat;

import java.util.ArrayList;

import del.mt.geowiz.MTApplication;
import del.mt.geowiz.opts.Options;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Label;
import javafx.scene.control.SplitPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.BorderPane;

public class DataView {
	public final LineChart<Number, Number> chartImpedances;
	public final LineChart<Number, Number> chartResistivities;
	public final LineChart<Number, Number> chartPhases;
	final NumberAxis xAxisResistivities = new NumberAxis();
	final NumberAxis xAxisPhases = new NumberAxis();
	final NumberAxis xAxisImpedances = new NumberAxis();

	final NumberAxis yAxisResistivities = new NumberAxis();
	final NumberAxis yAxisPhases = new NumberAxis();
	final NumberAxis yAxisImpedances = new NumberAxis();

	private MTApplication app;

	ToggleButton ares = new ToggleButton("ARes");
	ToggleButton phas = new ToggleButton("Phase");
	ToggleButton impe = new ToggleButton("Z");
	ToggleButton aresmis = new ToggleButton("ARes Misfit");
	ToggleButton phasmis = new ToggleButton("Phase Misfit");

	public DataView(MTApplication app) {
		this.app = app;
		bindLabelToTitle(xAxisResistivities,app.options.plottingOpts.FREQ_AXIS_TITLE);
		bindLabelToTitle(xAxisPhases,app.options.plottingOpts.FREQ_AXIS_TITLE);
		bindLabelToTitle(yAxisResistivities,app.options.plottingOpts.RES_AXIS_TITLE);
		bindLabelToTitle(yAxisPhases,app.options.plottingOpts.PHASE_AXIS_TITLE);

		chartResistivities = new LineChart<Number, Number>(xAxisResistivities,yAxisResistivities);
		chartPhases = new LineChart<Number, Number>(xAxisPhases, yAxisPhases);
		chartImpedances= new LineChart<Number, Number>(xAxisImpedances, yAxisImpedances);

		ares.setSelected(true);
		phas.setSelected(true);
	}

	private void bindLabelToTitle(NumberAxis ax, StringProperty s) {
		ax.labelProperty().set(s.get());
		s.addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				ax.setLabel(newValue);
			}

		});
	}

	public Node createViewPane() {
		BorderPane bp = new BorderPane();

		SplitPane vs = new SplitPane();

		vs.setOrientation(Orientation.VERTICAL);
		updateChartOrders(vs);

		bp.setCenter(vs);
		bp.setTop(getToolbar(vs));
		return bp;
	}
	private Node getToolbar(SplitPane vs) {
		ToolBar bar = new ToolBar();
		setUpdateListener(vs, ares,phas,impe,aresmis,phasmis);
		bar.getItems().addAll(ares,phas,impe,getRMSLabel());
		return bar;
	}

	private void setUpdateListener(SplitPane vs, ToggleButton ... buttons) {
		for(ToggleButton b : buttons) {
			b.selectedProperty().addListener(new ChangeListener<Boolean>() {

				@Override
				public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
					updateChartOrders(vs);
				}


			});
		}
	}
	private void updateChartOrders(SplitPane vs) {
		vs.getItems().clear();
		if(ares.isSelected()) vs.getItems().add(chartResistivities);
		if(phas.isSelected()) vs.getItems().add(chartPhases);
		if(impe.isSelected()) vs.getItems().add(chartImpedances);
	}
	private Node getRMSLabel() {
		Label l = new Label("");
		l.setVisible(true);
		app.rmsData.addListener(new ChangeListener<Data>() {

			@Override
			public void changed(ObservableValue<? extends Data> observable, Data oldValue, Data newValue) {
				if(newValue == null) l.setVisible(false);
				else {
					l.setVisible(true);
					String rms = "RMS = " + (newValue.rms) + "%";
					l.setText(rms);
				}
			}
		});
		return l;
	}

	public void clearData() {
		if(app.activeData.get() != null) {
			chartResistivities.getData().remove(app.activeData.get().seriesrxy);
			chartResistivities.getData().remove(app.activeData.get().seriesryx);
			chartPhases.getData().remove(app.activeData.get().seriespxy);
			chartPhases.getData().remove(app.activeData.get().seriespyx);
			chartImpedances.getData().remove(app.activeData.get().seriesimpedance);

		}
	}

	public void setData(Data d) {
		chartResistivities.getData().add(d.seriesrxy);
		chartResistivities.getData().add(d.seriesryx);
		chartPhases.getData().add(d.seriespxy);
		chartPhases.getData().add(d.seriespyx);
	}
	public void updateCharts(Data synth, ArrayList<Data> fieldData) {
		chartResistivities.getData().clear();
		chartPhases.getData().clear();
		chartImpedances.getData().clear();
		Options options = app.options;
		try {
			Series<Number, Number> rxy = options.plottingOpts.convertFrequencies(synth.seriesrxy,false);
			Series<Number, Number> pxy = options.plottingOpts.convertFrequencies(synth.seriespxy,true);
			Series<Number, Number> zxy = options.plottingOpts.convertFrequencies(synth.seriesimpedance,false);
			rxy.setName("Synthetic Data");
			pxy.setName("Synthetic Data");
			zxy.setName("Synthetic Data");


			Data sd = app.syntheticData.get();
			if(app.syntheticData.get() != null) {
				chartResistivities.getData().add(rxy);
				chartPhases.getData().add(pxy);
				chartImpedances.getData().add(zxy);
//				syntheticData.set(data);
			} else {
				chartResistivities.getData().add(rxy);
				chartPhases.getData().add(pxy);
				chartImpedances.getData().add(zxy);
				app.syntheticData.set(synth);

			}

//			if(fieldData.get() != null) {
//				rmsData.set(Data.computeRMS(fieldData.get(),syntheticData.get()));
//
//			} else {
//				rmsData.set(null) ;
//			}
			if(fieldData.size() > 0) {
				for(Data field : fieldData) {

					Series<Number, Number> frxy = options.plottingOpts.convertFrequencies(field.seriesrxy, false);
					Series<Number, Number> fpxy = options.plottingOpts.convertFrequencies(field.seriespxy, true);
					Series<Number, Number> fryx = options.plottingOpts.convertFrequencies(field.seriesryx, false);
					Series<Number, Number> fpyx = options.plottingOpts.convertFrequencies(field.seriespyx, true);

					frxy.setName("Field XY " + field.name);
					fryx.setName("Field YX " + field.name);
					fpxy.setName("Field XY " + field.name);
					fpyx.setName("Field YX " + field.name);
					chartResistivities.getData().add(frxy);
					chartPhases.getData().add(fpxy);
					chartResistivities.getData().add(fryx);
					chartPhases.getData().add(fpyx);
				}
			}
			chartResistivities.setAnimated(false);
			chartPhases.setAnimated(false);
			chartImpedances.setAnimated(false);

			xAxisResistivities.setLabel(options.plottingOpts.FREQ_AXIS_TITLE.get());
			xAxisPhases.setLabel(options.plottingOpts.FREQ_AXIS_TITLE.get());
			xAxisImpedances.setLabel(options.plottingOpts.FREQ_AXIS_TITLE.get());

			yAxisResistivities.setLabel(options.plottingOpts.RES_AXIS_TITLE.get());
			yAxisPhases.setLabel(options.plottingOpts.PHASE_AXIS_TITLE.get());
			yAxisImpedances.setLabel(options.plottingOpts.IMPEDANCE_AXIS_TITLE.get());


			if(options.plottingOpts.enableFixedFreq.get().get()) {
				double min = options.plottingOpts.minFixedFreq.get().get();
				double max = options.plottingOpts.maxFixedFreq.get().get();
				double step = options.plottingOpts.stepFixedFreq.get().get();
				xAxisImpedances.setAutoRanging(false);
				xAxisImpedances.setLowerBound(min);
			    xAxisImpedances.setUpperBound(max);

			    xAxisPhases.setAutoRanging(false);
			    xAxisPhases.setLowerBound(min);
			    xAxisPhases.setUpperBound(max);

			    xAxisResistivities.setAutoRanging(false);
			    xAxisResistivities.setLowerBound(min);
			    xAxisResistivities.setUpperBound(max);

			    xAxisImpedances.setTickUnit(step);
			    xAxisPhases.setTickUnit(step);
			    xAxisResistivities.setTickUnit(step);

			} else {
				xAxisImpedances.setAutoRanging(true);
				xAxisPhases.setAutoRanging(true);
				xAxisImpedances.setAutoRanging(true);
			}

			if(options.plottingOpts.enableFixedRho.get().get()) {
				double min = options.plottingOpts.minFixedRho.get().get();
				double max = options.plottingOpts.maxFixedRho.get().get();
				double step = options.plottingOpts.stepFixedRho.get().get();
				yAxisResistivities.setAutoRanging(false);
				yAxisResistivities.setLowerBound(min);
				yAxisResistivities.setUpperBound(max);
				yAxisResistivities.setTickUnit(step);
			} else {
				yAxisResistivities.setAutoRanging(true);
			}

			if(options.plottingOpts.enableFixedPhase.get().get()) {
				double min = options.plottingOpts.minFixedPhase.get().get();
				double max = options.plottingOpts.maxFixedPhase.get().get();
				double step = options.plottingOpts.stepFixedPhase.get().get();
				yAxisPhases.setAutoRanging(false);
				yAxisPhases.setLowerBound(min);
				yAxisPhases.setUpperBound(max);
				yAxisPhases.setTickUnit(step);
			} else {
				yAxisPhases.setAutoRanging(true);
			}

		} catch(Exception e) {

		}
	}
}
