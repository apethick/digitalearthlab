package del.mt.geowiz.dat;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import del.mt.geowiz.GeowizProperties;
import del.mt.geowiz.MTApplication;
import del.mt.geowiz.exec.FileExecution;
import del.mt.geowiz.geoelectric.LayerEntry;
import del.mt.geowiz.occam.OCCAM1DCSEM;
import javafx.beans.property.ObjectProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class DataInstanceNode extends BorderPane{

	public Button remove = new Button("X");
	public Button setActive = new Button("INACTIVE");
	public Button runOCCAM = new Button("RUN OCCAM");
	public Label name = new Label("Name");
	public CheckBox enable = new CheckBox("");
	private VBox parent;
	private MTApplication app;
	public Data fieldData;

	public DataInstanceNode(VBox parent, MTApplication app, Data fieldData) {
		this.parent = parent;
		this.app = app;
		this.fieldData = fieldData;
		setActive.setStyle("-fx-font: 22 arial; -fx-base: red;");
		remove.setStyle("-fx-font: 22 arial; -fx-base: red;");
		runOCCAM.setStyle("-fx-font: 22 arial; -fx-base: green;");
		remove.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				remove();
			}
		});

		setActive.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				setActive();
			}


		});
		runOCCAM.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				runOccam(app);
			}

		});
		enable.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				setEnabled(enable.isSelected());
			}
		});
		name.setText(fieldData.name);
		HBox left = new HBox(enable,name);
		HBox right = new HBox(setActive,remove, runOCCAM);
		setLeft(left);
		setRight(right);
		setEnabled(false);
		setActive();

	}
	public void runOccam(MTApplication app) {
		String name = this.name.getText();
		String scratch = GeowizProperties.SCRATCH;
		new File(scratch).mkdirs();
		File runFolder = new File(scratch + "/run/" + name + "/");
		runFolder.mkdirs();

		File runFolderTE = new File(scratch + "/run/" + name + "/TE/");
		File runFolderTM = new File(scratch + "/run/" + name + "/TM/");
		runFolderTE.mkdirs();
		runFolderTM.mkdirs();
		for(File f : runFolderTE.listFiles()) {
			try{
				f.delete();
			} catch(Exception e){

			}
		}
		for(File f : runFolderTM.listFiles()) {
			try{
				f.delete();
			} catch(Exception e){

			}
		}
		String fxypath = scratch + "/run/" + name + "/TE/";
		String fyxpath = scratch + "/run/" + name + "/TM/";
		File fxyDat = new File(fxypath + "data");
		File fyxDat = new File(fyxpath + "data");
		File fxyMod = new File(fxypath + "model");
		File fyxMod = new File(fyxpath + "model");
		File fxySta = new File(fxypath + "startup");
		File fyxSta = new File(fyxpath + "startup");
		try {

			copyFileUsingStream(new File("execution\\occam\\Occam1DCSEM.exe"), new File(scratch + "/run/" + name + "/TE/Occam1DCSEM.exe"));
			copyFileUsingStream(new File("execution\\occam\\Occam1DCSEM.exe"), new File(scratch + "/run/" + name + "/TM/Occam1DCSEM.exe"));
			OCCAM1DCSEM.writeDataFile(fieldData, fxyDat, fyxDat);
			OCCAM1DCSEM.writeEarthFile(fieldData.associatedEarth.earth, fxyMod, fyxMod);
			OCCAM1DCSEM.writeEarthFile(fieldData.associatedEarth.earth, fxyMod, fyxMod);
			OCCAM1DCSEM.writeStartupFile(fieldData.associatedEarth.earth, fxySta, fyxSta, "model", "data");
			FileExecution.runProcess(scratch + "/run/" + name + "/TE/Occam1DCSEM.exe", new File(fxypath));
			FileExecution.runProcess(scratch + "/run/" + name + "/TM/Occam1DCSEM.exe", new File(fyxpath));
			OCCAM1DCSEM.readOCCAMModel(fieldData, runFolderTE, OCCAM1DCSEM.COMPONENT_TE,app);
			OCCAM1DCSEM.readOCCAMModel(fieldData, runFolderTM, OCCAM1DCSEM.COMPONENT_TM,app);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	private static void copyFileUsingStream(File source, File dest) throws IOException {
	    InputStream is = null;
	    OutputStream os = null;
	    try {
	        is = new FileInputStream(source);
	        os = new FileOutputStream(dest);
	        byte[] buffer = new byte[1024];
	        int length;
	        while ((length = is.read(buffer)) > 0) {
	            os.write(buffer, 0, length);
	        }
	    } finally {
	        is.close();
	        os.close();
	    }
	}
	private void setActive() {
		for(Node n : parent.getChildren()) {
			if (n instanceof DataInstanceNode) {
				DataInstanceNode node = (DataInstanceNode) n;
				if(node != DataInstanceNode.this) {
					node.setActive.setText("INACTIVE");
					node.setActive.setStyle("-fx-font: 22 arial; -fx-base: red;");
				} else {
					node.setActive.setStyle("-fx-font: 22 arial; -fx-base: #b6e7c9;");
					node.setActive.setText("ACTIVE");
				}
			}
		}
		app.activeData.set(fieldData);
		app.activeLayering.set(fieldData.associatedEarth);
		app.fwd();

	}
	private void remove() {
		this.parent.getChildren().remove(this);
		this.app.importedData.remove(this.fieldData);
		this.app.fwd();
	}

	private void setEnabled(boolean selected) {
		try {
			if(selected) {
				this.app.importedData.add(this.fieldData);
			} else {
				this.app.importedData.remove(this.fieldData);
			}
		} catch(Exception e) {

		}
		this.app.fwd();
	}
	public static void setAllEarths(ArrayList<DataInstanceNode> nodes, ObjectProperty<LayerEntry> activeLayering) {
		for(DataInstanceNode n  : nodes) {
			n.fieldData.associatedEarth.earth.setEarth(activeLayering.get().earth);
		}
	}
	public static void setAllSelectedEarths(ArrayList<DataInstanceNode> nodes, ObjectProperty<LayerEntry> activeLayering) {
		for(DataInstanceNode n  : nodes) {
			if(n.enable.isSelected()) {
				n.fieldData.associatedEarth.earth.setEarth(activeLayering.get().earth);
			}
		}
	}

}
