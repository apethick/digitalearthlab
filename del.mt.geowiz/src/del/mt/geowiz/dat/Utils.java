package del.mt.geowiz.dat;

import java.util.ArrayList;

public class Utils {
	public static ArrayList<Double> getColumn(ArrayList<ArrayList<Double>> data, int i) {
		ArrayList<Double> column = new ArrayList<Double>();
		for (ArrayList<Double> d : data) {
			column.add(d.get(i));
		}
		return column;
	}
}
