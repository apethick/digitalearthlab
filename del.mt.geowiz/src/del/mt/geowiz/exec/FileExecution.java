package del.mt.geowiz.exec;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class FileExecution {
	
	public static ArrayList<String> runProcess(String executable, File currentWorkingDirectory) throws Exception{
		ArrayList<String> s = new ArrayList<String>();
		Runtime rt = Runtime.getRuntime();
		String commands[] = { "" };
		Process p = rt.exec(executable, commands, currentWorkingDirectory);
		BufferedReader proc1StdOut = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String line1StdOut;
		System.out.println(executable);
		while (true) {
			line1StdOut = proc1StdOut.readLine();
			if (line1StdOut == null) break;
			else s.add(line1StdOut);
			try {
				Thread.sleep(2);
			} catch (Exception e) {

			}
		}
		
		try {
			Thread.sleep(5);
		} catch (Exception e) {

		}
		p.destroy();
		return s;
	}
}
