/**
 * Developed by Andrew Pethick - AMPGeoscience 2015
 * Cut down from Omnium base version for use in Geowiz project
 * Converted to WTFPL
 */
package del.mt.geowiz.omnium.parameters;

import javax.measure.quantity.Dimensionless;

import del.mt.geowiz.omnium.parameters.representations.ParameterRepresentation;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.FloatProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;

public abstract class Parameter<E extends Property> extends ParameterType implements InformationDynamic {

	private final Class c;
	private final StringProperty key;
	
	private final ParameterProperty<E> value;
	private final ParameterProperty<E> defaultValue;
	
	public final Information info;

	public ParameterRepresentation representation;
	public ObservableList<ParameterProperty<?>> options = FXCollections.observableArrayList();	

	private ParameterProperty selectedOption;


	
	
	public Parameter(Class c, Information details,  ParameterProperty<E> defaultValue, ParameterProperty<E> value, boolean isOptional, ParameterRepresentation representation) {
		this.info = details;		
		this.representation = representation;
		this.value = value;
		this.defaultValue = defaultValue;
		this.c = c;
		this.key = new SimpleStringProperty(c.getName());
		addValueChangeListener();
	}
	

	
	public String getKey() {
		return this.key.get();
	}
	public StringProperty getKeyProperty() {
		return this.key;
	}

	private void addValueChangeListener() {
		addHistoryListener();
	}

	
	private void addHistoryListener() {

	}
	
	public StringProperty getName() {
		return info.getName();
	}

	public StringProperty getShortname() {
		return info.getShortname();
	}

	public StringProperty getDescription() {
		return info.getDescription();
	}

	public BooleanProperty getEnabled() {
		return isEnabled;
	}

	public StringProperty getSupportLink() {
		return info.getSupportLink();
	}



	public boolean isEditable() {
		return isEditable.get();
	}

	public ParameterProperty<E> getValue() {
		return value;
	}

	public ParameterProperty<E> getDefaultValue() {
		return defaultValue;
	}

	@Override
	public String toString() {
		String s = info.getName().get() + " [" + info.getShortname().get() + "]";
		return (s + "\t (" + getValue().getValuePrimative() + ")" + "\t" + info.getDescription().get());
	}

	public abstract void setValueFromString(String string);
	

	public abstract String getStringFromValue();

	public void setEditable(boolean isEditable) {
		this.isEditable.set(isEditable);
	}

	public final Tooltip getTooltip() {
		StringProperty name = this.getName();
		StringProperty desc = this.getDescription();
		StringProperty shortname = this.getShortname();
		Tooltip tooltip = new Tooltip(this.info.getDescription().get() + " (" + shortname.get() + " )");
		

		
		
		if (this instanceof ParameterNumerical) {
			ParameterNumerical number = (ParameterNumerical) this;
			if(!number.getUnit().equals(Dimensionless.UNIT)) {
				String units = " (" + "Units" + " = "+ number.getUnit().toString() + ")";
				tooltip.textProperty().bind(Bindings.concat(this.info.getDescription()).concat(units).concat("(").concat(shortname).concat(")"));			
			} else {
				tooltip.textProperty().bind(Bindings.concat(this.info.getDescription()).concat("(").concat(shortname).concat(")"));
			}
		} else {
			tooltip.textProperty().bind(Bindings.concat(this.info.getDescription()).concat("(").concat(shortname).concat(")"));
		}
		
		
		
		tooltip.setWrapText(true);
		tooltip.setMaxWidth(300);
		
		return tooltip;
	}

	public Label getLabel() {
		Label title = new Label(info.getName().get());
//		Tooltip tooltip = new Tooltip(info.getDescription().get());
		title.textProperty().bindBidirectional(info.getName());
//		tooltip.textProperty().bindBidirectional(info.getDescription());

		title.setMinWidth(150);
		title.setTooltip(getTooltip());
//		title.setFont(Font.font("System Bold", 10));
		return title;
	}
	public Label getDescriptionLabel() {
		Label description = new Label(info.getDescription().get());		
		description.textProperty().bindBidirectional(info.getDescription());
		description.setWrapText(true);
		description.setMinWidth(150);
		description.setTooltip(getTooltip());
//		title.setFont(Font.font("System Bold", 10));
		return description;
	}
	public Label getShortnameLabel() {
		Label description = new Label(info.getShortname().get());		
		description.textProperty().bindBidirectional(info.getShortname());
		description.setWrapText(true);
		description.setMinWidth(150);
		description.setTooltip(getTooltip());
//		title.setFont(Font.font("System Bold", 10));
		return description;
	}

	public void addOptions(ParameterProperty... options) {
		for (ParameterProperty o : options) {
			addOption(o);
		}
	}
	

	public ParameterProperty getSelectedOption() {
		return selectedOption;
	}

	public void setOption(ParameterProperty o) {

		Property value = (Property) o.value;
	
		if (value instanceof BooleanProperty)
			((PropertyBoolean) this.value).getValuePrimative().setValue(((BooleanProperty) value).get());
		else if (value instanceof StringProperty)
			((PropertyString) this.value).getValuePrimative().setValue(((StringProperty) value).get());
		else if (value instanceof DoubleProperty)
			((PropertyDouble) this.value).getValuePrimative().setValue(((DoubleProperty) value).get());
		else if (value instanceof IntegerProperty)
			((PropertyInteger) this.value).getValuePrimative().setValue(((IntegerProperty) value).get());
		else if (value instanceof FloatProperty)
			((PropertyFloat) this.value).getValuePrimative().setValue(((FloatProperty) value).get());
		else {
			System.err.println("Value type " + value.getClass() + " not found");
		}
		this.selectedOption = o;
	}
	
	public static void setValue(ParameterProperty prop, Object obj) {
		
		switch (prop.getPropertyType()) {
		case TYPE_BOOLEAN: ((PropertyBoolean) prop).getValuePrimative().set((Boolean) obj); break;
		case TYPE_DOUBLE: ((PropertyDouble) prop).getValuePrimative().set((Double) obj); break;
		case TYPE_FLOAT: ((PropertyFloat) prop).getValuePrimative().set((Float) obj); break;		
		case TYPE_INTEGER: ((PropertyInteger) prop).getValuePrimative().set((Integer) obj); break;
		case TYPE_STRING: ((PropertyString) prop).getValuePrimative().set((String) obj); break;
		case TYPE_FILE: ((PropertyString) prop).getValuePrimative().set((String) obj); break;		
			
		default:			
			break;
		}
	}

	public void addOption(ParameterProperty option) {
		if (!options.contains(option)) {
			options.add(option);
		}
	}

	public void removeOption(ParameterProperty option) {
		options.remove(option);
	}

	public Class getType() {
		return super.getClass();
	}

	public Node createNode() {
		return representation.createNode(this);
	}
	public void setRepresentation(ParameterRepresentation p) {
		this.representation = p;
	}
	public Property getProperty() {
		return (Property) value.getProperty();
	}

	public Information getInformation() {
		return info;
	}

	public void setToDefault() {
		((ParameterProperty<E>) value).setValue(((ParameterProperty<E>) defaultValue).getProperty());
	}

	public abstract int getParameterType();



	// public byte [] serialize() {
	// switch (getParameterType()) {
	// case TYPE_BOOLEAN: return SerializationUtils.serialize(new
	// Boolean(((BooleanProperty)this.getProperty()).get()));
	// case TYPE_DOUBLE: return SerializationUtils.serialize(new
	// Double(((DoubleProperty)this.getProperty()).get()));
	// case TYPE_FLOAT: return SerializationUtils.serialize(new
	// Float(((FloatProperty)this.getProperty()).get()));
	// case TYPE_LONG: return SerializationUtils.serialize(new
	// Long(((LongProperty)this.getProperty()).get()));
	// case TYPE_INTEGER: return SerializationUtils.serialize(new
	// Integer(((IntegerProperty)this.getProperty()).get()));
	// case TYPE_STRING: return SerializationUtils.serialize(new
	// String(((StringProperty)this.getProperty()).get()));
	// // case TYPE_FILE: return SerializationUtils.serialize(new
	// Double(((File)this.getProperty()).get()));
	// case TYPE_OBJECT: {
	//
	// }
	// default:
	// break;
	// }
	// resources.error(new Exception("Unknown Type for Serialization"));
	// return null;
	// }
	// public static void read(Parameter p, byte [] data) {
	// switch (p.getParameterType()) {
	// case TYPE_BOOLEAN: ((BooleanProperty) p.getProperty()).set((Boolean)
	// SerializationUtils.deserialize(data)); break;
	// case TYPE_DOUBLE: ((DoubleProperty) p.getProperty()).set((Double)
	// SerializationUtils.deserialize(data));break;
	// case TYPE_FLOAT: ((FloatProperty) p.getProperty()).set((Float)
	// SerializationUtils.deserialize(data));break;
	// case TYPE_LONG: ((LongProperty) p.getProperty()).set((Long)
	// SerializationUtils.deserialize(data));break;
	// case TYPE_INTEGER: ((IntegerProperty) p.getProperty()).set((Integer)
	// SerializationUtils.deserialize(data));break;
	// case TYPE_STRING: ((StringProperty) p.getProperty()).set((String)
	// SerializationUtils.deserialize(data));break;
	//
	// // case TYPE_FILE: return SerializationUtils.serialize(new
	// Double(((File)this.getProperty()).get()));
	// case TYPE_OBJECT: {
	//
	// } default : break;
	// }
	// resources.error(new Exception("Unknown Type for Serialization " +
	// p.toString()));
	// }

}
