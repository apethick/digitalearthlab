/**
 * Developed by Andrew Pethick - AMPGeoscience 2015
 * Cut down from Omnium base version for use in Geowiz project
 * Converted to WTFPL
 */
package del.mt.geowiz.omnium.parameters;

import javax.measure.unit.Unit;

import javafx.beans.property.FloatProperty;
import javafx.beans.property.SimpleFloatProperty;

public class PropertyFloat extends PropertyNumerical<FloatProperty> {

	public PropertyFloat(Information information, float value, Unit unit) {
		super(information, new SimpleFloatProperty(value), unit);
	}

	@Override
	public int getPropertyType() {
		return TYPE_FLOAT;
	}

	@Override
	public Number getNumber() {
		return ((FloatProperty) super.value).get();
	}
	public void set(Float v) {
		super.value.set(v);
	}

	@Override
	public void setValue(FloatProperty v) {
		set(v.get());
	}
}
