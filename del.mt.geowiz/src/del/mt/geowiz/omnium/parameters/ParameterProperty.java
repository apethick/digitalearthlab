/**
 * Developed by Andrew Pethick - AMPGeoscience 2015
 * Cut down from Omnium base version for use in Geowiz project
 * Converted to WTFPL
 */
package del.mt.geowiz.omnium.parameters;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleBooleanProperty;

public abstract class ParameterProperty<E extends Property> extends ParameterType implements InformationDynamic {
	public Information information;
	protected final E value;
	public BooleanProperty selectedProperty = new SimpleBooleanProperty(false);
	
	private boolean forDelete = false; 
	public ParameterProperty(Information information, E value) {
		this.information = information;
		this.value = value;
	}

	@Override
	public Information getInformation() {
		return information;
	}

	public E getProperty() {
		return value;
	}

	@Override
	public String toString() {
		return information.getName().get();
	}

	public abstract int getPropertyType();




	public E getValuePrimative() {
		return value;
	}

	public abstract void setValue(E v);




	public void setDeleteStatus(boolean isDelete) {
		forDelete = isDelete;
	}
	public boolean getDeleteStatus() {
		return this.forDelete;
	}

	

}
