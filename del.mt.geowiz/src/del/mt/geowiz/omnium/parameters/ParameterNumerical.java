/**
 * Developed by Andrew Pethick - AMPGeoscience 2015
 * Cut down from Omnium base version for use in Geowiz project
 * Converted to WTFPL
 */
package del.mt.geowiz.omnium.parameters;

import javax.measure.unit.Unit;

import del.mt.geowiz.omnium.parameters.representations.ParameterRepresentation;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

public abstract class ParameterNumerical<E> extends Parameter{

	private ObjectProperty<Unit> unit = new SimpleObjectProperty<Unit>();	
	
	public ParameterNumerical(Class c, Information information,	Unit unit, PropertyNumerical defaultValue, PropertyNumerical value, boolean isOptional,	ParameterRepresentation representation) {
		super(c, information,  defaultValue, value, isOptional, representation);
		this.unit.set(unit);
	}
	public Unit getUnit() {
		return unit.get();
	}
	public ObjectProperty<Unit> getUnitProperty() {
		return unit;
	}
	public void setUnit(Unit unit) {
		this.unit.set(unit);
	}
	public  abstract Number getNumber();
	

}
