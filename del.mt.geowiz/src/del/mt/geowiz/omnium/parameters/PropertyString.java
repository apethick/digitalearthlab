/**
 * Developed by Andrew Pethick - AMPGeoscience 2015
 * Cut down from Omnium base version for use in Geowiz project
 * Converted to WTFPL
 */
package del.mt.geowiz.omnium.parameters;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PropertyString extends ParameterProperty<StringProperty> {

	public PropertyString(Information information, String value) {
		super(information, new SimpleStringProperty(value));
	}

	@Override
	public int getPropertyType() {
		return TYPE_STRING;
	}
	@Override
	public String toString() {
		return value.get();
	}
	public void set(String s) {
		value.set(s);
	}

	@Override
	public void setValue(StringProperty v) {
		set(v.get());
	}

}
