/**
 * Developed by Andrew Pethick - AMPGeoscience 2015
 * Cut down from Omnium base version for use in Geowiz project
 * Converted to WTFPL
 */
package del.mt.geowiz.omnium.parameters;

import java.io.File;
import java.util.ArrayList;

import del.mt.geowiz.omnium.parameters.representations.ParameterRepresentation;

public class ParameterFile extends ParameterString{

	private final String [] SEPARATORS = {",",";","~"};
	
	
	public ParameterFile(Information info, String defaultValue, String value, boolean isOptional, ParameterRepresentation representation) {
		super(info,  defaultValue, value, isOptional, representation);				
	}

	public File getFile() {
		String path = get().get();
		return new File(path);
	}
	public ArrayList<File> getFiles()  {
		String s = get().get();
		for(String separator : SEPARATORS) {
			s = s.replaceAll(separator, "\t");	
		}

		String [] fileStrings = s.split("\t");
		ArrayList<File> files = new ArrayList<File>();
		for(String fs : fileStrings) {

			try {
				File f = new File(fs);				
				if(f.exists()) files.add(f);				
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		return files;
		
	}
		
	
	@Override
	public int getParameterType() {
		return Parameter.TYPE_FILE;
	}
}
