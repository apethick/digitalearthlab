/**
 * Developed by Andrew Pethick - AMPGeoscience 2015
 * Cut down from Omnium base version for use in Geowiz project
 * Converted to WTFPL
 */
package del.mt.geowiz.omnium.parameters;

import javax.measure.unit.Unit;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

public class PropertyDouble extends PropertyNumerical<DoubleProperty> {

	public PropertyDouble(Information information, double value, Unit unit) {
		super(information, new SimpleDoubleProperty(value), unit);
	}

	@Override
	public int getPropertyType() {
		return TYPE_DOUBLE;
	}

	@Override
	public Number getNumber() {
		return ((DoubleProperty) super.value).get();
	}
	public void set(Double v) {
		super.value.set(v);
	}

	@Override
	public void setValue(DoubleProperty v) {
		set(v.get());
	}
}
