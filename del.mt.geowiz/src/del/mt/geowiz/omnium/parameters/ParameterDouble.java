/**
 * Developed by Andrew Pethick - AMPGeoscience 2015
 * Cut down from Omnium base version for use in Geowiz project
 * Converted to WTFPL
 */
package del.mt.geowiz.omnium.parameters;

import javax.measure.Measure;
import javax.measure.unit.Unit;

import del.mt.geowiz.omnium.parameters.representations.ParameterRepresentation;
import javafx.beans.property.DoubleProperty;

public class ParameterDouble extends ParameterNumerical<DoubleProperty> {

	public ParameterDouble(Information info, Unit units, double defaultValue, double value, boolean isOptional,ParameterRepresentation representation) {
		super(ParameterDouble.class, info, units,  new PropertyDouble(info,defaultValue, units), new PropertyDouble(info,value, units), isOptional, representation);
	}
	public void set(double value) {
		((DoubleProperty)this.getProperty()).set(value);
	}
	@Override
	public void setValueFromString(String string) {
		set(Double.valueOf(string));
	}
	public DoubleProperty get() {
		return ((DoubleProperty)this.getProperty());
	}
	public double get(Unit u) {
		if(getUnit().isCompatible(u)) {
			return Measure.valueOf(get().get(), this.getUnit()).doubleValue(u);	
		} else {
			//create global list of converters for units
			return Measure.valueOf(get().get(), this.getUnit()).doubleValue(u);	
		}
		
	}

	@Override
	public Number getNumber() {
		return get().doubleValue();
	}
	@Override
	public String getStringFromValue() {
		return ("" + get().doubleValue());
				
	}
	@Override
	public int getParameterType() {
		return Parameter.TYPE_DOUBLE;
	}
	
}
