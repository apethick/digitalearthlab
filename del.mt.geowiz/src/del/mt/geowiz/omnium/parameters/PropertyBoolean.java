/**
 * Developed by Andrew Pethick - AMPGeoscience 2015
 * Cut down from Omnium base version for use in Geowiz project
 * Converted to WTFPL
 */
package del.mt.geowiz.omnium.parameters;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

public class PropertyBoolean extends ParameterProperty<BooleanProperty> {	
	public PropertyBoolean(Information information, boolean value) {
		super(information, new SimpleBooleanProperty(value));
	}

	@Override
	public int getPropertyType() {
		return TYPE_BOOLEAN;
	}
	@Override
	public String toString() {
		return (value.get() ? "true" : "false");
	}

	public void set(Boolean v) {
		this.value.set(v);
	}
	@Override
	public void setValue(BooleanProperty v) {
		set(v.getValue());
	}

}
