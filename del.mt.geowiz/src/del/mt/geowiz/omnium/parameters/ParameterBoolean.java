/**
 * Developed by Andrew Pethick - AMPGeoscience 2015
 * Cut down from Omnium base version for use in Geowiz project
 * Converted to WTFPL
 */
package del.mt.geowiz.omnium.parameters;

import del.mt.geowiz.omnium.parameters.representations.ParameterRepresentation;
import javafx.beans.property.BooleanProperty;

public class ParameterBoolean extends Parameter<BooleanProperty>{


	
	public ParameterBoolean(Information info, boolean defaultValue, boolean value, boolean isOptional, ParameterRepresentation representation) {
		super(ParameterBoolean.class, info, new PropertyBoolean(info,defaultValue), new PropertyBoolean(info,value), isOptional, representation);
	}

	public void set(boolean value) {
		((BooleanProperty)this.getProperty()).set(value);
	}
	public void setDefault(boolean value) {
		(this.getDefaultValue()).getValuePrimative().set(value);
	}
	
	
	@Override
	public void setValueFromString(String string) {
		if(string.toUpperCase().contains("TRUE")) {	set(true); return; }
		if(string.toUpperCase().contains("FALSE")) {set(false); return;}
		if(string.toUpperCase().contains("YES")) {set(true); return;}
		if(string.toUpperCase().contains("NO")) {set(false); return;}
		if(string.toUpperCase().contains("T")) {set(true); return;}
		if(string.toUpperCase().contains("F")) {set(false); return;}
		if(string.toUpperCase().contains("Y")) {set(true); return;}
		if(string.toUpperCase().contains("N")) {set(false); return;}
		 throw new Error("UNKNOWN " + string);
	}

		public BooleanProperty get() {
		return ((BooleanProperty)this.getProperty());
	}

		@Override
		public String getStringFromValue() {
			return (get().get() ? "T" : "F"); 
		}
//
//		@Override
//		public byte[] serialize() {
//			byte[] data = SerializationUtils.serialize(new Boolean(get().get()));
//			return data;
//		}
//
//		@Override
//		public void read(byte[] data) {
//			Boolean b = (Boolean) SerializationUtils.deserialize(data);
//			set(b);
//		}

		@Override
		public int getParameterType() {
			return Parameter.TYPE_BOOLEAN;
		}

		



	

}
