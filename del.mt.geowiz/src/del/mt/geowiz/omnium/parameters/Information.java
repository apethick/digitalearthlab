/**
 * Developed by Andrew Pethick - AMPGeoscience 2015
 * Cut down from Omnium base version for use in Geowiz project
 * Converted to WTFPL
 */
package del.mt.geowiz.omnium.parameters;

import javafx.beans.property.FloatProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.Tab;
import javafx.scene.control.Tooltip;

public class Information {
	// only used for database entry, not for internal referencing.
	// READ ONLY PROPERTIES
	protected final StringProperty name = new SimpleStringProperty();
	protected final StringProperty shortname = new SimpleStringProperty();
	protected final FloatProperty version = new SimpleFloatProperty(1.00f);
	protected final StringProperty date = new SimpleStringProperty("01/01/1999");
	protected final StringProperty comments = new SimpleStringProperty("");
	protected final StringProperty description = new SimpleStringProperty();
	protected final StringProperty supportLink = new SimpleStringProperty();

	public final static Information createInstance(String name, String shortname, String description) {
		return new Information(description, shortname, description) {
		};
	}

	public final static Information createInstance(String name, String shortname, String description, String link) {
		Information i = new Information(description, shortname, description);
		i.setSupport(link);
		return i;
	}

	public Information(String name, String shortname, String description) {
		this.name.set(name);
		this.shortname.set(shortname);
		this.description.set(description);
	}

	public Information(StringProperty name, StringProperty shortname, StringProperty description) {
		this.name.set(name.get());
		this.shortname.set(shortname.get());
		this.description.set(description.get());
		this.name.bindBidirectional(name);
		this.shortname.bindBidirectional(shortname);
		this.description.bindBidirectional(description);
	}

	public Information(Information info) {

		this.name.set(info.getName().get());
		this.shortname.set(info.getShortname().get());
		this.version.set(info.getVersion().get());
		this.date.set(info.getDate().get());
		this.comments.set(info.getComments().get());
		this.description.set(info.getDescription().get());
		this.name.bindBidirectional(info.getName());
		this.shortname.bindBidirectional(info.getShortname());
		this.version.bindBidirectional(info.getVersion());
		this.date.bindBidirectional(info.getDate());
		this.comments.bindBidirectional(info.getComments());
		this.description.bindBidirectional(info.getDescription());

	}
	public void sync(Information info) {
		set(info);
	}
	public void setName(String name) {
		this.name.set(name);
	}
	public void setDescription(String description) {
		this.description.set(description);
	}
	public void set(Information info) {
		this.name.set(info.getName().get());
		this.shortname.set(info.getShortname().get());
		this.version.set(info.getVersion().get());
		this.date.set(info.getDate().get());
		this.comments.set(info.getComments().get());
		this.description.set(info.getDescription().get());
	}

	public void setVersionInfo(float version, String date, String comments) {
		this.version.set(version);
		this.date.set(date);
		this.comments.set(comments);
	}

	public void setShortname(String shortname) {
		this.shortname.set(shortname);
	}

	public void set(String name, String shortname, String description) {
		this.name.set(name);
		this.shortname.set(shortname);
		this.description.set(description);
	}

	public void setSupport(String support) {
		this.supportLink.set(support);
	}

	public StringProperty getSupportLink() {
		return supportLink;
	}

	public StringProperty getName() {
		return name;
	}

	public StringProperty getShortname() {
		return shortname;
	}

	public FloatProperty getVersion() {
		return version;
	}

	public StringProperty getDate() {
		return date;
	}

	public StringProperty getComments() {
		return comments;
	}

	public StringProperty getDescription() {
		return description;
	}

	public String toString() {
		return name.get() + "\t[" + getShortname().get() + "]\t" + "(v" + getVersion().get() + "|" + getDate().get() + ") - " + getDescription().get() + " - " + getComments().get();
	}

	public void bindBidirectional(Information information) {
		this.name.bindBidirectional(information.name);
		this.shortname.bindBidirectional(information.name);
		this.version.bindBidirectional(information.version);
		this.date.bindBidirectional(information.date);
		this.comments.bindBidirectional(information.comments);
		this.description.bindBidirectional(information.description);
		this.supportLink.bindBidirectional(information.supportLink);
	}

	public void initNode(Labeled l) {
		l.textProperty().set(this.name.get());
		l.textProperty().bind(this.name);
		Tooltip t=  new Tooltip(getDescription().get());
		t.textProperty().bind(getDescription());
		l.setTooltip(t);		
	}
	public void initNode(Tab l) {
		l.textProperty().set(this.name.get());
		l.textProperty().bind(this.name);
		Tooltip t=  new Tooltip(getDescription().get());
		t.textProperty().bind(getDescription());
		l.setTooltip(t);		
	}


}
