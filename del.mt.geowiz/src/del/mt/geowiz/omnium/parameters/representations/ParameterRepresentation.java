/**
 * Developed by Andrew Pethick - AMPGeoscience 2015
 * Cut down from Omnium base version for use in Geowiz project
 * Converted to WTFPL
 */
package del.mt.geowiz.omnium.parameters.representations;

import javax.measure.quantity.Dimensionless;
import javax.measure.unit.Unit;

import del.mt.geowiz.omnium.parameters.Parameter;
import del.mt.geowiz.omnium.parameters.ParameterNumerical;
import del.mt.geowiz.omnium.parameters.axis.Axis;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.StringExpression;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.control.Tooltip;

public abstract class ParameterRepresentation {

	
	public static final ParameterRepresentation SINGLE_LABEL = new SingleLabelRepresentation();	
	public static final ParameterRepresentation SINGLE_CHECKBOX = new SingleBooleanCheckboxRepresentation();
	public static final ParameterRepresentation SINGLE_COMBO = new SingleComboRepresentation();
//	public static final ParameterRepresentation SINGLE_SLIDER = new SingleSliderRepresentation();	
	public static final ParameterRepresentation CUSTOM_TEXT = new CustomTextboxRepresentation();
	

	
	public BooleanProperty visibleNameLabel = new SimpleBooleanProperty(true);
	public BooleanProperty visibleDescLabel = new SimpleBooleanProperty(true);

	
	public abstract ParameterRepresentationNode createNode(Parameter p);


	public final static Tooltip getTooltip(Parameter p) {
		StringProperty name = p.getName();
		StringProperty desc = p.getDescription();
		StringProperty shortname = p.getShortname();
		Tooltip tooltip = new Tooltip(p.info.getDescription().get() + " (" + shortname.get() + " )");
		

		
		
		if (p instanceof ParameterNumerical) {
			ParameterNumerical number = (ParameterNumerical) p;
			if(!number.getUnit().equals(Dimensionless.UNIT)) {				
				String units = " (" + "Units" + " = "+ number.getUnit().toString() + ")";
				tooltip.textProperty().bind(Bindings.concat(p.info.getDescription()).concat(units).concat("(").concat(shortname).concat(")"));
				((ParameterNumerical) p).getUnitProperty().addListener(new ChangeListener<Unit>() {

					@Override
					public void changed(ObservableValue<? extends Unit> observable, Unit oldValue, Unit newValue) {
						String units = " (" + "Units" + " = "+ number.getUnit().toString() + ")";
						tooltip.textProperty().bind(Bindings.concat(p.info.getDescription()).concat(units).concat("(").concat(shortname).concat(")"));
					}
				});
			} else {
				tooltip.textProperty().bind(Bindings.concat(p.info.getDescription()).concat("(").concat(shortname).concat(")"));
			}
		} else {
			tooltip.textProperty().bind(Bindings.concat(p.info.getDescription()).concat("(").concat(shortname).concat(")"));
		}
		
		
		
		tooltip.setWrapText(true);
		tooltip.setMaxWidth(300);
		
		return tooltip;
	}
	public final static StringExpression getPromptText(Parameter p) {
		if (p instanceof ParameterNumerical) {
			ParameterNumerical number = (ParameterNumerical) p;
			if(!number.getUnit().equals(Dimensionless.UNIT)) {
				String units = "     (" + number.getUnit().toString() + ")";
				return Bindings.concat(p.info.getName()).concat(units);
			} else {
				return p.info.getName();
			}
		} else {
			 return p.info.getName();
		}
	}

	

}
