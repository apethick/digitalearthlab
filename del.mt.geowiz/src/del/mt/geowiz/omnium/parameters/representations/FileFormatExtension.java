/**
 * Developed by Andrew Pethick - AMPGeoscience 2015
 * Cut down from Omnium base version for use in Geowiz project
 * Converted to WTFPL
 */
package del.mt.geowiz.omnium.parameters.representations;

import del.mt.geowiz.omnium.parameters.Information;


public class FileFormatExtension extends Information{

	public FileFormatExtension(Information info) {
		super(info);
	}
	public FileFormatExtension(String name, String extension, String description) {
		super(name,extension,description);		
	}
	 
	
}
