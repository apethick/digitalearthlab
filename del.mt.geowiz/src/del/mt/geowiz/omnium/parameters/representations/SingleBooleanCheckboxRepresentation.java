/**
 * Developed by Andrew Pethick - AMPGeoscience 2015
 * Cut down from Omnium base version for use in Geowiz project
 * Converted to WTFPL
 */
package del.mt.geowiz.omnium.parameters.representations;

import del.mt.geowiz.omnium.parameters.Parameter;
import del.mt.geowiz.omnium.parameters.ParameterBoolean;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.BooleanProperty;
import javafx.scene.control.CheckBox;

public class SingleBooleanCheckboxRepresentation extends ControlRepresentation {
	

	@Override
	public ParameterRepresentationNode createNode(Parameter p) {
		
		if (p instanceof ParameterBoolean) {
			CheckBox box = new CheckBox();
			box.textProperty().bindBidirectional(p.getName());			
			box.setTooltip(getTooltip(p));
			box.selectedProperty().bindBidirectional((BooleanProperty) p.getProperty());
			box.disableProperty().bind(BooleanBinding.booleanExpression(p.isEditable).not());
			
			return super.createNode(box, p);
			// return ;
		} else {
			
			return null;
		}
	}
}
