/**
 * Developed by Andrew Pethick - AMPGeoscience 2015
 * Cut down from Omnium base version for use in Geowiz project
 * Converted to WTFPL
 */
package del.mt.geowiz.omnium.parameters.representations;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.swing.JFileChooser;

import del.mt.geowiz.fontawesome.FontAwesome;
import del.mt.geowiz.omnium.parameters.Parameter;
import del.mt.geowiz.omnium.parameters.ParameterDouble;
import del.mt.geowiz.omnium.parameters.ParameterInteger;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.BorderPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.util.StringConverter;
import javafx.util.converter.DoubleStringConverter;
import javafx.util.converter.IntegerStringConverter;

public class SingleDirectoryRepresentation extends ControlRepresentation {	
	private ArrayList<FileFormat> formats;
	private String message;

	public SingleDirectoryRepresentation(String message, ArrayList<FileFormat> formats) {

		this.message = message;
		this.formats = formats;
	}
	
	@Override
	public ParameterRepresentationNode createNode(final Parameter p) {
		return createNode(p, false, false);
	}

	public ParameterRepresentationNode createNode(final Parameter p, boolean centredLabel, boolean centredContent) {
		BorderPane pane = new BorderPane();
		TextField f = new TextField();
		f.setMaxWidth(Double.MAX_VALUE);
		f.editableProperty().bindBidirectional(p.isEditable);
		if (p instanceof ParameterInteger) {
			StringConverter<? extends Number> converter = new IntegerStringConverter();
			Bindings.bindBidirectional(f.textProperty(), p.getProperty(), (StringConverter<Number>) converter);
		} else if (p instanceof ParameterDouble) {
			StringConverter<? extends Number> converter = new DoubleStringConverter();
			Bindings.bindBidirectional(f.textProperty(), p.getProperty(), (StringConverter<Number>) converter);
		} else {
			f.textProperty().bindBidirectional(p.getProperty());
		}
		f.promptTextProperty().bindBidirectional(p.info.getName());
		f.setTooltip(p.getTooltip());
		

		

	
		f.setAlignment(centredContent ? Pos.CENTER : Pos.CENTER_LEFT);
		Tooltip tooltip = new Tooltip(p.info.getDescription().get());
		tooltip.textProperty().bindBidirectional(p.info.getDescription());
	
		Button b = new Button("", 	FontAwesome.FOLDER_OPEN());
		b.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				String key = p.getClass().getName() + "." + p.getInformation().getShortname().get();
				ArrayList<File> files = FileChooserUtils.filechooser(key, false, false, false, formats);
				if(files.size() >= 1) {
					f.setText(files.get(0).getAbsolutePath() + "/");
				}	
			}
		});
		pane.setCenter(f);
		pane.setRight(b);
		return super.createNode(pane, p);
	}

	
	
}
