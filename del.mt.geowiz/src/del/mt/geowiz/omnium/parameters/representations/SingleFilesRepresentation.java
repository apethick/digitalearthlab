/**
 * Developed by Andrew Pethick - AMPGeoscience 2015
 * Cut down from Omnium base version for use in Geowiz project
 * Converted to WTFPL
 */
package del.mt.geowiz.omnium.parameters.representations;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

import del.mt.geowiz.fontawesome.FontAwesome;
import del.mt.geowiz.omnium.parameters.Parameter;
import del.mt.geowiz.omnium.parameters.ParameterDouble;
import del.mt.geowiz.omnium.parameters.ParameterFile;
import del.mt.geowiz.omnium.parameters.ParameterInteger;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.util.StringConverter;
import javafx.util.converter.DoubleStringConverter;
import javafx.util.converter.IntegerStringConverter;

public class SingleFilesRepresentation extends ControlRepresentation {
	
	private ArrayList<FileFormat> formats;
	private String message;
	private final boolean isLoad;
	private boolean enableMultiple;

	public SingleFilesRepresentation(String message, boolean isLoad, boolean enableMultiple, ArrayList<FileFormat> formats) {

		this.message = message;
		this.formats = formats;
		this.isLoad = isLoad;
		this.enableMultiple = enableMultiple;
	}
	
	@Override
	public ParameterRepresentationNode createNode(final Parameter p) {
		return super.createNode(createNode(p, false, false), p);
	}

	public BorderPane createNode(final Parameter p, boolean centredLabel, boolean centredContent) {
		BorderPane pane = new BorderPane();
		
		TextField f = new TextField();
		
		f.editableProperty().bindBidirectional(p.isEditable);
		if (p instanceof ParameterInteger) {
			StringConverter<? extends Number> converter = new IntegerStringConverter();
			Bindings.bindBidirectional(f.textProperty(), p.getProperty(), (StringConverter<Number>) converter);
		} else if (p instanceof ParameterDouble) {
			StringConverter<? extends Number> converter = new DoubleStringConverter();
			Bindings.bindBidirectional(f.textProperty(), p.getProperty(), (StringConverter<Number>) converter);
		}  else {
			f.textProperty().bindBidirectional(p.getProperty());
		}
		f.promptTextProperty().bindBidirectional(p.info.getName());
		f.setTooltip(p.getTooltip());
		

//		Label l = v.label;
//		l.setAlignment(centredLabel ? Pos.CENTER : Pos.CENTER_LEFT);
//		f.setAlignment(centredContent ? Pos.CENTER : Pos.CENTER_LEFT);
//		Tooltip tooltip = new Tooltip(p.info.getDescription().get());
//		tooltip.textProperty().bindBidirectional(p.info.getDescription());
//		l.setTooltip(tooltip);
		Button b = new Button("", 	FontAwesome.FOLDER_OPEN());
		b.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				String key = p.getClass().getName() + "." + p.getInformation().getShortname().get();
				ArrayList<File> files = FileChooserUtils.filechooser(key, !isLoad, true, enableMultiple, formats);
				if(files.size() >= 1) {
					String s = "";
					Iterator<File> fi = files.iterator();
					while(fi.hasNext()) {
						File f = fi.next();
						s += f.getAbsolutePath();
						if(fi.hasNext()) s += ",";		
					}
					if(p instanceof ParameterFile) {
						ParameterFile pf = (ParameterFile) p;
						pf.getDefaultValue().getProperty().set(s);
						pf.getValue().getProperty().set(s);
					}
					f.setText(s);
				}	
			}
		});
		
		
		
		pane.setCenter(f);
		pane.setRight(b);
		return pane;
	}

}
