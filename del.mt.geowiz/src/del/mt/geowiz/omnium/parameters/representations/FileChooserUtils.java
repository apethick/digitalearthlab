/**
 * Developed by Andrew Pethick - AMPGeoscience 2015
 * Cut down from Omnium base version for use in Geowiz project
 * Converted to WTFPL
 */
package del.mt.geowiz.omnium.parameters.representations;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.swing.JFileChooser;

import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;

public class FileChooserUtils {
	// ************HISTORY************************************
	public ArrayList<File> filechooser(String key, boolean isSave, boolean isFile, boolean enableMultipleSelection, FileFormat... fformats) {
		ArrayList<FileFormat> formats = new ArrayList<>(Arrays.asList(fformats));
		return filechooser(key, isSave, isFile, enableMultipleSelection, formats);
	}
	public static ArrayList<File> filechooser(String key, boolean isSave, boolean isFile, boolean enableMultipleSelection, ArrayList<FileFormat> formats) {
		File propertiesFile = new File("history.filechooser.CONFIG");
		Properties properties = new Properties();
		String path = null;
		String resourcekey = "dir";

		try {
			properties.load(new FileInputStream(propertiesFile));
			path = getDefaultDir(resourcekey, properties);

			if (isFile) {
				FileChooser c = new FileChooser();
				String title = (isSave ? "Save" : "Load") + " ";
				for (FileFormat f : formats) {
					c.getExtensionFilters().addAll(f.getExtensionFilter());
					title += f.getInformation().getName().get() + " ";
				}
				title += "files";
				c.setTitle(title);
				if (path != null)
					c.setInitialDirectory(new File(path));
				ArrayList<File> list = new ArrayList<File>();
				if (enableMultipleSelection) {
					List<File> l = c.showOpenMultipleDialog(null);
					if (l == null)
						return list;
					for (File f : l) {
						list.add(f);
					}
				} else {
					File f = isSave ? c.showSaveDialog(null) : c.showOpenDialog(null);
					if (f == null)
						return list;
					list.add(f);
				}

				if (list.get(0) == null)
					return list;

				String dir = (list.get(0).isDirectory()) ? list.get(0).getCanonicalPath() : list.get(0).getParentFile().getCanonicalPath();

				properties.setProperty(resourcekey, dir);
				properties.setProperty(previouskey, dir);
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(propertiesFile));
				properties.store(stream, "Omnium File Chooser History");
				stream.close();
				return list;
			} else {
				ArrayList<File> list = new ArrayList<File>();
				if (enableMultipleSelection) {
					JFileChooser c = new JFileChooser(new File(path));
					c.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
					c.setMultiSelectionEnabled(true);
					int returnVal = c.showOpenDialog(null);
					if (returnVal == JFileChooser.APPROVE_OPTION) {
						for (File f : c.getSelectedFiles()) {
							list.add(f);
						}
					}
				} else {
					DirectoryChooser c = new DirectoryChooser();
					String title = "Load " + (enableMultipleSelection ? " Directories" : " Directory");
					c.setTitle(title);
					c.setInitialDirectory(new File(path));
					File f = c.showDialog(null);
					if (f == null)
						return list;
					list.add(f);
				}

				if (list.get(0) == null)
					return list;

				String dir = (list.get(0).isDirectory()) ? list.get(0).getCanonicalPath() : list.get(0).getParentFile().getCanonicalPath();

				properties.setProperty(resourcekey, dir);
				properties.setProperty(previouskey, dir);
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(propertiesFile));
				properties.store(stream, "Omnium File Chooser History");
				stream.close();

				return list;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new ArrayList<File>();

	}
	private static String previouskey = "previousdir";
	private static String defaultdir = null;
	
	private static String getDefaultDir(String resourcekey, Properties properties) {

		if (properties.getProperty(resourcekey) != null) {

			if (new File(properties.getProperty(resourcekey)).exists())
				return properties.getProperty(resourcekey);
		}
		if (properties.getProperty(previouskey) != null) {
			if (new File(properties.getProperty(previouskey)).exists())
				return properties.getProperty(previouskey);
		}
		return defaultdir;
	}

}
