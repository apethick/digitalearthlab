package del.mt.geowiz.omnium.parameters.representations;
import del.mt.geowiz.omnium.parameters.Parameter;
import del.mt.geowiz.omnium.parameters.ParameterString;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;

public class SingleLabelRepresentation extends ControlRepresentation {


	@Override
	public ParameterRepresentationNode createNode(Parameter p) {
		if (p instanceof ParameterString) {
			Label l = new Label();
			l.textProperty().bindBidirectional(p.getProperty());
			l.wrapTextProperty().set(true);
			Tooltip t = new Tooltip();
			t.textProperty().bindBidirectional(p.getDescription());
			return super.createNode(l, p);
		} else {
			return null;
		}
	}
}