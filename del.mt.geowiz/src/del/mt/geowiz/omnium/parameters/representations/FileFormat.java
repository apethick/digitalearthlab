/**
 * Developed by Andrew Pethick - AMPGeoscience 2015
 * Cut down from Omnium base version for use in Geowiz project
 * Converted to WTFPL
 */
package del.mt.geowiz.omnium.parameters.representations;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Optional;

import del.mt.geowiz.fontawesome.FontAwesome;
import del.mt.geowiz.omnium.parameters.Information;
import del.mt.geowiz.omnium.parameters.InformationDynamic;
import del.mt.geowiz.omnium.parameters.Parameter;
import del.mt.geowiz.omnium.parameters.ParameterFile;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser.ExtensionFilter;

public abstract class FileFormat implements InformationDynamic {

	
	public ArrayList<FileFormatExtension> extensions = new ArrayList<FileFormatExtension>();
	
	private ParameterFile fileInstancePath = new ParameterFile(new Information("file","file","file"),  "", "", false, ParameterRepresentation.CUSTOM_TEXT);

	private Information details;

	
	public FileFormat(Class instance,  Information details, Parameter... parameters) {
		this.details = details;
	}


	

	public ArrayList<FileFormatExtension> getExtensions() {
		return extensions;
	}

	public void addExtension(FileFormatExtension e) {
		if (!extensions.contains(e)) {
			extensions.add(e);
		}
	}

	public void setFileInstance(File f) {
		fileInstancePath.set(f.getAbsolutePath());
		Information i = new Information(f.getName(), f.getName(), "EDI Filename");
		this.getInformation().sync(i);
	}

	public File getFileInstance() {
		return fileInstancePath.getFile();
	}

	public ExtensionFilter getExtensionFilter() {

		ArrayList<String> extensions = new ArrayList<String>();
		ArrayList<String> shortextensions = new ArrayList<String>();
		for (FileFormatExtension e : getExtensions()) {
			String ext = "*." + e.getShortname().get();
			shortextensions.add(e.getShortname().get());
			extensions.add(ext);
		}
		String allext = " (";
		for (int i = 0; i < shortextensions.size(); i++) {
			allext += "." + shortextensions.get(i) + ((i == shortextensions.size() - 1) ? ")" : ",");
		}
		ExtensionFilter f = new ExtensionFilter(getDescription() + allext, extensions);

		return f;
	}

	private String getDescription() {
		String s = getInformation().getName().get();
		// s += "(" + getInformation().getDescription().get() + ")";
		return s;
	}
}
