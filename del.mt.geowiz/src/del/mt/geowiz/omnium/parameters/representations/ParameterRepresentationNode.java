/**
 * Developed by Andrew Pethick - AMPGeoscience 2015
 * Cut down from Omnium base version for use in Geowiz project
 * Converted to WTFPL
 */
package del.mt.geowiz.omnium.parameters.representations;

import javafx.beans.property.BooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class ParameterRepresentationNode extends VBox{

	public final Node control;
	public final Label label;
	public final Label descriptionLabel;
	private final BooleanProperty enableNameLabel;
	private final BooleanProperty enableDescLabel;
	
	public ParameterRepresentationNode(Node control, Label label, Label descriptionLabel, BooleanProperty enableNameLabel, BooleanProperty enableDescLabel) {
		super();
		this.control = control;
		this.label = label;
		this.descriptionLabel = descriptionLabel;
		this.enableNameLabel = enableNameLabel;
		this.enableDescLabel = enableDescLabel;

		update();
		enableNameLabel.addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				update();
			}
		});
		enableDescLabel.addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				update();
			}
		});
	}

	private void update() {
		super.getChildren().clear();
		if(enableNameLabel.get()) getChildren().addAll(label);
		if(enableDescLabel.get()) getChildren().addAll(descriptionLabel);
		HBox.setMargin(control, new Insets(0, 0, 0, 0));
		VBox.setMargin(control, new Insets(0, 0, 0, 0));
		getChildren().addAll(control);
	}
	
}
