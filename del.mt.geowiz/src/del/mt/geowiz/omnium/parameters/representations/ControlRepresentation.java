/**
 * Developed by Andrew Pethick - AMPGeoscience 2015
 * Cut down from Omnium base version for use in Geowiz project
 * Converted to WTFPL
 */
package del.mt.geowiz.omnium.parameters.representations;

import del.mt.geowiz.omnium.parameters.Parameter;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.text.TextAlignment;

public abstract class ControlRepresentation extends ParameterRepresentation{


	public ParameterRepresentationNode createNode(Node control, Parameter p) {
		
		Label label = p.getLabel();
		Label description = p.getDescriptionLabel();
		
		label.setTextAlignment(TextAlignment.LEFT);
		description.setTextAlignment(TextAlignment.LEFT);
//		control.setStyle("-fx-background-color: rgba(255,255,255,0.3);");
		label.setStyle("-fx-font-weight: bold;");
		label.setId("h3");
		description.setId("h4");
		control.setId("text");
		label.setTooltip(p.getTooltip());
		
		ParameterRepresentationNode n = new ParameterRepresentationNode(control, label, description,super.visibleNameLabel,super.visibleDescLabel);
		label.visibleProperty().bind(super.visibleNameLabel);
		description.visibleProperty().bind(super.visibleDescLabel);
		
		
		HBox.setHgrow(label, Priority.ALWAYS);
		HBox.setHgrow(description, Priority.ALWAYS);
		HBox.setHgrow(control, Priority.ALWAYS);
		
		label.setMaxWidth(Double.MAX_VALUE);
		description.setMaxWidth(Double.MAX_VALUE);
		control.maxWidth(Double.MAX_VALUE);
		
		return n;
	}

	

}
