/**
 * Developed by Andrew Pethick - AMPGeoscience 2015
 * Cut down from Omnium base version for use in Geowiz project
 * Converted to WTFPL
 */
package del.mt.geowiz.omnium.parameters.representations;

import del.mt.geowiz.omnium.parameters.Information;
import del.mt.geowiz.omnium.parameters.Parameter;
import del.mt.geowiz.omnium.parameters.ParameterBoolean;
import del.mt.geowiz.omnium.parameters.ParameterProperty;
import del.mt.geowiz.omnium.parameters.PropertyBoolean;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.util.Callback;
import javafx.util.StringConverter;

public class SingleComboRepresentation extends ControlRepresentation {

	private Label selectedLabel;
	@Override
	public ParameterRepresentationNode createNode(final Parameter p) {
		ComboBox<ParameterProperty> combo = new ComboBox<ParameterProperty>(){
			
		};
		combo.itemsProperty().set(p.options);

		if (p instanceof ParameterBoolean) {
			if (p.options.size() == 0) {
				p.options.add(new PropertyBoolean(new Information("Yes", "yes", "yes"), true));
				p.options.add(new PropertyBoolean(new Information("No", "no", "no"), false));

			}

		}

		
		combo.setConverter(new StringConverter<ParameterProperty>() {

			@Override
			public String toString(ParameterProperty arg0) {
				if (arg0 == null) {
					return null;
				} else {
					return arg0.getInformation().getName().get();
				}
			}

			@Override
			public ParameterProperty fromString(String arg0) {
				return null;
			}
		});
		// if (p instanceof ParameterInteger) {
		//
		// } else if (p instanceof ParameterDouble) {
		// StringConverter<? extends Number> converter = new
		// DoubleStringConverter();
		// Bindings.bindBidirectional(f.textProperty(), p.getProperty(),
		// (StringConverter<Number>)converter);
		// } else if (p instanceof ParameterFloat) {
		// StringConverter<? extends Number> converter = new
		// FloatStringConverter();
		// Bindings.bindBidirectional(f.textProperty(), p.getProperty(),
		// (StringConverter<Number>)converter);
		// } else if (p instanceof ParameterLong) {
		// StringConverter<? extends Number> converter = new
		// LongStringConverter();
		// Bindings.bindBidirectional(f.textProperty(), p.getProperty(),
		// (StringConverter<Number>)converter);
		// } else {
		// f.textProperty().bindBidirectional(p.getProperty());
		// }
		// resources.error(new Exception("Currently not implemented"));
		combo.setCellFactory(new Callback<ListView<ParameterProperty>, ListCell<ParameterProperty>>() {

			@Override
			public ListCell<ParameterProperty> call(ListView<ParameterProperty> param) {
				return new ListCell<ParameterProperty>() {
					private final BorderPane node;
		
					{
						setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
						node = new BorderPane();
					}

					@Override
					protected void updateItem(ParameterProperty l, boolean empty) {
						super.updateItem(l, empty);
						if (node == null || empty) {
							setGraphic(null);
						} else {
							Label label = new Label(l.getInformation().getName().get());
							selectedLabel = label;
							label.textProperty().bindBidirectional(l.getInformation().getName());
							Tooltip tooltip = new Tooltip();
							tooltip.textProperty().bindBidirectional(l.getInformation().getDescription());
							label.setTooltip(tooltip);
							setGraphic(label);
							setText(l.getInformation().getName().get());
						}
					}
				};
			}
		});
		if (p.getSelectedOption() != null) {
			combo.getSelectionModel().select(p.getSelectedOption());
		} else if (p.options.size() > 0) {
			combo.getSelectionModel().select(0);
		}
		combo.valueProperty().addListener(new ChangeListener<ParameterProperty>() {
			@Override
			public void changed(ObservableValue ov, ParameterProperty t, ParameterProperty t1) {
				if(t1 != null)
				p.setOption(t1);
			}
		});
		combo.setPrefWidth(-1);
		VBox b = new VBox();		
		b.getChildren().addAll(p.getLabel(), combo);

		BorderPane vp = new BorderPane();
		
		combo.setMaxWidth(Double.MAX_VALUE);
		
//		combo.editableProperty().set(p.isEditable());
//		
//		FloatyNodeView view = new FloatyNodeView(combo);
//		view.label.textProperty().bindBidirectional(p.getInformation().getName());
//		Tooltip tooltip = new Tooltip(p.getInformation().getDescription().get());
//		tooltip.textProperty().bindBidirectional(p.getInformation().getDescription());
//		view.label.setTooltip(tooltip);
//		combo.setTooltip(tooltip);

		return super.createNode(getEditableNode(combo, p), p);

	}
	public Label getValueLabel(ParameterProperty p) {		
		Label l = new Label(p == null ? "" : p.getInformation().getName().get());
		l.textAlignmentProperty().set(TextAlignment.LEFT);
		return l;
	}
	
	public Node getEditableNode(ComboBox n, Parameter p) {		
		Label l = getValueLabel((ParameterProperty) n.getSelectionModel().getSelectedItem());

		BorderPane b = new BorderPane();
		b.setLeft(p.isEditable.get() ? n : l);
		n.selectionModelProperty().addListener(new ChangeListener<Object>() {

			@Override
			public void changed(ObservableValue<? extends Object> observable, Object oldValue, Object v) {				
				b.setCenter(p.isEditable.get() ? n : l);
				String st = ((ParameterProperty) n.getSelectionModel().getSelectedItem()).getInformation().getName().get();
				l.textProperty().set(st);
			}
		});
		return b;
	}
}

