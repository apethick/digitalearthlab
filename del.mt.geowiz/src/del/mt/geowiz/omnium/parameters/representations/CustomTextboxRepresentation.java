/**
 * Developed by Andrew Pethick - AMPGeoscience 2015
 * Cut down from Omnium base version for use in Geowiz project
 * Converted to WTFPL
 */
package del.mt.geowiz.omnium.parameters.representations;

import javax.measure.quantity.Dimensionless;
import javax.measure.unit.Unit;

import del.mt.geowiz.omnium.parameters.Parameter;
import del.mt.geowiz.omnium.parameters.ParameterDouble;
import del.mt.geowiz.omnium.parameters.ParameterInteger;
import del.mt.geowiz.omnium.parameters.ParameterNumerical;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.util.StringConverter;
import javafx.util.converter.DoubleStringConverter;
import javafx.util.converter.FloatStringConverter;
import javafx.util.converter.IntegerStringConverter;
import javafx.util.converter.LongStringConverter;

public class CustomTextboxRepresentation extends ControlRepresentation {
	

	@Override
	public ParameterRepresentationNode createNode(final Parameter p) {
		return  createNode(p, false, false);

	}

	public ParameterRepresentationNode createNode(final Parameter p, boolean centredLabel, boolean centredContent) {
//		FloatyFieldView v = FloatyFieldView.createNode();
		TextField f = new TextField();		
		f.disableProperty().bind(Bindings.not(p.isEditable));
		
		if (p instanceof ParameterInteger) {
			StringConverter<? extends Number> converter = new IntegerStringConverter();
			Bindings.bindBidirectional(f.textProperty(), p.getProperty(), (StringConverter<Number>) converter);
		} else if (p instanceof ParameterDouble) {
			StringConverter<? extends Number> converter = new DoubleStringConverter();
			Bindings.bindBidirectional(f.textProperty(), p.getProperty(), (StringConverter<Number>) converter);
		}  else {
			f.textProperty().bindBidirectional(p.getProperty());
		}
		f.promptTextProperty().bind(getPromptText(p));
		f.setMaxWidth(Double.MAX_VALUE);
		
//		HBox.setHgrow(f, Priority.ALWAYS);
//		HBox b = new HBox(f);
//		b.setAlignment(Pos.BASELINE_LEFT);
//		Tooltip t = getTooltip(p);
//		Label l = v.label;
//		f.setTooltip(t);
//		l.setTooltip(t);		
//		l.setAlignment(centredLabel ? Pos.CENTER : Pos.CENTER_LEFT);
//		f.setAlignment(centredContent ? Pos.CENTER : Pos.CENTER_LEFT);
		
		if (p instanceof ParameterNumerical) {
			ParameterNumerical num = (ParameterNumerical) p;
			if(num.getUnit().equals(Dimensionless.UNIT)) return super.createNode(f,p);
			HBox b = new HBox();
			Label l = new Label(num.getUnit().toString());
			l.setMinWidth(100);
			b.setMaxWidth(Double.MAX_VALUE);
			HBox.setHgrow(b, Priority.ALWAYS);
			f.setMaxWidth(Double.MAX_VALUE);
			HBox.setHgrow(f, Priority.ALWAYS);
			b.getChildren().add(f);
			b.getChildren().add(l);			
			((ParameterNumerical) p).getUnitProperty().addListener(new ChangeListener<Unit>() {

				@Override
				public void changed(ObservableValue<? extends Unit> observable, Unit oldValue, Unit newValue) {
					l.setText(num.getUnit().toString());
				
				}
			});
			return super.createNode(b, p);
		} else return super.createNode(f, p);
	}
	
	public TextField getTextFieldNode(final Parameter p) {
		TextField f = new TextField();
	
		f.setMaxWidth(Double.MAX_VALUE);
		f.disableProperty().bind(Bindings.not(p.isEditable));
		if (p instanceof ParameterInteger) {
			StringConverter<? extends Number> converter = new IntegerStringConverter();
			Bindings.bindBidirectional(f.textProperty(), p.getProperty(), (StringConverter<Number>) converter);
		} else if (p instanceof ParameterDouble) {
			StringConverter<? extends Number> converter = new DoubleStringConverter();
			Bindings.bindBidirectional(f.textProperty(), p.getProperty(), (StringConverter<Number>) converter);
		}  else {
			f.textProperty().bindBidirectional(p.getProperty());
		}
		f.promptTextProperty().bind(getPromptText(p));
		return f;
	}
	
	public ParameterRepresentationNode createNode(final Parameter p, boolean centredLabel, boolean centredContent, boolean enableNameLabels, boolean enableDescriptionLabels) {
		super.visibleNameLabel.set(enableNameLabels);
		super.visibleDescLabel.set(enableDescriptionLabels);
//		FloatyFieldView v = FloatyFieldView.createNode();
		TextField f = new TextField();
		f.setMaxWidth(Double.MAX_VALUE);
		f.disableProperty().bind(Bindings.not(p.isEditable));
		if (p instanceof ParameterInteger) {
			StringConverter<? extends Number> converter = new IntegerStringConverter();
			Bindings.bindBidirectional(f.textProperty(), p.getProperty(), (StringConverter<Number>) converter);
		} else if (p instanceof ParameterDouble) {
			StringConverter<? extends Number> converter = new DoubleStringConverter();
			Bindings.bindBidirectional(f.textProperty(), p.getProperty(), (StringConverter<Number>) converter);
		} else {
			f.textProperty().bindBidirectional(p.getProperty());
		}
		f.promptTextProperty().bind(getPromptText(p));
//		HBox.setHgrow(f, Priority.ALWAYS);
//		HBox b = new HBox(f);
//		b.setAlignment(Pos.BASELINE_LEFT);
//		Tooltip t = getTooltip(p);
//		Label l = v.label;
//		f.setTooltip(t);
//		l.setTooltip(t);		
//		l.setAlignment(centredLabel ? Pos.CENTER : Pos.CENTER_LEFT);
//		f.setAlignment(centredContent ? Pos.CENTER : Pos.CENTER_LEFT);
		
		if (p instanceof ParameterNumerical) {
			ParameterNumerical num = (ParameterNumerical) p;
			if(num.getUnit().equals(Dimensionless.UNIT)) return super.createNode(f,p);
			HBox b = new HBox();
			Label l = new Label(num.getUnit().toString());
			((ParameterNumerical) p).getUnitProperty().addListener(new ChangeListener<Unit>() {

				@Override
				public void changed(ObservableValue<? extends Unit> observable, Unit oldValue, Unit newValue) {
					l.setText(num.getUnit().toString());
				
				}
			});
			b.getChildren().add(f);
			b.getChildren().add(l);
			
			return super.createNode(b, p);
			
		} else return super.createNode(f, p);
	}
}
