/**
 * Developed by Andrew Pethick - AMPGeoscience 2015
 * Cut down from Omnium base version for use in Geowiz project
 * Converted to WTFPL
 */
package del.mt.geowiz.omnium.parameters;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

public  class ParameterType {
	

	public final static int TYPE_BOOLEAN = 0;
	public final static int TYPE_DOUBLE = 1;
	public final static int TYPE_FLOAT = 2;
	public final static int TYPE_LONG = 3;
	public final static int TYPE_INTEGER = 4;
	public final static int TYPE_STRING = 5;
	public final static int TYPE_FILE = 6;
	public static final int TYPE_OBJECT = 7;

	
	public final BooleanProperty isEditable = new SimpleBooleanProperty(true);
	public final BooleanProperty isOptional = new SimpleBooleanProperty(false);
	public final BooleanProperty isImported = new SimpleBooleanProperty(false);
	public final BooleanProperty isEnabled = new SimpleBooleanProperty(true);

	


	

	public BooleanProperty getEditable() {
		return isEditable;
	}

	public BooleanProperty getOptional() {
		return isOptional;
	}

	public BooleanProperty getImported() {
		return isImported;
	}

	public BooleanProperty getEnabled() {
		return isEnabled;
	}


}
