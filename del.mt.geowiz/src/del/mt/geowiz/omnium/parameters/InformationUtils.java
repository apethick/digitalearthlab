/**
 * Developed by Andrew Pethick - AMPGeoscience 2015
 * Cut down from Omnium base version for use in Geowiz project
 * Converted to WTFPL
 */
package del.mt.geowiz.omnium.parameters;

import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;

public class InformationUtils {
	public static Label getLabel(Information info) {
		Label title = new Label(info.getName().get());
		Tooltip tooltip = new Tooltip(info.getDescription().get());
		title.setId("h1");
		title.setTooltip(tooltip);



		return title;
	}

	public static Label getDescriptionLabel(Information info) {
		Label title = new Label(info.getDescription().get());
		Tooltip tooltip = new Tooltip(info.getDescription().get());
		title.setId("h2");
		title.setTooltip(tooltip);
		return title;
	}

}

