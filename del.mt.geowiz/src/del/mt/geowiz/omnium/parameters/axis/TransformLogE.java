/**
 * Developed by Andrew Pethick - AMPGeoscience 2015
 * Cut down from Omnium base version for use in Geowiz project
 * Converted to WTFPL
 */
package del.mt.geowiz.omnium.parameters.axis;

import del.mt.geowiz.omnium.parameters.Information;

public class TransformLogE extends Transform {
	
	public TransformLogE() {
		super(new Information("LogE","loge","LogE"));
		negativeEnabled.set(false);
		negativeEnabled.setDefault(false);
		negativeEnabled.setEditable(false);
	}

	@Override
	public double getValueFromBase(double b) {
		return Math.exp(b);
	}

	@Override
	public double getBaseFromValue(double v) {
		return Math.log(v);
	}

}
