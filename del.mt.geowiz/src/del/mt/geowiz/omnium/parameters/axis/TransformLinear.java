/**
 * Developed by Andrew Pethick - AMPGeoscience 2015
 * Cut down from Omnium base version for use in Geowiz project
 * Converted to WTFPL
 */
package del.mt.geowiz.omnium.parameters.axis;

import del.mt.geowiz.omnium.parameters.Information;
import javafx.scene.Node;

public class TransformLinear extends Transform{

	
	public TransformLinear() {	
		super(new Information("Linear","Linear","Linear"));
		negativeEnabled.set(true);
		negativeEnabled.setDefault(true);
		negativeEnabled.setEditable(false);
	}


	@Override
	public double getValueFromBase(double b) {
		return b;
	}

	@Override
	public double getBaseFromValue(double v) {
		return v;
	}







}
