/**
 * Developed by Andrew Pethick - AMPGeoscience 2015
 * Cut down from Omnium base version for use in Geowiz project
 * Converted to WTFPL
 */
package del.mt.geowiz.omnium.parameters.axis;

import java.util.ArrayList;

import javax.measure.quantity.Dimensionless;

import del.mt.geowiz.omnium.parameters.Information;
import del.mt.geowiz.omnium.parameters.ParameterBoolean;
import del.mt.geowiz.omnium.parameters.ParameterDouble;
import del.mt.geowiz.omnium.parameters.ParameterString;
import del.mt.geowiz.omnium.parameters.PropertyString;
import del.mt.geowiz.omnium.parameters.representations.ParameterRepresentation;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.scene.Node;


public class Axis {

	
	public final ParameterBoolean isReversed;
	private final ParameterDouble min;
	private final ParameterDouble max;
	private final ParameterDouble minExtent;
	private final ParameterDouble maxExtent;
	private final ParameterString transformSelection;

	private BooleanProperty changed = new SimpleBooleanProperty();
	
	private double baseWidth;
	
	
	
	public final Transforms transforms;
	
	Task currentTask = null;
	Task nextTask = null;
	
//	private final AsynchronousQueue exec;
//	private final AsynchronousQueueSettings execSettings;
	
	public ObjectProperty<Transform> transform = new SimpleObjectProperty<Transform>();
	public final static double [] defaultStepIncrements = {10,7.5,5,4,3,2.5,2,1.5,1}; 
	
	public Axis() {
		
		this.transforms = new Transforms();
		this.transform.set(transforms.LINEAR);
		this.min = new ParameterDouble(new Information("Min","min","Min"), Dimensionless.UNIT, -100, -100, false, ParameterRepresentation.CUSTOM_TEXT);
		this.max = new ParameterDouble(new Information("Max","max","Max"), Dimensionless.UNIT,  100, 100, false, ParameterRepresentation.CUSTOM_TEXT);
		this.minExtent = new ParameterDouble(new Information("Min Extent","minextent","Min Extent"), Dimensionless.UNIT, -1E100, -1E100, false, ParameterRepresentation.CUSTOM_TEXT);
		this.maxExtent = new ParameterDouble(new Information("Max Extent","maxextent","Max Extent"), Dimensionless.UNIT,  1E100, 1E100, false, ParameterRepresentation.CUSTOM_TEXT);
		this.isReversed = new ParameterBoolean(new Information("Reversed","reversed","Reversed"), false, false, false, ParameterRepresentation.SINGLE_CHECKBOX);
		this.transformSelection = new ParameterString(new Information("Transformed","transformed","Transformed"), transforms.LINEAR.info.getName().get(), transforms.LINEAR.info.getName().get(), false, ParameterRepresentation.SINGLE_COMBO);
		this.transformSelection.addOption(new PropertyString(transforms.LINEAR.info, transforms.LINEAR.info.getName().get()));
		this.transformSelection.addOption(new PropertyString(transforms.LOG10.info, transforms.LOG10.info.getName().get()));
		this.transformSelection.addOption(new PropertyString(transforms.LOG2.info, transforms.LOG2.info.getName().get()));
		this.transformSelection.addOption(new PropertyString(transforms.LOGE.info, transforms.LOGE.info.getName().get()));
		
		
//		this.execSettings = new AsynchronousQueueSettings(base);
//		this.execSettings.corePoolSize.set(1);
//		this.execSettings.maximumPoolSize.set(1);
//		this.exec = new AsynchronousQueue(execSettings, new SynchronousQueue<Runnable>());
		this.transformSelection.get().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				for(Transform t : transforms.getAllTransforms()) {
					if(t.info.getName().get().equals(transformSelection.getValue().getValuePrimative().get())) {
						transform.set(t);
						if(!transform.get().negativeEnabled.get().get()) {
							if(min.get().get() <= 0) min.set(1E-50);
							if(max.get().get() <= 0) max.set(1E50);
							if(minExtent.get().get() <= 0) minExtent.set(1E-50);
							if(maxExtent.get().get() <= 0) maxExtent.set(1E50);							
						} else {
							setChanged();	
						}						
					}
				}
			}

			
		});
		transform.addListener(new ChangeListener<Transform>() {

			@Override
			public void changed(ObservableValue<? extends Transform> observable, Transform oldValue, Transform newValue) {
				setChanged();
			}
		});
		
	}
	private void setChanged() {
		this.changed.set(!this.changed.get());
	}
	public DoubleProperty getMinExtentProperty() {
		return minExtent.get();
	}
	public DoubleProperty getMaxExtentProperty() {
		return maxExtent.get();
	}
	public double getMinExtent() {
		return minExtent.get().get();
	}
	public double getMaxExtent() {
		return maxExtent.get().get();
	}
//	public void submitUpdateTask(Task task) {
//
//		Thread t = new Thread(task);
//		t.start();
////		if(currentTask == null) {
////			currentTask = task;
////			if(task != null)
////			currentTask.setOnSucceeded(new EventHandler<Event>() {
////
////				@Override
////				public void handle(Event event) {
////					if(nextTask == null) return;
////					Task task2 = nextTask;
////					nextTask = null;
////					currentTask = null;
////					submitUpdateTask(task2);					
////				}
////			});
////			t.start();	
////			
////			return;
////		}
////		if(currentTask.isDone()) {
////			currentTask = task;
////			currentTask.setOnSucceeded(new EventHandler<Event>() {
////
////				@Override
////				public void handle(Event event) {
////					Task task2 = nextTask;
////					nextTask = null;
////					currentTask = null;
////					submitUpdateTask(task2);					
////				}
////			});
////			t.start();			
////			return;
////		}
////		Platform.runLater(new Runnable() {
////			
////			@Override
////			public void run() {
////				if(currentTask.isRunning()) {
////					nextTask = task;
////				
////				}
////			}
////		});
//		
//	}
//	public void update(){
//
//		double pixelWidth = size;
//		double valueWidth = projection.getBaseFromValue(max) - projection.getBaseFromValue(min);
//		
//		valuesPerPixel = valueWidth/pixelWidth;
//
//		for(Viewer c : linkedViewers) c.repaint();
//		
//	}
//	@Override
//	public Node getIcon() {
//		return FontAwesome.SORT_NUMERIC_ASC();
//	}



	public ArrayList<Double> getAxisLabelValues(int size, int maxLabels, int minSpacingHorizontal, int minSpacingVertical, boolean isHorizontal) {
		double step = getAverageStepSize(min.get().get(),max.get().get(),isHorizontal,size, maxLabels,minSpacingHorizontal, minSpacingVertical);
		double start = getStart(min.get().get(), size, step);
		ArrayList<Double> values = getAxisValues(start, max.get().get(), step);
		return values;
	}
	public double getAverageStepSize(double min, double max, boolean isHorizontal, int size, int maxLabels, int minSpacingHorizontal, int minSpacingVertical) {
		Transform transform = this.transform.get();
		try {
			if(min == max) {
				return 1; //to stop infinite series
			}
			double difference = transform.getBaseFromValue(max) - transform.getBaseFromValue(min);
			int basis = (int) Math.floor(Math.log10(difference));			
			int iteration = 0;
			while(iteration++ < 5) {
				double multiplier = Math.pow(10, basis);
				double lastValue = defaultStepIncrements[0] * multiplier;
				for(int i = 1 ; i < defaultStepIncrements.length ; i++) {
					double d = defaultStepIncrements[i] * multiplier;
					int nSteps = (int) (difference/d);
					nSteps = Math.max(1, nSteps);
					int spacing = size/nSteps;
					
					if(nSteps >= maxLabels || (spacing < (isHorizontal ? minSpacingHorizontal : minSpacingVertical))) {

						return lastValue;
					}
					lastValue = d;
				}
				basis--;
			}
			double step = defaultStepIncrements[0] *  Math.pow(10, basis);

			return step;
		} catch(Exception e) {
	
			return 1;
		}	
	}
	public void setMinMaxExtent(double min , double max) {

		this.minExtent.set(min);
		this.maxExtent.set(max);

		setChanged();
	}
	public void setMinMax(double min, double max) {

		this.min.set(Math.min(min,max));
		this.max.set(Math.max(min,max));

		setChanged();
	}
	public double getStart(double min, double max, double step) {	
		Transform transform = this.transform.get();
		if(transform.negativeEnabled.get().get()) {
			 int i = (int) Math.floor(transform.getBaseFromValue(min - min%transform.getValueFromBase(step)));
	
			double start = ((double) i) - step;
			return start;
		} else {
			return Math.floor(transform.getBaseFromValue(min));
		}
	}
	public ArrayList<Double> getAxisValues(double start, double end, double step) {
		Transform transform = this.transform.get();
		ArrayList<Double> values = new ArrayList<Double>();		
		if(step == 0) step = 1;
		for(double d = start ; d <= (transform.getBaseFromValue(end) + step) ; d += step) {
			try {
				values.add(transform.getValueFromBase(d));
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		return values;
	}
	
	
//	public double convertToAxisCoordinate(int pixel) {
//		double value = transform.getValueFromBase(valuesPerPixel * (isReversed ? size - pixel : pixel) + min);
//		return value;
//	}
//	
//	public double convertPixelToValue(int pixel) {
//		double bv = transform.getBaseFromValue(min.get().get()) + ((double) pixel * valuesPerPixel);
//		return transform.getValueFromBase(bv);
//	}
	
	
	
	public float convertPixelToPercent(int pixel, int size, boolean isYAxis) {
		if(isYAxis) return 1-((float) pixel / (float) size);
		else return ((float) pixel / (float) size);
	}
	public double convertPixelToValue(int pixel, int size, boolean isYAxis) {
		return convertPercentToValue(convertPixelToPercent(pixel, size,isYAxis));
	}
	public int convertValueToPixel(double value, int size, boolean isYAxis) {
		float p = convertValueToPercent(value);
		int pixel = (int) (((float)size)*p);
		if(isYAxis) return (size - pixel);
		return pixel;
	}
	public int convertPercentToPixel(float percent, int size, boolean isYAxis) {		
		int pixel = (int) ((float)size*percent);
		if(isYAxis) return (size - pixel);
		return pixel;
	}
//	public int convertValueToPixel(double value) {
//		int pixel = (int) ((transform.getBaseFromValue(value) - transform.getBaseFromValue(min))/valuesPerPixel);
//		return (isReversed ? size - pixel : pixel);
//	}
//
	public float convertPercentToValue(float percent) {
		Transform transform = this.transform.get();
		float p = isReversed.get().get() ? 1 - percent : percent;
		double max = this.max.get().get();
		double min = this.min.get().get();
		return (float) (transform.getValueFromBase(p * (transform.getBaseFromValue(max) - transform.getBaseFromValue(min)) + transform.getBaseFromValue(min))); 
	}
	public float convertValueToPercent(double value) {
		Transform transform = this.transform.get();
		double min = this.min.get().get();
		double max = this.max.get().get();
		double transformMin = transform.getBaseFromValue(min);
		double transformMax = transform.getBaseFromValue(max);
		double transformValue = transform.getBaseFromValue(value);
		boolean isReversed = this.isReversed.get().get();
		float percent = (float) ((transformValue - transformMin)/(transformMax - transformMin));
		return (isReversed ? 1 - percent : percent);
	}
	
	
	
//	public float getScalarPercent(double value) {
//		return (isReversed() ? 1 : 1) * ((float) Math.round((transform.getBaseFromValue(value))/valuesPerPixel)/size);
//	}
	
//package open.opengl.viewer.axis;


//import java.awt.Color;
//import java.awt.event.ActionEvent;
//import java.awt.event.ActionListener;
//import java.util.ArrayList;
//
//import javax.swing.JCheckBox;
//import javax.swing.JMenu;
//import javax.swing.event.ChangeEvent;
//
//import open.opengl.viewer.main.Structure;
//import open.opengl.viewer.main.Viewer;
//import open.utilities.math.transform.Transform;
//import open.utilities.math.transform.TransformLinear;
//import open.utilities.math.transform.TransformLog10;
//import open.utilities.math.transform.TransformLog2;
//import open.utilities.math.transform.TransformLogE;
//import open.utilities.math.transform.TransformPseudoLog10;
//import open.utilities.math.transform.Transforms;
//import open.utilities.misc.gui.swing.ChangeListener;
//import open.xml.settings.viewer.plot.ViewerSettingsPlotAxis;
//
//import com.jogamp.opengl.util.gl2.GLUT;
//
//public class Axis {
//	public ViewerSettingsPlotAxis settings;
//	public ArrayList<AxisGL> linkedAxisGL = new ArrayList<AxisGL>();
//	public AxisJScrollBar linkedScrollBar;
//	
//	public String name;
//	
//	public Transform projection = new TransformLinear();
//	
//	public boolean hold = false;
//	private boolean LOCKED = false;
//	
//	private double min = 0.01;
//	private double max = 1.00;
//	private double minExtent = Double.NEGATIVE_INFINITY;
//	private double maxExtent = Double.POSITIVE_INFINITY;
//	boolean isReversed;
//	int GLUT_FONT = GLUT.BITMAP_HELVETICA_10;
//	float [] FONT_COLOR = {1,1,1,0.5f};
//	float [] AXIS_BACKGROUND_COLOR = {0f,0f,0f,1f};
//	float [] AXIS_TICK_COLOR = {1,1,1,1};
//	float [] AXIS_TITLE_FOREGROUND = {1,1,1,1};
//	float [] AXIS_TITLE_BACKGROUND = {0,0,0,0.8f};
//	
//	private int size;
//	private double currentStep = 1;
//	private double valuesPerPixel = 1;
//	public ArrayList<Viewer> linkedViewers = new ArrayList<Viewer>();
//
//	
//	ChangeListener l = new ChangeListener();
//	
//	
//	public Axis(Transform projection, ViewerSettingsPlotAxis settings) {
//		this.settings = settings;						
//		this.projection = projection;
//		validateAxis();		
//		initSettings();
//		
//	}
//
//	public void lock(boolean locked) {
//		this.LOCKED = locked;
//	}
//	
//	ArrayList<AxisGridTicks> ticks = new ArrayList<AxisGridTicks>();
//	
//	private void initSettings() {
//		min = settings.getMinimum();
//		max = settings.getMaximum();
//		minExtent = settings.getMinimumExtent(); 
//		maxExtent = settings.getMaximumExtent(); 
////		setMaximumExtents(minExtent, maxExtent);
//		isReversed = settings.isReversed();
//		name = settings.getTitle();	
//		GLUT_FONT = settings.getGLFont();
//		FONT_COLOR = settings.getAxisTextColor();
//		AXIS_BACKGROUND_COLOR = settings.getAxisBackgroundColor();
//		AXIS_TICK_COLOR = settings.getAxisTickColor();
//		AXIS_TITLE_FOREGROUND = settings.getAxisTitleForegroundColor();
//		AXIS_TITLE_BACKGROUND = settings.getAxisTitleBackgroundColor();
//		
//	}
//	public void setSize(int size) {
//		this.size = size;
//		setTransform();
//	}
//	
//	public double getValuesPerPixel() {
//		return new Double(valuesPerPixel);
//	}
//	public void setReversed(boolean isReversed) {
//		this.isReversed = isReversed;
//		setTransform();
//	}
//	
//	public boolean isReversed() {
//		return isReversed;
//	}
//	
//	public void linkAxisGL(AxisGL axisGL) {
////		if(axisGL != null) ticks.add();
//		if(axisGL != null) {
//			Structure s = new AxisGridTicks(axisGL.isHorizontal(), this, settings.getTickSettings());
//			for(Viewer v : linkedViewers) s.initialise(v);
//		}
//				
//		this.linkedAxisGL.add(axisGL);
//	}
//	
//
//
//	
//	public void unlinkAllGrids(){
//		linkedViewers = new ArrayList<Viewer>();
//	}
//	
//	public void unlinkGrid(Viewer g) {
//		linkedViewers.remove(g);
//	}
//	
//	public void linkViewer(Viewer v) {
//		linkedViewers.add(v);
//		
//	}
//	
//		
//	public void setMaximumExtents(double minExtent, double maxExtent) {
//		if(!projection.canBeNegative() && minExtent <= 0) {
//			System.err.println("ERROR AXIS CANNOT BE NEGATIVE");
//			this.minExtent = 1E-40;
//		} else {
//			this.minExtent = minExtent;	
//		}		
//		if(!projection.canBeNegative() && maxExtent <= 0) {
//			System.err.println("ERROR AXIS CANNOT BE NEGATIVE");
//			this.maxExtent = 1E-40;
//		} else {
//			this.maxExtent = maxExtent;	
//		}	
//		
//		if(minExtent > maxExtent) {
//			this.minExtent = Math.min(maxExtent,minExtent);
//			this.maxExtent = Math.max(maxExtent,minExtent);
//		} else if(minExtent == maxExtent){
//			this.maxExtent = minExtent + 1;
//		}		
//		
//	}
//	
	public DoubleProperty getMinProperty() {
		return min.get();
	}
	public DoubleProperty getMaxProperty() {
		return max.get();
	}
	public double getMin() {
		return min.get().get();
	}
	public double getMax() {
		return max.get().get();
	}
//	public double getMinExtent() {
//		return new Double(minExtent);
//	}
//	public double getMaxExtent() {
//		return new Double(maxExtent);
//	}
//	
//
//	
//	public double getMinExtentPercent() {
//		if(Double.isInfinite(minExtent) || Double.isInfinite(maxExtent)) return -1;
//		double width = projection.getBaseFromValue(maxExtent) - projection.getBaseFromValue(minExtent);
//		return (projection.getBaseFromValue(this.min) - projection.getBaseFromValue(minExtent))/width;
//	}
//	
//	public double getMaxExtentPercent() {
//		if(Double.isInfinite(minExtent) || Double.isInfinite(maxExtent)) return -1;
//		double width = projection.getBaseFromValue(maxExtent) - projection.getBaseFromValue(minExtent);
//		return (projection.getBaseFromValue(this.max) - projection.getBaseFromValue(minExtent))/width;	
//	}
//	
//	public void setExtents(double min, double max) {
//		setMin(min);
//		setMax(max);
//	}
//	
//	public void setMinMaxOverride(double min, double max) {	
//			this.min = min;
//			this.max = max;
//			validateAxis();
//	}
//	
//	public void setMinMax(double min, double max) {
//
//		if(!hold) {
//			this.min = min;
//			this.max = max;
//			validateAxis();
//		}
//		
//	}
//	
//	public void setMin(double min) {
//		if(!LOCKED) {
//			if(!Double.isInfinite(minExtent)) this.min = Math.max(this.min, this.minExtent);
//			if(!Double.isInfinite(maxExtent)) this.max = Math.min(this.max, this.maxExtent);
//			validateAxis();
//			setTransform();
//		}
//	}
//	public void setMax(double max) {
//		if(!LOCKED) {
//			if(!Double.isInfinite(minExtent)) this.min = Math.max(this.min, this.minExtent);
//			if(!Double.isInfinite(maxExtent)) this.max = Math.min(this.max, this.maxExtent);
//			validateAxis();
//			setTransform();
//		}
//	}
//	
//	
//	public AxisJScrollBar getScrollBar(int direction){		
//		double extents = (projection.getBaseFromValue(maxExtent) - projection.getBaseFromValue(minExtent));
//		double leftPercent = (projection.getBaseFromValue(min) - projection.getBaseFromValue(minExtent))/extents;
//		double rightPercent = (projection.getBaseFromValue(max) - projection.getBaseFromValue(minExtent))/extents;
//		
//		int leftPosition = (int)(((double)  AxisJScrollBar.RESOLUTION) * leftPercent);
//		int rightPosition = (int)(((double)  AxisJScrollBar.RESOLUTION) * rightPercent);
//		int newExtent = rightPosition - leftPosition;	
//		final AxisJScrollBar bar = new AxisJScrollBar(this,direction, leftPosition, newExtent, 0, AxisJScrollBar.RESOLUTION);
//		this.linkedScrollBar = bar;
//		this.linkedScrollBar.getModel().addChangeListener(new javax.swing.event.ChangeListener() {
//			
//			@Override
//			public void stateChanged(ChangeEvent arg0) {
//				for(AxisGL a : linkedAxisGL) {
//					a.axis.update();
//					a.repaintParent();
//				}
//			}
//		});
//		updateScrollBar();
//		return bar;
//	}
//	
//
//	
//	
//	
//	public void validateAxis() {
//		if(!projection.canBeNegative()) {
//			if(this.min <= 0) {
//				new Exception().printStackTrace();
//				System.err.println("Minimum is negative and axis is Log.");
//				this.min = 1E-50;
//			}
//			if(this.max <= 0) {
//				System.err.println("Maximum is negative and axis is Log.");
//				this.max = this.min + 1;
//			}
//		}
//		if(min == max) {
////			System.err.println("Minimum cannot equal maximum.");
//			this.min = this.min - 1;
//			this.max = this.max + 1;
//		}
//	}
//
//	
//	
//
//	public void updateAxis() {
//		for(AxisGL a : linkedAxisGL) {
//			if(a != null) {
//				if(!a.axis.hold) a.v.display();
//			}
//		}
//	}
//	
//	public void update(){
//
//		double pixelWidth = size;
//		double valueWidth = projection.getBaseFromValue(max) - projection.getBaseFromValue(min);
//		
//		valuesPerPixel = valueWidth/pixelWidth;
//
//		for(Viewer c : linkedViewers) c.repaint();
//		
//	}
//	
//	private void setTransform() {
//		update();
//		updateScrollBar();
//		l.setValuesChanged();
//	}
//
//	
//	
//	
//	
//	//AUTOMATING AXIS LABELS



//	
//
//
//	public double getStart(double min, double max, double step) {				
//		if(projection.canBeNegative()) {
//			 int i = (int) Math.floor(projection.getBaseFromValue(min - min%projection.getValueFromBase(step)));
//
//			double start = ((double) i) - step;
//			return start;
//		} else {
//			return Math.floor(projection.getBaseFromValue(min));
//		}
//	}
//	
//	
//	public ArrayList<Double> getAxisValues(double start, double end, double step) {
//		ArrayList<Double> values = new ArrayList<Double>();		
//		if(step == 0) step = 1;
//		for(double d = start ; d <= (projection.getBaseFromValue(end) + step) ; d += step) {
//			try {
//			values.add(projection.getValueFromBase(d));
//			} catch(Exception e) {
//				System.err.println(values.size());
//			}
//		}
//		return values;
//	}
//	

//	
	public void translate(float translation, double min, double max) {
		Platform.runLater(new Runnable() {
			
			@Override
			public void run() {
		
				Transform transform = Axis.this.transform.get();
				double newMin = min;
				double newMax = max;
				double originalDifference = transform.getBaseFromValue(max) - transform.getBaseFromValue(min);
				double shift = (isReversed.get().get() ? -1 : 1) * originalDifference*translation;
				newMin = transform.getValueFromBase(transform.getBaseFromValue(min) + shift);
				newMax = transform.getValueFromBase(transform.getBaseFromValue(max) + shift);
				
				newMin = Math.max(newMin, minExtent.get().get());
				newMax = Math.min(newMax, maxExtent.get().get());
				double width = Axis.this.max.get().get() - Axis.this.min.get().get();
				
				
				
				//STOP SCROLLING OFF DATA EXTENTS AND RESCALING
				if(newMin <= Axis.this.minExtent.get().get()) {
					if(!Double.isInfinite(minExtent.get().get())) {
						Axis.this.min.set(Math.max(Axis.this.min.get().get(), Axis.this.minExtent.get().get()));
						Axis.this.max.set(Axis.this.min.get().get() + width);
					}
					else Axis.this.min.set(Math.min(newMin, newMax));
				} else if(newMax >= Axis.this.maxExtent.get().get()) {
					if(!Double.isInfinite(maxExtent.get().get())) {
						Axis.this.max.set(Math.min(Axis.this.max.get().get(), Axis.this.maxExtent.get().get()));
						Axis.this.min.set(Axis.this.max.get().get() - width);
					} else {
						Axis.this.max.set(Math.max(newMin, newMax));
					}
				} else {
					Axis.this.min.set(Math.min(newMin, newMax));
					Axis.this.max.set(Math.max(newMin, newMax));
				}
	
				setChanged();
				
			}
		});
	
	}


	public void scale(float scale, double min, double max, boolean mouseActiveFirstHalf) {
		Platform.runLater(new Runnable() {
			
			@Override
			public void run() {
	
				Transform transform = Axis.this.transform.get();
				//SCALE CANNOT BE OVER +/- 98% to prevent INFINITE SCALING
				float scale2 = (float) Math.max(-0.98, scale);
				scale2 = (float) Math.min(0.98, scale2);
				
				 double difference = transform.getBaseFromValue(max) - transform.getBaseFromValue(min);
				 double shift = (isReversed.get().get() ? -1 : 1) *difference * scale2;
				 double minN = transform.getValueFromBase(transform.getBaseFromValue(min) + shift);
				 double maxN = transform.getValueFromBase(transform.getBaseFromValue(max) + shift);
				 if(isReversed.get().get()) {
					 if(mouseActiveFirstHalf)  Axis.this.max.set(Math.max(minN,maxN));
					 else  Axis.this.min.set(Math.min(minN,maxN));
				 } else {
					 if(mouseActiveFirstHalf)  Axis.this.min.set(Math.min(minN,maxN));
					 else  Axis.this.max.set(Math.max(minN,maxN));
				 }
			
				if(!Double.isInfinite(minExtent.get().get())) Axis.this.min.set(Math.max(Axis.this.min.get().get(), Axis.this.minExtent.get().get()));
				if(!Double.isInfinite(maxExtent.get().get())) Axis.this.max.set(Math.min(Axis.this.max.get().get(), Axis.this.maxExtent.get().get()));
			
				setChanged();
			}
		});
		
		
	}

	public void zoom(double percent, float mouseLocation) {
		Platform.runLater(new Runnable() {
			
			@Override
			public void run() {
				
			  		Transform transform = Axis.this.transform.get();
			  		float percent2 = ((float) percent )/ 10;
			  		double difference = transform.getBaseFromValue(max.get().get()) - transform.getBaseFromValue(min.get().get());

			  		double change =  difference * percent2/25; 
			  		double changeMin = change*(1-mouseLocation);
			  		double changeMax = change*(mouseLocation);

			  		double minN = transform.getValueFromBase(transform.getBaseFromValue(min.get().get()) - changeMin);
			  		 double maxN = transform.getValueFromBase(transform.getBaseFromValue(max.get().get()) + changeMax);
			  		Axis.this.min.set(Math.min(minN, maxN));
			  		Axis.this.max.set(Math.max(minN, maxN));

			  		if(!Double.isInfinite(minExtent.get().get())) Axis.this.min.set(Math.max(Axis.this.min.get().get(), Axis.this.minExtent.get().get()));

			  		if(!Double.isInfinite(maxExtent.get().get())) Axis.this.max.set(Math.min(Axis.this.max.get().get(), Axis.this.maxExtent.get().get()));
			
			  	
			  		setChanged();
			}
		});
		
	}
//
//	
//	public void updateScrollBar() {
//		if(linkedScrollBar != null && !linkedScrollBar.getValueIsAdjusting()) {
//			if(!Double.isInfinite(minExtent) && !Double.isInfinite(maxExtent)) {				
//				double extents = (projection.getBaseFromValue(maxExtent) - projection.getBaseFromValue(minExtent));
//				double leftPercent = (projection.getBaseFromValue(min) - projection.getBaseFromValue(minExtent))/extents;
//				double rightPercent = (projection.getBaseFromValue(max) - projection.getBaseFromValue(minExtent))/extents;
//				
//				int leftPosition = (int)(((double) linkedScrollBar.getModel().getMaximum()) * leftPercent);
//				int rightPosition = (int)(((double) linkedScrollBar.getModel().getMaximum()) * rightPercent);
//				int newExtent = rightPosition - leftPosition;
//				linkedScrollBar.update(leftPosition, newExtent, 0, AxisJScrollBar.RESOLUTION);
//				
//			}
//			
//		}
//	}
//
//	public void setProjection(Transform projection) {
//		if(!projection.canBeNegative()) {
//			if(max <= 0) max = 1;
//			if(min <= 0) min = max/10;
//		}
//		this.projection = projection;
//		if (projection instanceof TransformLinear) settings.setBase(ViewerSettingsPlotAxis.LINEAR);
//		else if (projection instanceof TransformLog10) settings.setBase(ViewerSettingsPlotAxis.LOG10);
//		else if (projection instanceof TransformLog2) settings.setBase(ViewerSettingsPlotAxis.LOG2);
//		else if (projection instanceof TransformLogE) settings.setBase(ViewerSettingsPlotAxis.LOGE);
//		else if (projection instanceof TransformPseudoLog10) settings.setBase(ViewerSettingsPlotAxis.PSEUDO_LOG10);
//		
//	}
//
//	public JMenu getContextMenu() {
//		JMenu axis = new JMenu();
//		JMenu transform = new JMenu("Transform");
//		JMenu setMinMax = new JMenu("Set Min/Max");
//		
//		final JCheckBox hold = new JCheckBox("Hold",this.hold);
//		
//		float [] currentColor = settings.getAxisBackgroundColor();
//		boolean isNegated = currentColor[0] == 0f && currentColor[1] == 0f && currentColor[2] == 0f;
//		final JCheckBox negate = new JCheckBox("Negate",isNegated);
//		
//		
//		
//		negate.addActionListener(new ActionListener() {
//			
//			@Override
//			public void actionPerformed(ActionEvent arg0) {
//				settings.setAxisBackgroundColor(negate.isSelected() ? Color.black : Color.white);
//				settings.setAxisTextColor(negate.isSelected() ? Color.white : Color.black);
//				settings.setAxisTickColor(negate.isSelected() ? Color.white : Color.black);
//				for(AxisGL a : linkedAxisGL) {
//					if(a != null) a.v.repaint();
//				}
//				for(Viewer v : linkedViewers) v.repaint();
//			}
//			
//		});
//		
//		final ArrayList<JCheckBox> XScales = new ArrayList<JCheckBox>();
//		for(final Transform projection : Transforms.getAllTransforms()) {
//			final JCheckBox c = new JCheckBox(projection.getName());
//			c.setSelected(this.projection.getName().equals(projection.getName()));
//			XScales.add(c);
//			c.addActionListener(new ActionListener() {				
//				@Override
//				public void actionPerformed(ActionEvent arg0) {
//					setProjection(projection);				
//					for(JCheckBox b : XScales) b.setSelected(false);
//					c.setSelected(true);
//					setTransform();
//					for(AxisGL a : linkedAxisGL) {
//						if(a != null) a.v.repaint();
//					}
//					for(Viewer v : linkedViewers) v.repaint();
//				}
//			});
//			transform.add(c);
//		}
//		hold.addActionListener(new ActionListener() {
//			
//			@Override
//			public void actionPerformed(ActionEvent arg0) {
//				Axis.this.hold = hold.isSelected();
//			}
//		});
//		axis.add(hold);
//		axis.add(negate);
//		axis.add(transform);
//		return axis;
//	}
//
//	public int getSize() {
//		return size;
//	}
//
//	public boolean isWithinBounds(double d) {
//		return (min <= d && max >= d); 
//	}
//	public boolean isLessThanMaximum(double d) {
//		return max >= d;
//	}
//	public boolean isMoreThanMaximum(double d) {
//		return max <= d;
//	}
//	public boolean isLessThanMinimum(double d) {
//		return min >= d;
//	}
//	public boolean isMoreThanMinimum(double d) {
//		return min <= d;
//	}
//	public double getCurrentStep() {
//		return currentStep;
//	}
//
//	public void addChangeListener(javax.swing.event.ChangeListener l) {
//		this.l.addChangeListener(l);
//	}
//
//
//
//	
//
//	
//
//	
//}
	
	
	

	
}