/**
 * Developed by Andrew Pethick - AMPGeoscience 2015
 * Cut down from Omnium base version for use in Geowiz project
 * Converted to WTFPL
 */
package del.mt.geowiz.omnium.parameters.axis;

import del.mt.geowiz.omnium.parameters.Information;
import del.mt.geowiz.omnium.parameters.ParameterBoolean;
import del.mt.geowiz.omnium.parameters.representations.ParameterRepresentation;


public abstract class Transform {

	
	
	public ParameterBoolean negativeEnabled = new ParameterBoolean(new Information("Negative Enabled","negen","Enable Negative Values"), true, true, false, ParameterRepresentation.SINGLE_CHECKBOX);
	public Information info;
	
	public Transform(Information info) {
		this.info = info;
	}	
	
//	public final static Transform LINEAR = new TransformLinear();
//	public final static Transform LOG10 = new TransformLog10();
//	public final static Transform LOG2 = new TransformLog2();
//	public final static Transform LOGE = new TransformLogE();
	
	

	public abstract double getValueFromBase(double b);
	public abstract double getBaseFromValue(double v);
	
}
