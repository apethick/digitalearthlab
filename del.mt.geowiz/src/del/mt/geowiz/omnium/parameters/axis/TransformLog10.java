/**
 * Developed by Andrew Pethick - AMPGeoscience 2015
 * Cut down from Omnium base version for use in Geowiz project
 * Converted to WTFPL
 */
package del.mt.geowiz.omnium.parameters.axis;

import del.mt.geowiz.omnium.parameters.Information;

public class  TransformLog10 extends Transform {
	
	public TransformLog10() {
		super(new Information("Log10", "log10", "Log10"));
		negativeEnabled.set(false);
		negativeEnabled.setDefault(false);
		negativeEnabled.setEditable(false);
	}

	@Override
	public double getValueFromBase(double b) {
		return Math.pow(10, b);
	}

	@Override
	public double getBaseFromValue(double v) {
		return Math.log10(v);
	}



}
