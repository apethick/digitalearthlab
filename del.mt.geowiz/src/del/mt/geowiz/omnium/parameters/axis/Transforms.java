/**
 * Developed by Andrew Pethick - AMPGeoscience 2015
 * Cut down from Omnium base version for use in Geowiz project
 * Converted to WTFPL
 */
package del.mt.geowiz.omnium.parameters.axis;

import java.util.Vector;

public class Transforms {
	
	public final Transform LINEAR; 
	public final Transform LOG10; 
	public final Transform LOG2; 
	public final Transform LOGE; 
	
	
	public Transforms() {		
		this.LINEAR =  new TransformLinear();
		this.LOG10 =  new TransformLog10();
		this.LOG2 =  new TransformLog2();
		this.LOGE =  new TransformLogE();
		

	}

	
	
	public Vector<Transform> getAllTransforms() {
		Vector<Transform> projections = new Vector<Transform>();
		projections.add(LINEAR);
		projections.add(LOG10);
		projections.add(LOG2);
		projections.add(LOGE);		
		return projections;
	}
}
