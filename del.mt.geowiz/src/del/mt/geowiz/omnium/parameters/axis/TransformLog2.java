/**
 * Developed by Andrew Pethick - AMPGeoscience 2015
 * Cut down from Omnium base version for use in Geowiz project
 * Converted to WTFPL
 */
package del.mt.geowiz.omnium.parameters.axis;

import del.mt.geowiz.omnium.parameters.Information;

public class TransformLog2 extends Transform {

	public TransformLog2() {
		super(new Information("Log2","log2","Log2"));
		
		negativeEnabled.set(false);
		negativeEnabled.setDefault(false);
		negativeEnabled.setEditable(false);
	}

	@Override
	public double getValueFromBase(double b) {
		return Math.pow(2, b);
	}

	@Override
	public double getBaseFromValue(double v) {
		return Math.log(v)/Math.log(2);
	}

}


