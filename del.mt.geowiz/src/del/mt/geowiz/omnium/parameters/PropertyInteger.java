/**
 * Developed by Andrew Pethick - AMPGeoscience 2015
 * Cut down from Omnium base version for use in Geowiz project
 * Converted to WTFPL
 */
package del.mt.geowiz.omnium.parameters;

import javax.measure.quantity.Dimensionless;
import javax.measure.unit.Unit;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class PropertyInteger extends PropertyNumerical<IntegerProperty> {

	public PropertyInteger(Information information, int value, Unit unit) {
		super(information, new SimpleIntegerProperty(value), unit);
	}
	public PropertyInteger(Information information, int value) {
		super(information, new SimpleIntegerProperty(value), Dimensionless.UNIT);
	}

	@Override
	public int getPropertyType() {
		return TYPE_INTEGER;
	}
	@Override
	public Number getNumber() {
		return ((IntegerProperty) super.value).get();
	}
	public void set(Integer v) {
		super.value.set(v);
	}
	@Override
	public void setValue(IntegerProperty v) {
		set(v.get());
	}
}
