/**
 * Developed by Andrew Pethick - AMPGeoscience 2015
 * Cut down from Omnium base version for use in Geowiz project
 * Converted to WTFPL
 */
package del.mt.geowiz.omnium.parameters;

import javax.measure.unit.Unit;

import javafx.beans.property.Property;

public abstract class PropertyNumerical<E extends Property>  extends ParameterProperty<E>{

	private Unit unit;	
	
	public PropertyNumerical(Information information, E value, Unit unit) {
		super(information, value);
		this.unit = unit;
	}

	public Unit getUnit() {
		return unit;
	}
	public void setUnit(Unit unit) {
		this.unit = unit;
	}
	public  abstract Number getNumber();

	@Override
	public String toString() {
		return value.toString();
	}
	
}
