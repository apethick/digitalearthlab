/**
 * Developed by Andrew Pethick - AMPGeoscience 2015
 * Cut down from Omnium base version for use in Geowiz project
 * Converted to WTFPL
 */
package del.mt.geowiz.omnium.parameters;

import del.mt.geowiz.omnium.parameters.representations.ParameterRepresentation;
import javafx.beans.property.StringProperty;

public class ParameterString extends Parameter<StringProperty>{

	
	public ParameterString(Information info,	 String defaultValue, String value, boolean isOptional, ParameterRepresentation representation) {
		super(ParameterString.class, info, new PropertyString(info,defaultValue), new PropertyString(info,value), isOptional, representation);
	}
	public StringProperty get() {
		return ((StringProperty)this.getProperty());
	}
	public void set(String value) {
		((StringProperty)this.getProperty()).set(value);
	}
	@Override
	public void setValueFromString(String string) {
		set(string);
	}
	@Override
	public String getStringFromValue() {
		return get().get();
	}
	@Override
	public int getParameterType() {
		return Parameter.TYPE_STRING;
	}
}
