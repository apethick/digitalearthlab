/**
 * Developed by Andrew Pethick - AMPGeoscience 2015
 * Cut down from Omnium base version for use in Geowiz project
 * Converted to WTFPL
 */
package del.mt.geowiz.omnium.parameters;

import javax.measure.Measure;
import javax.measure.unit.Unit;

import del.mt.geowiz.omnium.parameters.representations.ParameterRepresentation;
import javafx.beans.property.IntegerProperty;

public class ParameterInteger extends ParameterNumerical<IntegerProperty> {

	public ParameterInteger(Information info, Unit units,	int defaultValue, int value, boolean isOptional,ParameterRepresentation representation) {
		super(ParameterInteger.class, info, units,  new PropertyInteger(info,defaultValue, units), new PropertyInteger(info,value, units), isOptional, representation);
	}
	public void set(int value) {
		((IntegerProperty)this.getProperty()).set(value);
	}
	public IntegerProperty get() {
		return ((IntegerProperty)this.getProperty());
	}
	@Override
	public void setValueFromString(String string) {
		set(Integer.valueOf(string));
	}
	public int get(Unit u) {
		return Measure.valueOf(get().get(), this.getUnit()).intValue(u);
	}

	@Override
	public Number getNumber() {
		return get().intValue();
	}
	@Override
	public String getStringFromValue() {
		return ("" + get().intValue());
	}
	@Override
	public int getParameterType() {
		return Parameter.TYPE_INTEGER;
	}
	
}
