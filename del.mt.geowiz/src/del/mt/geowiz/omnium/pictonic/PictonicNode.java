/**
 * Developed by Andrew Pethick - AMPGeoscience 2015
 * Cut down from Omnium base version for use in Geowiz project
 * Converted to WTFPL
 */
package del.mt.geowiz.omnium.pictonic;

import javafx.scene.control.Label;

public class PictonicNode extends Label{

	public float size = Pictonic.DEFAULT_SIZE;

	public PictonicNode(String type) {
		super(type);
	}
	public PictonicNode(String type, float size) {
		super(type);
		this.size = size;
	}
}
