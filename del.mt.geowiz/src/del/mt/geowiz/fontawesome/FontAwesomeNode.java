package del.mt.geowiz.fontawesome;

import javafx.scene.control.Label;

public class FontAwesomeNode extends Label{

	public float size = FontAwesome.DEFAULT_SIZE;

	public FontAwesomeNode(String type) {
		super(type);
	}
	public FontAwesomeNode(String type, float size) {
		super(type);
		this.size = size;
	}
}
