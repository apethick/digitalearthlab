package del.mt.geowiz.occam;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import Text.Text;
import del.mt.geowiz.MTApplication;
import del.mt.geowiz.dat.Data;
import del.mt.geowiz.execution.SystemsAnalyst;
import del.mt.geowiz.geoelectric.GeoelectricalEarth;


public class OCCAM1DCSEM {

	public final static int COMPONENT_TE = 0;
	public final static int COMPONENT_TM = 1;

	public final static double [] defaultLevels = {0,5,10,15,20,25,30,35,40,45,50,55,65,75,85,95,105,
			115,125,135,145,155,165,180,195,210,225,240,255,270,285,300,
			315,330,350,370,390,410,430,450,470,490,510,530,550,575,600,
			625,650,675,700,725,750,775,800,825,875,925,975,1025,1075,
			1125,1175,1225,1275,1325,1375,1450,1525,1600,1675,1750,1825,
			1900,1975,2050,2125,2200,2300,2400,2500,2600,2700,2800,2900,
			3000,3100,3200,3300,3450,3600,3750,3900,4050,4200,4350,4500,
			4650,4800,4950,5150,5350,5550,5750,5950,6150,6350,6550,6750,
			6950,7150,7650,8150,8650,9150,9650,10150,10650,11150,11650,
			12150,12650,13650,14650,15650,16650,17650,18650,19650,20650,
			21650,22650};


	public static void writeStartupFile(GeoelectricalEarth e, File fxy, File fyx, String modelFile, String dataFile) throws Exception {
		BufferedWriter outxy = new BufferedWriter(new FileWriter(fxy));
		BufferedWriter outyx = new BufferedWriter(new FileWriter(fyx));

		String format = "OCCAMITER_FLEX";
		String desc = "Geowiz Auto-generated file";



		String [] strs = {"Format:             " + format,
						"Description:        " + desc,
						"Model File:         " + modelFile                ,
						"Data File:          " + dataFile                 ,
						"Date/Time:          " + SystemsAnalyst.getDate() ,
						"Iterations to run:  " + 100                      ,
						"Target Misfit:      " + 0.001                    ,
						"Roughness Type:     " + 1                        ,
						"Stepsize Cut Count: " + 6                        ,
						"Model Limits:       " + "0,"+"6"                  ,
						"Model Value Steps:  " + 0.05                     ,
						"Debug Level:        " + 1                        ,
						"Iteration:          " + 0                        ,
						"Lagrange Value:     " + 5.00                     ,
						"Roughness Value:    " + 1E10                     ,
						"Misfit Value:       " + 10000                    ,
						"Misfit Reached:     " + 0                        ,
						"Param Count:        " + defaultLevels.length    };


		for(String st : strs) {
			outxy.write(st + "\n");
			outyx.write(st + "\n");
		}

		double nth = 1;
		for(double l : defaultLevels) {
			double res = e.getResistivityAtDepth(l);
			outxy.write(Math.log10(res) + (nth%10 == 0 ? "\n" : "\t"));
			outyx.write(Math.log10(res) + (nth%10 == 0 ? "\n" : "\t"));
			nth++;
		}
//		3	3	3	3	3	3	3	3	3	3
//		3	3	3	3	3	3	3	3	3	3
//		3	3	3	3	3	3	3	3	3	3
//		3	3	3	3	3	3	3	3	3	3
//		3	3	3	3	3	3	3	3	3	3
//		3	3	3	3	3	3	3	3	3	3
//		3	3	3	3	3	3	3	3	3	3
//		3	3	3	3	3	3	3	3	3	3
//		3	3	3	3	3	3	3	3	3	3
//		3	3	3	3	3	3	3	3	3	3
//		3	3	3	3	3	3	3	3	3	3
//		3	3	3	3	3	3	3	3	3	3
//		3	3	3	3	3	3	3	3	3	3
//		3	3

		outxy.close();
		outyx.close();
	}

	public static void writeEarthFile(GeoelectricalEarth e, File fxy, File fyx) throws Exception{
		BufferedWriter outxy = new BufferedWriter(new FileWriter(fxy));
		BufferedWriter outyx = new BufferedWriter(new FileWriter(fyx));

		outxy.write("Format: Resistivity1DMod_1.0" + "\n");
		outyx.write("Format: Resistivity1DMod_1.0"+ "\n");
		outxy.write("#LAYERS:    " + (defaultLevels.length + 1)+ "\n");
		outyx.write("#LAYERS:    " + (defaultLevels.length + 1)+ "\n");

		outxy.write("! Layer block in file is:"+ "\n");
		outxy.write("! [top_depth    resistivity  penalty    prejudice   prej_penalty]  ! note that top_depth and penalty of 1st layer ignored"+ "\n");
		outxy.write("-100000       1d12          0          0             0          ! Air, fixed layer"+ "\n");

		outyx.write("! Layer block in file is:"+ "\n");
		outyx.write("! [top_depth    resistivity  penalty    prejudice   prej_penalty]  ! note that top_depth and penalty of 1st layer ignored"+ "\n");
		outyx.write("-100000       1d12          0          0             0          ! Air, fixed layer"+ "\n");

		for(double l : defaultLevels) {

			outxy.write(l + "          ?           0.9          0           0"+ "\n");
			outyx.write(l + "          ?           0.9          0           0"+ "\n");
		}
		outxy.close();
		outyx.close();

	}



	public static void writeDataFile(Data d, File fxy, File fyx) throws Exception{
		BufferedWriter outxy = new BufferedWriter(new FileWriter(fxy));
		BufferedWriter outyx = new BufferedWriter(new FileWriter(fyx));
		String comment = "This data file generated from Geowiz";

		writeHeader(d,outxy,comment + ": TE Mode");
		writeFrequencyPhaseReceiverBlocks(d,outxy);
		writeDataBlock(d,outxy,COMPONENT_TE);

		writeHeader(d,outyx,comment + ": TM Mode");
		writeFrequencyPhaseReceiverBlocks(d,outyx);
		writeDataBlock(d,outyx,COMPONENT_TM);
		outxy.close();
		outyx.close();
	}

	private static void writeDataBlock(Data d, BufferedWriter out, int componentTETM) throws Exception{
		ArrayList<Integer> types = new ArrayList<>();
		ArrayList<Integer> freqs = new ArrayList<>();
		ArrayList<Integer> txs = new ArrayList<>();
		ArrayList<Integer> rxs = new ArrayList<>();
		ArrayList<Double> dat = new ArrayList<>();
		ArrayList<Double> stderr = new ArrayList<>();

		switch (componentTETM) {
		case COMPONENT_TE:

				for(int i = 0 ; i < d.frequencies.size() ; i++) {
					//Rho
					if(d.rhoXY.get(i) < 1E6) {
						freqs.add(i+1);
						types.add(103);
						txs.add(0);
						rxs.add(0);
						dat.add(d.rhoXY.get(i));
						stderr.add(Math.abs(d.rhoXY.get(i)*0.05));
					}

					//Phs
					if(Math.abs(d.phaseXY.get(i)) < 360) {
						freqs.add(i+1);
						types.add(104);
						txs.add(0);
						rxs.add(0);
						double phase = d.phaseXY.get(i);
						phase = phase < 0 ? phase + 180 : phase;
						dat.add(phase);
						stderr.add(5.0);
					}
				}

			break;
		case COMPONENT_TM:
			for(int i = 0 ; i < d.frequencies.size() ; i++) {
				//Rho
				if(d.rhoYX.get(i) < 1E6) {
					freqs.add(i+1);
					types.add(105);
					txs.add(0);
					rxs.add(0);
					dat.add(d.rhoYX.get(i));
					stderr.add(Math.abs(d.rhoYX.get(i)*0.05));
				}

				//Phs
				if(Math.abs(d.phaseYX.get(i)) < 360) {
					freqs.add(i+1);
					types.add(106);
					txs.add(0);
					rxs.add(0);
					double phase = d.phaseYX.get(i);
					phase = phase < 0 ? phase + 180 : phase;
					dat.add(phase);
					stderr.add(5.0);
				}
			}
			break;
		default:
			break;
		}


		out.write("# Data:       " + freqs.size() + "\n");
		out.write("!         Type        Freq#        Tx#           Rx#       Data           Std_Er" + "\n");
		for(int i = 0 ; i < freqs.size() ; i++) {
			out.write(types.get(i) + "\t" + freqs.get(i) + "\t" + txs.get(i) + "\t" + rxs.get(i) + "\t" + dat.get(i) + "\t" + stderr.get(i) + "\n");
		}

	}

	private static void writeFrequencyPhaseReceiverBlocks(Data d, BufferedWriter out) throws Exception{
		out.write("# Frequencies:    " + d.frequencies.size() + "\n");
		for(double f : d.frequencies) {
			out.write(f + "\n");
		}

		out.write("Phase Convention: lag"+ "\n");
		out.write("# Receivers:      1"+ "\n");
		out.write("!            X            Y            Z        Theta        Alpha         Beta"+ "\n");
		out.write("0            0            1            0            0            0" + "\n");
	}

	private static  void writeHeader(Data d, BufferedWriter out, String comment) throws Exception{
		out.write("Format:  EMData_1.1\n");
		out.write("! This is a synthetic data file generated from Dipole1D output\n");
		out.write("! Enoisefloor: 1e-15 V/m/Am\n");
		out.write("! Bnoisefloor: 1e-18 T/Am\n");
		out.write("! Percent Noise added: 2 %\n");
		out.write("! Data have been rotated (theta,alpha,beta):      0,      0,      0 degrees\n");
		out.write("\n");
		out.write("# Transmitters:   1\n");
		out.write("!            X            Y            Z      Azimuth          Dip\n");
		out.write("0            0          0           0            0\n");
		out.write("");

	}

	public static void readOCCAMModel(Data fieldData, File folder, int componentTETM, MTApplication app) throws Exception{
		ArrayList<File> iterations = new ArrayList<>();
		for(File f : folder.listFiles()) {
			if(f.getAbsolutePath().toLowerCase().endsWith(".iter")) {
				iterations.add(f);
			}
		}
		Collections.sort(iterations, new Comparator<File>() {

			@Override
			public int compare(File o1, File o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		File finalIteration = (iterations.get(iterations.size()-1));
		BufferedReader in = new BufferedReader(new FileReader(finalIteration));
		String line = "";
		while(!(line = in.readLine()).contains("Param Count")) {

		}
		ArrayList<Double> values = new ArrayList<>();
		while((line = in.readLine()) != null) {
			values.addAll(Text.split(line));
		}

		Double [] valuesArr  = values.toArray(new Double[0]);
		fieldData.invertedDepths =  defaultLevels;
		switch (componentTETM) {
		case COMPONENT_TE:
			fieldData.invertedTERes = valuesArr;

			break;
		case COMPONENT_TM:
			fieldData.invertedTMRes = valuesArr;
			break;
		default:
			break;
		}
//		System.out.println("DONE1");
//		for(int i = 0 ; i < defaultLevels.length-1 ; i++) {
//			System.out.println("DONE " + i);
//			double l1 = defaultLevels[i];
//			double l2 = defaultLevels[i+1];
//			double thick = Math.abs(l2 - l1);
//			switch (componentTETM) {
//			case COMPONENT_TE:
//
//				fieldData.invertedTE.earth.addNewLayer(i, thick, values.get(i), 1, 0.001, 10000, 0.001, 10000);
//				break;
//			case COMPONENT_TM:
//
//				fieldData.invertedTM.earth.addNewLayer(i, thick, values.get(i), 1, 0.001, 10000, 0.001, 10000);
//				break;
//			default:
//				break;
//			}
//
//		}
//		switch (componentTETM) {
//		case COMPONENT_TE:
//			fieldData.invertedTE.earth.addNewLayer(defaultLevels.length, -1, values.get(defaultLevels.length-1), 1, 0.001, 10000, 0.001, 10000);
//			break;
//		case COMPONENT_TM:
//			fieldData.invertedTM.earth.addNewLayer(defaultLevels.length, -1, values.get(defaultLevels.length-1), 1, 0.001, 10000, 0.001, 10000);
//			break;
//		default:
//			break;
//		}


//
		in.close();
	}
}
