package del.mt.geowiz.occam;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class FileExecution {


	public static void runProcess(String executableString, File currentWorkingDirectory) {
		try {

			Runtime rt = Runtime.getRuntime();
			String commands[] = { "" };
			Process p = rt.exec(executableString, commands, currentWorkingDirectory);
			BufferedReader proc1StdOut = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line1StdOut;
			while (true) {
				line1StdOut = proc1StdOut.readLine();
				if (line1StdOut == null) break;
				try {
					Thread.sleep(2);
				} catch (Exception e) {

				}
			}
			try {
				Thread.sleep(2);
			} catch (Exception e) {

			}
			p.destroy();

		} catch (Exception exc) {
			System.err.println("Could not execute algorithm!");

			try {
				Thread.sleep(2000);
				try {
					Runtime rt = Runtime.getRuntime();
					String commands[] = { "" };
					Process p = rt.exec(executableString, commands, currentWorkingDirectory);
					BufferedReader proc1StdOut = new BufferedReader(new InputStreamReader(p.getInputStream()));
					String line1StdOut;
					while (true) {
						line1StdOut = proc1StdOut.readLine();
						if (line1StdOut == null) break;
						try {
							Thread.sleep(2);
						} catch (Exception e) {

						}
					}
					try {
						Thread.sleep(50);
					} catch (Exception e) {

					}
					p.destroy();

				} catch (Exception exc2) {
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					exc.printStackTrace();
				}
			} catch (InterruptedException e2) {
				e2.printStackTrace();
			}

		}
	}

	public static void runProcess(File executable, File currentWorkingDirectory, ArrayList<String> parameters) {
		try {
			Runtime rt = Runtime.getRuntime();
			String commands[] = {};

			String executionCommand = "./" + executable.getName();
			for(String p : parameters) {
				executionCommand += (" " + p);
			}
			System.out.println(currentWorkingDirectory.getAbsolutePath());
			System.out.println(executionCommand);


			Process p = rt.exec(executionCommand, commands, currentWorkingDirectory);

			BufferedReader proc1StdOut = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			String line1StdOut;

			while (true) {

				line1StdOut = proc1StdOut.readLine();
				System.out.println("LINE : " + line1StdOut);
				if (line1StdOut == null) break;
			}
			try {
				Thread.sleep(50);
			} catch (Exception e) {

			}
			p.destroy();

		} catch (Exception exc) {
			System.err.println("Could not execute algorithm!");
			System.err.println("Attempting to chmod program");
			Runtime rt2 = Runtime.getRuntime();
			String commands2[] = { "" };
			try {
				Process p2 = rt2.exec("chmod 777 " + executable.getAbsolutePath(), commands2, currentWorkingDirectory);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			try {
				Thread.sleep(2000);
				try {
					Runtime rt = Runtime.getRuntime();
					String commands[] = { "" };
					Process p = rt.exec(executable.getAbsolutePath(), commands, currentWorkingDirectory);
					BufferedReader proc1StdOut = new BufferedReader(new InputStreamReader(p.getInputStream()));
					String line1StdOut;
					while (true) {
						line1StdOut = proc1StdOut.readLine();
						if (line1StdOut == null) break;
						try {
							Thread.sleep(2);
						} catch (Exception e) {

						}
					}
					try {
						Thread.sleep(50);
					} catch (Exception e) {

					}
					p.destroy();

				} catch (Exception exc2) {
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					exc.printStackTrace();
				}
			} catch (InterruptedException e2) {
				e2.printStackTrace();
			}

		}
	}

	public static ArrayList<String> runProcess(File executable, File currentWorkingDirectory) {
		ArrayList<String> s = new ArrayList<String>();
		try {
			Runtime rt = Runtime.getRuntime();
			String commands[] = { "" };
//			System.out.println("EXECUTION : " + executable.getAbsolutePath() + " " + currentWorkingDirectory);
			Process p = rt.exec(executable.getAbsolutePath(), commands, currentWorkingDirectory);
			BufferedReader proc1StdOut = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line1StdOut;

			while (true) {
				line1StdOut = proc1StdOut.readLine();
				if (line1StdOut == null) break;
				else s.add(line1StdOut);
				try {
					Thread.sleep(2);
				} catch (Exception e) {

				}
			}

			try {
				Thread.sleep(5);
			} catch (Exception e) {

			}
			p.destroy();
			return s;
		} catch (Exception exc) {
			System.err.println("Could not execute algorithm!");
			System.err.println("Attempting to chmod program");
			Runtime rt2 = Runtime.getRuntime();
			String commands2[] = { "" };
			try {
				Process p2 = rt2.exec("chmod 777 " + executable.getAbsolutePath(), commands2, currentWorkingDirectory);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			try {
				Thread.sleep(2000);
				try {
					Runtime rt = Runtime.getRuntime();
					String commands[] = { "" };
					Process p = rt.exec(executable.getAbsolutePath(), commands, currentWorkingDirectory);
					BufferedReader proc1StdOut = new BufferedReader(new InputStreamReader(p.getInputStream()));
					String line1StdOut;

					while (true) {
						line1StdOut = proc1StdOut.readLine();
						if (line1StdOut == null) break;
						else s.add(line1StdOut);
						try {
							Thread.sleep(2);
						} catch (Exception e) {

						}
					}
					try {
						Thread.sleep(5);
					} catch (Exception e) {

					}
					p.destroy();
					return s;
				} catch (Exception exc2) {
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					exc.printStackTrace();
				}
			} catch (InterruptedException e2) {
				e2.printStackTrace();
			}

		}
		return s;
	}
}
