package del.mt.geowiz.geoelectric;

import javax.measure.unit.NonSI;
import javax.measure.unit.SI;
import javax.measure.unit.Unit;

import del.mt.geowiz.omnium.parameters.Information;
import del.mt.geowiz.omnium.parameters.ParameterDouble;
import del.mt.geowiz.omnium.parameters.representations.CustomTextboxRepresentation;
import del.mt.geowiz.omnium.parameters.representations.ParameterRepresentation;
import del.mt.geowiz.opts.Options;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.control.Slider;

public class Layer {
	public final static double DEFAULT_THICKNESS = 100;
	public final static double DEFAULT_RESISTIVITY = 100;
	public final static double DEFAULT_WEIGHT = 1;
	public final static double DEFAULT_RESISTIVITY_MIN = 0.000001;
	public final static double DEFAULT_RESISTIVITY_MAX = 10000;
	public final static double DEFAULT_THICKNESS_MIN = 0.1;
	public final static double DEFAULT_THICKNESS_MAX = 10000;

	public static Information RESISTIVITY = new Information("Resistivity in Ohm.m","res","");
	public static Information CONDUCTIVITY = new Information("Conductivity in S/m","res","");
	public static Information CONDUCTIVITY_MS = new Information("Conductivity in mS/m","res","");
	public static Information WEIGHTING = new Information("Weighting","weight","");
	public static Information RESISTIVITY_MIN = new Information("Min Resistivity in Ohm.m","minres","");
	public static Information RESISTIVITY_MAX = new Information("Max Resistivity in Ohm.m","maxres","");
	public static Information THICKNESS = new Information("Thickness","thick","");
	public static Information THICKNESS_MIN = new Information("Min Thickness","minthick","");
	public static Information THICKNESS_MAX = new Information("Max Thickness","maxthick","");

	public ParameterDouble thickness = new ParameterDouble(THICKNESS, SI.METER,  DEFAULT_THICKNESS, DEFAULT_THICKNESS, false, ParameterRepresentation.CUSTOM_TEXT);
	public ParameterDouble resistivity = new ParameterDouble(RESISTIVITY, SI.OHM.times(SI.METER),  DEFAULT_RESISTIVITY,DEFAULT_RESISTIVITY, false, ParameterRepresentation.CUSTOM_TEXT);
	public ParameterDouble weighting = new ParameterDouble(WEIGHTING, NonSI.PERCENT,  DEFAULT_WEIGHT, DEFAULT_WEIGHT, false, ParameterRepresentation.CUSTOM_TEXT);
	public ParameterDouble resistivityMin = new ParameterDouble(RESISTIVITY_MIN, SI.OHM.times(SI.METER),  DEFAULT_RESISTIVITY_MIN, DEFAULT_RESISTIVITY_MIN, false, ParameterRepresentation.CUSTOM_TEXT);
	public ParameterDouble resistivityMax = new ParameterDouble(RESISTIVITY_MAX, SI.OHM.times(SI.METER),  DEFAULT_RESISTIVITY_MAX, DEFAULT_RESISTIVITY_MAX, false, ParameterRepresentation.CUSTOM_TEXT);
	public ParameterDouble thicknessMin = new ParameterDouble(THICKNESS_MIN, SI.METER,  DEFAULT_THICKNESS_MIN, DEFAULT_THICKNESS_MIN, false, ParameterRepresentation.CUSTOM_TEXT);
	public ParameterDouble thicknessMax = new ParameterDouble(THICKNESS_MAX, SI.METER,  DEFAULT_THICKNESS_MAX, DEFAULT_THICKNESS_MAX, false, ParameterRepresentation.CUSTOM_TEXT);

	Slider st = new Slider(0, 4, 1);
	Slider sr = new Slider(-4, 4, 1);





//	public TextField t = new TextField(""+-1);
//	public TextField r = new TextField(""+-1);
	private GeoelectricalEarth e;

	private Options options;

	public Layer(Layer l) {
		this.thickness.set(new Double(l.thickness.get().get()));
		this.resistivity.set(new Double(l.resistivity.get().get()));
		this.weighting.set(new Double(l.weighting.get().get()));
		this.resistivityMin.set(new Double(l.resistivityMin.get().get()));
		this.resistivityMax.set(new Double(l.resistivityMax.get().get()));
		this.thicknessMin.set(new Double(l.thicknessMin.get().get()));
		this.thicknessMax.set(new Double(l.thicknessMax.get().get()));
		CustomTextboxRepresentation rep = new CustomTextboxRepresentation();
		rep.visibleDescLabel.set(false);
		this.resistivity.setRepresentation(rep);
		this.thickness.setRepresentation(rep);
		this.e = l.e;
		this.options = l.options;
		thickness.get().addListener(getListener());
		resistivity.get().addListener(getListener());
		convertThickness(options.geoOpts.thicknessUnits.get().get());
		convertResistivity(options.geoOpts.resistivityUnits.get().get());
		this.options.geoOpts.resistivityUnits.get().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {

				setResistivityUnits(options.geoOpts.resistivityUnits.get().get());
			}
		});
		this.options.geoOpts.thicknessUnits.get().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				setThicknessUnits(options.geoOpts.thicknessUnits.get().get());
			}
		});
		setSliders();
	}
	public Layer(Options options, GeoelectricalEarth e, Double t, Double r, Double weight, Double rmin, Double rmax, Double tmin, Double tmax) {
		this.thickness.set(t);
		this.resistivity.set(r);
		this.weighting.set(weight);
		this.resistivityMin.set(rmin);
		this.resistivityMax.set(rmax);
		this.thicknessMin.set(tmin);
		this.thicknessMax.set(tmax);
		CustomTextboxRepresentation rep = new CustomTextboxRepresentation();
		rep.visibleDescLabel.set(false);
		this.resistivity.setRepresentation(rep);
		this.thickness.setRepresentation(rep);
		this.e = e;
		this.options = options;
		thickness.get().addListener(getListener());
		resistivity.get().addListener(getListener());
		convertThickness(options.geoOpts.thicknessUnits.get().get());
		convertResistivity(options.geoOpts.resistivityUnits.get().get());
		this.options.geoOpts.resistivityUnits.get().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {

				setResistivityUnits(options.geoOpts.resistivityUnits.get().get());
			}
		});
		this.options.geoOpts.thicknessUnits.get().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				setThicknessUnits(options.geoOpts.thicknessUnits.get().get());
			}
		});
		setSliders();
	}

	private void setSliders() {
		st.setValue(Math.log10(thickness.get().get()));
		sr.setValue(Math.log10(resistivity.get().get()));
		sr.valueProperty().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable,
					Number oldValue, Number newValue) {
				resistivity.set(Math.pow(10,newValue.doubleValue()));

			}
		});
		st.minHeight(20);
		sr.minHeight(20);
		st.valueProperty().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable,
					Number oldValue, Number newValue) {
				thickness.set(Math.pow(10,newValue.doubleValue()));
			}
		});

		thickness.get().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				st.setValue(Math.log10(newValue.doubleValue()));
			}
		});
		resistivity.get().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				sr.setValue(Math.log10(newValue.doubleValue()));
			}
		});
	}
	public void setResistivityUnits(int i) {
		convertResistivity(i);
	}

	public void setThicknessUnits(int i) {
		convertThickness(i);
	}

	private ChangeListener<? super Number> getListener() {
		return new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				e.listener.setValuesChanged();
			}
		};
	}


	public void convertThickness(int i) {
		double newThickness = getThickness(i);
		Unit newUnit = SI.METER;
		switch (i) {
		case Options.UNITS_FT: newUnit = NonSI.FOOT; break;
		case Options.UNITS_M: newUnit = SI.METER; break;
		}
		thickness.setUnit(newUnit);
		thickness.set(newThickness);
	}
	public void convertResistivity(int i) {
		double newResistivity = getResistivity(i);
		Unit newUnit = SI.OHM.times(SI.METER);
		switch (i) {
		case Options.UNITS_OHM_M: newUnit = SI.OHM.times(SI.METER); break;
		case Options.UNITS_S_M: newUnit = SI.SIEMENS.divide(SI.METER); break;
		case Options.UNITS_MS_M: newUnit = SI.MILLI(SI.SIEMENS).divide(SI.METER); break;
		}
		if(i == Options.UNITS_OHM_M) resistivity.info.sync(RESISTIVITY);
		else if(i == Options.UNITS_S_M) resistivity.info.sync(CONDUCTIVITY);
		else if(i == Options.UNITS_MS_M) resistivity.info.sync(CONDUCTIVITY_MS);

		resistivity.setUnit(newUnit);
		resistivity.set(newResistivity);

	}


	public double getResistivity(int i) {
		return getResistivity(this.resistivity,i);
	}
	public double getResistivityMin(int i) {
		return getResistivity(this.resistivityMin,i);
	}
	public double getResistivityMax(int i) {
		return getResistivity(this.resistivityMax,i);
	}

	public double getResistivity(ParameterDouble d, int i) {
//		System.out.println(resistivity.getUnit().isCompatible(SI.OHM.times(SI.METER)));
		switch (i) {
		case Options.UNITS_OHM_M:
			if(d.getUnit().isCompatible(SI.OHM.times(SI.METER))) return d.get(SI.OHM.times(SI.METER));
			else return 1/(d.get(SI.SIEMENS.divide(SI.METER)));
		case Options.UNITS_S_M:
			if(d.getUnit().isCompatible(SI.OHM.times(SI.METER))) return 1/d.get(SI.OHM.times(SI.METER));
			else return d.get(SI.SIEMENS.divide(SI.METER));
		case Options.UNITS_MS_M:
			if(d.getUnit().isCompatible(SI.OHM.times(SI.METER))) return 1/d.get(SI.OHM.times(SI.METER));
			else return d.get(SI.MILLI(SI.SIEMENS).divide(SI.METER));
		default:
			if(d.getUnit().isCompatible(SI.OHM.times(SI.METER))) return d.get(SI.OHM.times(SI.METER));
			else return 1/(d.get(SI.SIEMENS.divide(SI.METER)));
		}
	}

	public double getThickness(int i) {
		return getThickness(this.thickness,i);
	}
	public double getThicknessMin(int i) {
		return getThickness(this.thicknessMin,i);
	}
	public double getThicknessMax(int i) {
		return getThickness(this.thicknessMax,i);
	}
	public double getThickness(ParameterDouble d, int i) {
		switch (i) {
		case Options.UNITS_FT: return d.get(NonSI.FOOT);
		case Options.UNITS_M: return d.get(SI.METER);
		default:
			return d.get(SI.METER);
		}
	}
	public Node getResistivitySlider() {
		return sr;
	}


	public Node getThicknessSlider() {
		return st;
	}
	public double getWeighting() {
		return this.weighting.get().get();
	}






}