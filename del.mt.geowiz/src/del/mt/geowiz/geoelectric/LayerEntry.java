package del.mt.geowiz.geoelectric;


import java.util.Iterator;

import javax.swing.event.ChangeEvent;

import del.mt.geowiz.MTApplication;
import del.mt.geowiz.fontawesome.FontAwesome;
import del.mt.geowiz.opts.Options;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.effect.BlendMode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;


public class LayerEntry extends SplitPane{
	
	public GeoelectricalEarth earth;
	public GeoelectricalEarthView earthView;
	
	private MTApplication app;
	
	VBox layeringView = new VBox();
	BorderPane layerEdit = new BorderPane();
	ListView<Layer> layerListView;
	

     
	public LayerEntry(MTApplication app) {
		this.app = app;
		this.earth = new GeoelectricalEarth(app);
		
		this.layerListView = new ListView<Layer>(this.earth.layering);
		this.earthView = new GeoelectricalEarthView(app, earth);
		setOrientation(Orientation.VERTICAL);
	
		setupLayerEntry();
				
		getItems().add(layerEdit);
		getItems().add(earthView.chart);
		addLayerListener();
		setLayerListView();
		layerEdit.setCenter(layerListView);
	}

	private void setLayerListView() {
		layerListView.setCellFactory(new Callback<ListView<Layer>, ListCell<Layer>>() {
			
			@Override
			public ListCell<Layer> call(ListView<Layer> param) { 
					return new LayerViewRenderer(earth);
					
				};
		});
	}

	private void addLayerListener() {
		earth.layering.addListener(new ListChangeListener<Layer>() {

			@Override
			public void onChanged(javafx.collections.ListChangeListener.Change<? extends Layer> c) {
				updateLayerEditor();
			}
		});
		earth.listener.addChangeListener(new javax.swing.event.ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				refreshView();
			}
		});
	}


	private void updateLayerEditor() {
		refreshView();
	}


	public void refreshView() {
		app.fwd();
		earthView.updateView();		
		
	}	
	
	
	
//	Button add = new Button();
	private void setupLayerEntry() {
		HBox b = new HBox();
		


//		b.getChildren().add(layerUI.resistivity.createNode());
//		b.getChildren().add(layerUI.thickness.createNode());
//		b.getChildren().add(add);
		b.setBlendMode(BlendMode.MULTIPLY);
		
		layerEdit.setTop(b);
		layerEdit.setCenter(new ScrollPane(layeringView));
		earth.addNewLayer(0,100,100,1,0.000001,10000,0.1,10000);
//		add.setOnAction(new EventHandler<ActionEvent>() {
//			
//			@Override
//			public void handle(ActionEvent event) {
//				
//				
//			}		
//		});
//		add.setMaxHeight(Double.MAX_VALUE);
//
//		add.setGraphic(FontAwesome.PLUS_CIRCLE(1.5f));
//		
	}
	
//	private void addNewLayer(int nth, double thick, double res, double w, double rmin, double rmax, double tmin, double tmax) {
//		// TODO Auto-generated method stub
//		Double t = Double.valueOf(thick);
//		Double r = Double.valueOf(res);
//		Layer l = new Layer(app.options,this,t,r,w,rmin,rmax,tmin,tmax);
//		layering.add(nth,l);
//		Button remove = new Button();
//		Button add = new Button();
//		add.setGraphic(FontAwesome.PLUS(2.0f));
//		remove.setGraphic(FontAwesome.MINUS(2.0f));
//		remove.setMaxHeight(Double.MAX_VALUE);
//		add.setMaxHeight(Double.MAX_VALUE);
//				
//		VBox resB = new VBox();
//		VBox thickB = new VBox();
//		resB.getChildren().add(l.resistivity.createNode());
//		resB.getChildren().add(l.getResistivitySlider());
//		thickB.getChildren().add(l.thickness.createNode());
//		thickB.getChildren().add(l.getThicknessSlider());
//		
//		HBox b = new HBox(resB,thickB,remove,add);
//		VBox v = new VBox();
//		v.getChildren().add(b);
//		layeringView.getChildren().add(nth, v);
//		Iterator<Node> it = layeringView.getChildren().iterator();
//		while(it.hasNext()) {
//			VBox box = (VBox) it.next();
//			HBox box2 = (HBox) box.getChildren().get(0);
//			((Button) box2.getChildren().get(2)).setDisable(!it.hasNext());
//		}
//		add.setOnAction(new EventHandler<ActionEvent>() {			
//			@Override
//			public void handle(ActionEvent event) {
//				int currentIndex = layering.indexOf(l);
//				
//				addNewLayer(currentIndex+1,
//						l.getThickness(Options.UNITS_M),
//						l.getResistivity(Options.UNITS_OHM_M),
//						l.getWeighting(),
//						l.getResistivityMin(Options.UNITS_OHM_M),
//						l.getResistivityMax(Options.UNITS_OHM_M),
//						l.getThicknessMin(Options.UNITS_M),
//						l.getThicknessMax(Options.UNITS_M));
//				
//			}	
//		});
//		
//
//		remove.setOnAction(new EventHandler<ActionEvent>() {
//			
//			@Override
//			public void handle(ActionEvent event) {
//				layering.remove(l);
//				layeringView.getChildren().remove(v);
//				updateLayeringGraph();
//				
//			}
//		});
//		
//		if(nth == 0) remove.setDisable(true);
//		if(nth != 0) updateLayeringGraph();
//	}

//	public void updateAll() {
//		if(layering.size() == 0) addNewLayer(0,Layer.DEFAULT_THICKNESS,Layer.DEFAULT_RESISTIVITY,Layer.DEFAULT_WEIGHT,Layer.DEFAULT_RESISTIVITY_MIN,Layer.DEFAULT_RESISTIVITY_MAX,Layer.DEFAULT_THICKNESS_MIN,Layer.DEFAULT_THICKNESS_MAX); //stops modelling null earths
//		
//		
//	}

	














}