package del.mt.geowiz.geoelectric;

import del.mt.geowiz.fontawesome.FontAwesome;
import del.mt.geowiz.opts.Options;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class LayerViewRenderer  extends ListCell<Layer> {
	
	private GeoelectricalEarth earth;

	public LayerViewRenderer(GeoelectricalEarth earth) {
		this.earth = earth;
	}
	
	@Override public void updateItem(Layer item, boolean empty) {
        super.updateItem(item, empty);
 
        if (empty) {
            setText(null);
            setGraphic(null);
        } else if (item instanceof Layer) {
            setText(null);
            Layer l = (Layer) item;
            Node currentNode = getGraphic();
            Button remove = new Button();
    		Button add = new Button();
    		add.setGraphic(FontAwesome.PLUS(2.0f));
    		remove.setGraphic(FontAwesome.MINUS(2.0f));
    		remove.setMaxHeight(Double.MAX_VALUE);
    		add.setMaxHeight(Double.MAX_VALUE);
    				
    		VBox resB = new VBox();
    		VBox thickB = new VBox();
    		resB.getChildren().add(l.resistivity.createNode());
    		resB.getChildren().add(l.getResistivitySlider());
    		thickB.getChildren().add(l.thickness.createNode());
    		thickB.getChildren().add(l.getThicknessSlider());
    		
    		HBox b = new HBox(resB,thickB,remove,add);
    		VBox v = new VBox();
    		v.getChildren().add(b);
    		

    		add.setOnAction(new EventHandler<ActionEvent>() {			
    			@Override
    			public void handle(ActionEvent event) {
    				int currentIndex = earth.layering.indexOf(l);
    				
    				earth.addNewLayer(currentIndex+1,
    						l.getThickness(Options.UNITS_M),
    						l.getResistivity(Options.UNITS_OHM_M),
    						l.getWeighting(),
    						l.getResistivityMin(Options.UNITS_OHM_M),
    						l.getResistivityMax(Options.UNITS_OHM_M),
    						l.getThicknessMin(Options.UNITS_M),
    						l.getThicknessMax(Options.UNITS_M));
    				
    			}	
    		});
    		
    
    		remove.setOnAction(new EventHandler<ActionEvent>() {
    			
    			@Override
    			public void handle(ActionEvent event) {
    				if(earth.layering.size() > 1)
    				earth.layering.remove(l);    				    				
    				
    			}
    		});
            if (currentNode == null || ! currentNode.equals(v)) {
                setGraphic(v);
            }
        } else {
            setText(item == null ? "null" : item.toString());
            setGraphic(null);
        }
    }
}