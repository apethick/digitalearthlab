package del.mt.geowiz.geoelectric;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.imageio.ImageIO;

import del.mt.geowiz.MTApplication;
import del.mt.geowiz.opts.Options;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.chart.Axis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import open.opengl.viewer2d.Viewer2D;

public class GeoelectricalEarthView {

	public final LineChart<Number,Number> chart;
	public final NumberAxis xAxis = new NumberAxis(-3,3,0.5);
	public final NumberAxis yAxis = new NumberAxis();
	private GeoelectricalEarth earth;
	private MTApplication app;

	public GeoelectricalEarthView(MTApplication app, GeoelectricalEarth earth) {
		this.app = app;
		this.chart = new LineChart<>(xAxis, yAxis);
		this.earth = earth;



		addListeners();
	}

	private void addListeners() {
//		this.earth.listener.addChangeListener(new ChangeListener() {
//
//			@Override
//			public void stateChanged(ChangeEvent e) {
//				updateView();
//			}
//
//
//		});
	}


	public void updateView() {
		chart.getData().clear();


		ObservableList<Layer> layering = earth.layering;
		if(layering.size() > 0) {
			double depth = 0;
			double lastRes = -1;
			Iterator<Layer> layerIt = layering.iterator();
			int nLayers = 0;
			boolean isFirst = true;
			String colour = "#004C80";

			double maxRes = layering.get(0).getResistivity(Options.UNITS_OHM_M);
			double minRes = layering.get(0).getResistivity(Options.UNITS_OHM_M);

			int resUnits = app.options.geoOpts.resistivityUnits.get().get();
			boolean isLog = app.options.geoOpts.resistivityLog.get().get();
			while(layerIt.hasNext()) {
				nLayers++;
				Layer values = layerIt.next();
				double res = values.getResistivity(Options.UNITS_OHM_M);
				maxRes = Math.max(res, maxRes);
				minRes = Math.min(res, minRes);


				if(!isFirst) {
					XYChart.Series seriesHoriz = new XYChart.Series();
					seriesHoriz.nodeProperty().addListener(new ChangeListener<Node>() {
			            @Override
			            public void changed(ObservableValue<? extends Node> ov, Node oldNode, Node newNode) {
			                newNode.setStyle("-fx-bar-fill: "+colour+";");
			                newNode.setStyle(String.format("-fx-stroke: "+colour+";"));
			            }
			        });
					ObservableList<XYChart.Data<Number,Number>> dataHoriz = seriesHoriz.getData();
					XYChart.Data d1 = new XYChart.Data(getConvertedResistivityValue(lastRes,resUnits,isLog),new Double(depth));
					XYChart.Data d2 = new XYChart.Data(getConvertedResistivityValue(res,resUnits,isLog),new Double(depth));
					dataHoriz.add(d1);
					dataHoriz.add(d2);
					chart.getData().add(seriesHoriz);


				}
				XYChart.Series series = new XYChart.Series();

				series.nodeProperty().addListener(new ChangeListener<Node>() {
		            @Override
		            public void changed(ObservableValue<? extends Node> ov, Node oldNode, Node newNode) {
		                newNode.setStyle("-fx-bar-fill: "+colour+";");
		                newNode.setStyle(String.format("-fx-stroke: "+colour+";"));
		            }
		        });




				ObservableList<XYChart.Data<Number,Number>> data = series.getData();

				lastRes = res;
				double thick = layerIt.hasNext() ? -values.getThickness(Options.UNITS_M) : depth/nLayers;
				data.add(new XYChart.Data(getConvertedResistivityValue(res,resUnits,isLog),new Double(depth)));
				depth += thick;

				data.add(new XYChart.Data(getConvertedResistivityValue(res,resUnits,isLog),new Double(depth)));
				chart.setCreateSymbols(false);
				chart.setLegendVisible(false);
				chart.getData().add(series);

				isFirst = false;
			}


			chart.setTitle("Geo-electrical Model");

			yAxis.setLabel("Elevation (m)");
			chart.setAnimated(false);


			switch (app.options.geoOpts.resistivityUnits.get().get()) {
			case Options.UNITS_OHM_M: xAxis.setLabel((isLog ? "Log10 " : "") + "Resistivity (Ohm m)"); break;
			case Options.UNITS_S_M: xAxis.setLabel((isLog ? "Log10 " : "") + "Conductivity (S/m)"); break;
			case Options.UNITS_MS_M: xAxis.setLabel((isLog ? "Log10 " : "") + "Conductivity (mS/m)"); break;
			default:
			}


			double newMinRes = isLog ? Math.min(Math.log10(minRes), -3) : Math.min(0.0000, minRes);
			double newMaxRes = isLog ? Math.max(Math.log10(maxRes), 3) : Math.max(200, maxRes);
			double newMinCond = isLog ? Math.min(Math.log10(1/maxRes), -3) : Math.min(0.0000, 1/maxRes);
			double newMaxCond = isLog ? Math.max(Math.log10(1/minRes), 3) : Math.max(200, 1/minRes);

			switch (app.options.geoOpts.resistivityUnits.get().get()) {
			case Options.UNITS_OHM_M: {
				xAxis.setLowerBound(newMinRes);
				xAxis.setUpperBound(newMaxRes);
				xAxis.setTickUnit(isLog ? 0.5 : getLinearRangeUnit(newMaxRes-newMinRes));
			}; break;
			case Options.UNITS_S_M: {
				xAxis.setLowerBound(newMinCond);
				xAxis.setUpperBound(newMaxCond);
				xAxis.setTickUnit(isLog ? 0.5 : getLinearRangeUnit(newMaxCond-newMinCond));
			};  break;
			case Options.UNITS_MS_M: {
				xAxis.setLowerBound(isLog ? newMinCond + 3 : newMinCond*1000);
				xAxis.setUpperBound(isLog ? newMinCond + 3 : newMaxCond*1000);
				xAxis.setTickUnit(isLog ? 0.5 : getLinearRangeUnit(newMinCond*1000-newMaxCond*1000));
			};  break;
			default:
			}
		}


	}
    private double getLinearRangeUnit(double d) {
    	if(d == 0) return 1;
		int base = (int) Math.log10(Math.abs(d));
		double rangeUnit = Math.pow(10, (double) base)/2;
//		System.out.println(d + " " + rangeUnit);
		return rangeUnit;
	}
	class MouseHandler implements EventHandler<MouseEvent> {
        private XYChart.Data data;
        private Axis<Number> xAxis, yAxis;

        public MouseHandler(XYChart.Data data, Axis<Number> xAxis, Axis<Number> yAxis) {
            this.data=data;
            this.xAxis=xAxis;
            this.yAxis=yAxis;
        }

        @Override
        public void handle(MouseEvent event) {
            //we want these in the axis coordinate frame
            double x =  xAxis.getValueForDisplay(event.getX()).doubleValue();
            double y =  yAxis.getValueForDisplay(event.getY()).doubleValue();

            if (event.getEventType() == MouseEvent.MOUSE_DRAGGED) {
                System.out.println(String.format("(%.2f,%.2f)",x,y));
                //data.setXValue(x);
                //data.setYValue(y);
            }
        }
    }
	public void exportPNG() {
		 WritableImage image = chart.snapshot(new SnapshotParameters(), null);

		    File file = new File("earth_"+GeoelectricalEarth.getCompactDate()+".png");

		    try {
		        ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
		        Desktop.getDesktop().open(file);
		    } catch (IOException e) {
		       e.printStackTrace();
		    }
	}
	private Object getConvertedResistivityValue(double res, int unit, boolean isLog) {
		double reso = res;
		switch (app.options.geoOpts.resistivityUnits.get().get()) {
		case Options.UNITS_OHM_M: reso = res; break;
		case Options.UNITS_S_M: reso =  (1/res); break;
		case Options.UNITS_MS_M: reso = (1/res)*1000; break;
		default:
		}
		if(isLog) {
			reso = Math.log10(reso);
		}
		return reso;
	}
}
