package del.mt.geowiz.geoelectric;

import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.measure.unit.SI;

import del.mt.geowiz.MTApplication;
import del.mt.geowiz.execution.ChangeListener;
import del.mt.geowiz.opts.Options;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.WritableImage;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class GeoelectricalEarth {

	public ChangeListener listener = new ChangeListener();
	public ObservableList<Layer> layering = FXCollections.observableArrayList();
	private MTApplication app;

	public GeoelectricalEarth(MTApplication app) {
		this.app = app;
		addNewLayer(0, 100, 100, 1, 0.00001, 10000, 0.0000001, 100000);
	}

	public void addNewLayer(int nth, double thick, double res, double w, double rmin, double rmax, double tmin, double tmax) {
		Double t = Double.valueOf(thick);
		Double r = Double.valueOf(res);
		Layer l = new Layer(app.options,this,t,r,w,rmin,rmax,tmin,tmax);
		layering.add(nth,l);
	}

	public void setChanged() {
		listener.setValuesChanged();
	}


	public void setEarth(GeoelectricalEarth earth) {
		if(earth != this) {
			layering.clear();
			for(Layer l : earth.layering) {
				this.layering.add(new Layer(l));
			}
		}
	}
	public double getResistivityAtDepth(double l) {
		double depth = 0;
		for(Layer layer : layering) {
			if(depth <= l) return layer.getResistivity(Options.UNITS_OHM_M);
			double thick = layer.getThickness(Options.UNITS_M);
			depth += thick;
		}
		return layering.get(layering.size()-1).getResistivity(Options.UNITS_OHM_M);
	}

	public void save() {
		FileChooser c = new FileChooser();
		String title = "Save Earth File";
		c.setTitle(title);
		String path = "./";
		if (path != null) {
			c.setInitialDirectory(new File(path));
		}
		c.getExtensionFilters().add(new ExtensionFilter("Earth Output", new ArrayList<String>(Arrays.asList("*.earth"))));

		File l = c.showSaveDialog(null);
		if(l == null) {

		} else {
			if(!l.getPath().toLowerCase().endsWith(".earth")) {
				l = new File(l.getPath() + ".earth");
			}
			if(l.exists()) {
				l.delete();
			}
			try {

				BufferedWriter out = new BufferedWriter(new FileWriter(l));
//				out.write("resistivity(Ohmm),thickness(m),weight(%),resistivity_min(Ohmm),resistivity_max(Ohmm),thickness_min(m),thickness_max(m)");
				out.write(layering.size());
				out.newLine();
				double depth = 0;
				for(Layer layer : layering) {
					double res = layer.getResistivity(Options.UNITS_OHM_M);
					double thick = layer.getThickness(Options.UNITS_M);
					double weight = layer.getWeighting();
					double resmin = layer.getResistivityMin(Options.UNITS_OHM_M);
					double resmax = layer.getResistivityMax(Options.UNITS_OHM_M);
					double thickmin = layer.getThicknessMin(Options.UNITS_M);
					double thickmax = layer.getThicknessMax(Options.UNITS_M);
					out.write(res + "," + thick+ "," + weight+ "," + resmin+ "," + resmax+ "," + thickmin+ "," + thickmax);
					out.newLine();
					depth -= Double.valueOf(layer.getThickness(Options.UNITS_M));
				}
				out.close();
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}
	public void load() {
		FileChooser c = new FileChooser();
		String title = "Load Earth File";
		c.setTitle(title);
		String path = "./";
		if (path != null) {
			c.setInitialDirectory(new File(path));
		}
		c.getExtensionFilters().add(new ExtensionFilter("Earth Output", new ArrayList<String>(Arrays.asList("*.earth"))));

		File l = c.showOpenDialog(null);
		layering.clear();

		if(l == null) {

		} else {
			if(l.exists()) {

				try {
					BufferedReader in = new BufferedReader(new FileReader(l));
					String line = "";
					in.readLine(); //skip header
					layering.clear();
					int nth = 0;
					while((line = in.readLine()) != null) {
						String [] split = line.split(",");
						int ith = 0;
//						double depth = Double.valueOf(split[ith++]);
						double res = Double.valueOf(split[ith++]);
						double thick = Double.valueOf(split[ith++]);
						double weight = Double.valueOf(split[ith++]);
						double resmin = Double.valueOf(split[ith++]);
						double resmax = Double.valueOf(split[ith++]);
						double thickmin = Double.valueOf(split[ith++]);
						double thickmax = Double.valueOf(split[ith++]);
						addNewLayer(nth++,thick,res,weight,resmin,resmax,thickmin,thickmax);
					}
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}


		}
	}
	public static String getCompactDate() {
		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
		return dateFormat.format(date);
	}

	public void exportCSV() {

		    File file = new File("earth_"+getCompactDate()+".csv");

		    try {

				BufferedWriter out = new BufferedWriter(new FileWriter(file));
				double depth = 0;
				out.write("depth(m),resistivity(Ohmm),thickness(m),weight(%),resistivity_min(Ohmm),resistivity_max(Ohmm),thickness_min(m),thickness_max(m)");
				out.newLine();
				for(Layer layer : layering) {

					double res = layer.getResistivity(Options.UNITS_OHM_M);
					double thick = layer.getThickness(Options.UNITS_M);
					double weight = layer.getWeighting();
					double resmin = layer.getResistivityMin(Options.UNITS_OHM_M);
					double resmax = layer.getResistivityMax(Options.UNITS_OHM_M);
					double thickmin = layer.getThicknessMin(Options.UNITS_M);
					double thickmax = layer.getThicknessMax(Options.UNITS_M);

					out.write(-depth + "," + res + "," + thick+ "," + weight+ "," + resmin+ "," + resmax+ "," + thickmin+ "," + thickmax);

					out.newLine();
					depth -= Double.valueOf(layer.thickness.get(SI.METER));
				}
				out.close();
				Desktop.getDesktop().open(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
	}

	public void saveIntervals() {
		 File file = new File("earth_"+getCompactDate()+"_intervals_1m.csv");

		    try {

				BufferedWriter out = new BufferedWriter(new FileWriter(file));
				double depth = 0;
				double layerDepth = 0;
				int currentLayerIndex = 0;
				Layer currentLayer = layering.get(currentLayerIndex);
				out.write("depth(m),resistivity(Ohmm),conductivity(mS/m)");
				out.newLine();


				for(Layer layer : layering) {

					double res = layer.getResistivity(Options.UNITS_OHM_M);
					double thick = layer.getThickness(Options.UNITS_M);
					double weight = layer.getWeighting();
					double resmin = layer.getResistivityMin(Options.UNITS_OHM_M);
					double resmax = layer.getResistivityMax(Options.UNITS_OHM_M);
					double thickmin = layer.getThicknessMin(Options.UNITS_M);
					double thickmax = layer.getThicknessMax(Options.UNITS_M);
					double nextDepth = layerDepth + Double.valueOf(layer.thickness.get(SI.METER));

					while(nextDepth > depth) {
//						System.out.println(depth);
						out.write(depth + "," + res +"," + (1000/res));
						out.newLine();
						depth++;
					}
//					out.write(-depth + "," + res + "," + thick+ "," + weight+ "," + resmin+ "," + resmax+ "," + thickmin+ "," + thickmax);

//					out.newLine();
					layerDepth = nextDepth;

				}
				out.close();
				Desktop.getDesktop().open(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
	}




}
