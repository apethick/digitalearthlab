package del.mt.geowiz.execution;

import java.awt.EventQueue;

import javax.swing.JCheckBox;

public class ChangeListener {
	
	/*Allows global control over a single change so a single thread
	 * can check only a single value for changes 
	*/ 
	
	
	
	//Main Change Value
	private boolean hasValuesChanged = false;
	JCheckBox c;
	
	public void addChangeListener(final javax.swing.event.ChangeListener a) {
			Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				for(javax.swing.event.ChangeListener l : c.getChangeListeners()) c.removeChangeListener(l);
					c.addChangeListener(a);
				}
			});
			EventQueue.invokeLater(t);
	}
	
	public ChangeListener() {
		Thread t = new Thread(new Runnable() {
			
			@Override
			public void run() {
				c = new JCheckBox("",false);		
			}
		});
		EventQueue.invokeLater(t);

		
	}
	public ChangeListener(boolean b) {
		hasValuesChanged = b;
		
	}

	public void setValuesChanged() {
		hasValuesChanged = true;
		try {
			c.setSelected(!c.isSelected());
		} catch(Exception e) {
			
		}
	}

	public boolean hasValuesChanged() {
		Boolean b = new Boolean(hasValuesChanged);
		hasValuesChanged = false;
		return b;
	}
	


	
	
}
