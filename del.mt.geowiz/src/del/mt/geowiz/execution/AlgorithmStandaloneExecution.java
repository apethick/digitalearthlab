package del.mt.geowiz.execution;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.List;
import java.util.Scanner;

import javax.swing.event.ChangeEvent;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.exec.ShutdownHookProcessDestroyer;

import del.mt.geowiz.fwd.MTForward1DInstance;
import sun.misc.IOUtils;





public class AlgorithmStandaloneExecution {
	public boolean IS_COMPLETE = false;
	public long COMPLETION_TIME = 0;
	int MAX_ATTEMPT = 2;
	long startTime;
	DefaultExecutor executor = new DefaultExecutor();
	ByteArrayOutputStream out = new ByteArrayOutputStream();
    ByteArrayOutputStream err = new ByteArrayOutputStream();
    PumpStreamHandler  handler = new PumpStreamHandler(out, err, null);
    InputStream stdout;
    InputStream stderr;
    BufferedReader outStream;

	private List<String> args;
	private String executable;

	ChangeListener postChangeListener = new ChangeListener();
	ChangeListener preChangeListener = new ChangeListener();
	public MTForward1DInstance algorithm;
	public void addPreChangeListener(javax.swing.event.ChangeListener event) {
		postChangeListener.addChangeListener(event);
	}
	public void addPostChangeListener(javax.swing.event.ChangeListener event) {
		postChangeListener.addChangeListener(event);
	}
	
	
	
	
	public AlgorithmStandaloneExecution(final MTForward1DInstance a, List<String> args) {
		this.algorithm = a;
		this.executable = a.getAlgorithmExecutionFile();
		this.args = args;
		executor.setExitValues(null);
		executor.setWatchdog(new ExecuteWatchdog(ExecuteWatchdog.INFINITE_TIMEOUT));
	    executor.setProcessDestroyer(new ShutdownHookProcessDestroyer());	   
	    executor.setStreamHandler(handler);
	}
	public static String convertStreamToString(InputStream is) { 
	    return new Scanner(is).useDelimiter("\\A").next();
	}
	
	public void stop() {
		executor.getWatchdog().destroyProcess();
		IS_COMPLETE = true;
		COMPLETION_TIME = System.currentTimeMillis() - startTime;
	}
	
	public void setCWD(String cwd) {
		executor.setWorkingDirectory((new File(cwd)).getAbsoluteFile());
	}
	
	public void start(int attempt) {
	
		startTime = System.currentTimeMillis();
		
		 try {
			 copyFiles();
			 this.algorithm.writeFiles(executor.getWorkingDirectory());
			 Thread.sleep(1);
			 
			 String st = executor.getWorkingDirectory().getAbsolutePath() + File.separator + executable + " " + executor.getWorkingDirectory().getAbsolutePath();
//			 Runtime.getRuntime().exec(st);
//			 final CommandLine cl = new CommandLine(executor.getWorkingDirectory().getAbsolutePath() + File.separator + executable);
			 final CommandLine cl = new CommandLine(executable);
			 
			 System.out.println("Running "  + executable + " in " + executor.getWorkingDirectory().getAbsolutePath() );
			 System.out.println(args);
	    for (String arg : args) cl.addArgument(arg, false);
	    		executor.execute(cl);
	         stdout = out.size() == 0 ? null : new ByteArrayInputStream(out.toByteArray());
	         stderr = err.size() == 0 ? null : new ByteArrayInputStream(err.toByteArray());	         
		 } catch(Exception e) {
			 if(attempt <= MAX_ATTEMPT) {
				 try {
					Thread.sleep(25);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				 start(attempt + 1);
			 }
			 else e.printStackTrace();
		 }
		 IS_COMPLETE = true;
		 COMPLETION_TIME = System.currentTimeMillis() - startTime;
		 this.algorithm.readFiles(executor.getWorkingDirectory());
		 
	}
	private void copyFiles() {
		
		File dir = algorithm.getAlgorithmDirectory();
		
		if(dir.isFile()) {
			try {					
				CopyUtils.copyFile(dir, new File(executor.getWorkingDirectory().getPath() + "/" + dir.getName()));
			} catch(Exception e) {
				
			}
		} else {
			for(File f : algorithm.getAlgorithmDirectory().listFiles()) {
				if(f.isFile()) {
					try {
						if(new File(executor.getWorkingDirectory().getPath() + "/" + f.getName()).exists()) {
							
						} else {
							CopyUtils.copyFile(f, new File(executor.getWorkingDirectory().getPath() + "/" + f.getName()));
						}
					
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

//	public String nextOutputText() {
//		try {
//			StringWriter writer = new StringWriter();
//			IOUtils.copy(stdout, writer);
//			String s = writer.toString();
//			return s;
//		} catch(Exception e) {
//			
//		}
//		return null;
//	}

    
}
