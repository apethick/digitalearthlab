package del.mt.geowiz.execution;

import java.io.File;
import java.util.ArrayList;


public class AlgorithmStandaloneExecutionHandler {
	int MAX_RUNNING_THREADS = SystemsAnalyst.getNumberOfAvailableProcessors();
		
	ArrayList<AlgorithmStandaloneExecution> queued = new ArrayList<AlgorithmStandaloneExecution>();
	ArrayList<AlgorithmStandaloneExecution> executing = new ArrayList<AlgorithmStandaloneExecution>();
	ArrayList<AlgorithmStandaloneExecution> completed = new ArrayList<AlgorithmStandaloneExecution>();
	ArrayList<Thread> executingThreads = new ArrayList<Thread>();
	  boolean [] availableIndexes = new boolean[MAX_RUNNING_THREADS];

	
	public int MODE = MODE_STATIC_QUEUE;

	private String executionDirectory = "/tmp/";
	public final static int MODE_DYNAMIC_QUEUE = 0; //WILL CONTINUE TO GROW
	public final static int MODE_STATIC_QUEUE = 1; //WILL KICK OUT NEXT RUNNING THREAD
			
	
	ArrayList<ChangeListener> activeListeners = new ArrayList<ChangeListener>();
	ArrayList<ChangeListener> completionListeners = new ArrayList<ChangeListener>();
	
	public void add(AlgorithmStandaloneExecution e) {
		if(!isExecuting()) changeActiveListeners();
		switch (MODE) {
		case MODE_DYNAMIC_QUEUE:
			if(isFull()) {
				queued.add(e);
			} else {
				executing.add(e);
			
				start(e);
			}
			break;
		case MODE_STATIC_QUEUE:	
			if(isFull()) {		
				queued = new ArrayList<AlgorithmStandaloneExecution>();
		
				queued.add(e);
			} else {
				executing.add(e);
				start(e);
		}
		break;
		default:
			break;
		}		
	}
	
	public void addActiveChangeListener(ChangeListener l) {
		activeListeners.add(l);
	}
	public void addCompletionChangeListener(ChangeListener l) {
		completionListeners.add(l);		
	}
	
	private void changeActiveListeners() {
		for(ChangeListener l : activeListeners) l.setValuesChanged();
	}
	private void changeCompletionListeners() {
		for(ChangeListener l : completionListeners) l.setValuesChanged();
	}

	public int getAvailableCore() {
		for(int i = 0 ; i < availableIndexes.length ; i++) if(availableIndexes[i]) return i;
		return -1;
	}
	
	private synchronized void start(final AlgorithmStandaloneExecution e) {
		final int core = getAvailableCore();

		if(core != -1) {
			
			Thread t = new Thread(new Runnable() {			
				@Override
				public void run() {
					try {
						
						availableIndexes[core] = false;			
						
						String cwd = executionDirectory + "/" + "Core_" + (core + 1);
						File f = new File(cwd);
						f.mkdirs();
						e.setCWD(cwd);
						e.start(0);
						while(!e.IS_COMPLETE) {
							System.out.println("STILL GOING HUH?");
							Thread.sleep(500);						
						}
						remove(e);						
						completed.add(e);
						availableIndexes[core] = true;
						recheck();
						
						
					} catch(Exception e) {
						availableIndexes[core] = true;
					}
				}
			});
			
			t.start();			
			executingThreads.add(t);
			
		} else {
			queued.add(0,e);
		}
		
	}
	private synchronized void recheck() {
		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		if(!isFull()) {
			if(queued.size() > 0) {		
				AlgorithmStandaloneExecution e = queued.remove(0);
				executing.add(e);				
				start(e); //always take head of queue
			} else 	if(executing.size() == 0 && queued.size() == 0) {
				changeCompletionListeners();
			}
		}
		
	}	
	private void remove(AlgorithmStandaloneExecution e) {
		executing.remove(e);
	}

	private boolean isFull() {
		return (executing.size() == MAX_RUNNING_THREADS);
	}
	
	public synchronized boolean isExecuting() {
		recheck();
		if(executing.size() > 0 || queued.size() > 0) {			
			return true;
		} else {
			for(int i = 0 ; i < executingThreads.size() ; i++) {
				Thread t = executingThreads.get(i);
				if(!t.isAlive()) {

					executingThreads.remove(i);
				}
				else return true;
			}
			return false;
		}
		
	
	}
	
	public void waitThreads() {
		while(isExecuting()) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public AlgorithmStandaloneExecutionHandler(String executionDirectory) {
		this.executionDirectory = executionDirectory;
		for(int i = 0 ; i < MAX_RUNNING_THREADS ; i++) {
			availableIndexes[i] = true;
		}
	}
	
	public void setMaxRunningThreads(int max) {
		if(max > SystemsAnalyst.getNumberOfAvailableProcessors()) {
			System.err.println("TOO MANY CORES");
		} else {
			this.MAX_RUNNING_THREADS = max;
			while(isExecuting()) {
				
			}
			availableIndexes = new boolean[MAX_RUNNING_THREADS];
			for(int i = 0 ; i < MAX_RUNNING_THREADS ; i++) {
				availableIndexes[i] = true;
			}
		}
	}
	
//	public static void main(String[] args) {
//		
//		GlobalExecutionHandler h = new GlobalExecutionHandler("c:\\temp\\");
//		h.MODE = GlobalExecutionHandler.MODE_DYNAMIC_QUEUE;
//		String execDir = "I:\\Builds\\CSEMoMatic_v1.3\\Execution\\";
//		for(int j = 0 ; j < 200 ; j++){ 
//		for(int i = 1 ; i <= 1 ; i++) {
//			String dir = execDir + "Core_" + i;
//			String exe = dir + "\\DIPOLE1D_WINDOWS.EXE";
//			AlgorithmExecution e = new AlgorithmExecution(exe,new ArrayList<String>(),dir);
//			h.add(e);			
////			System.out.println("q = " + h.queued.size());
////			System.out.println("e = " + h.executing.size());
////			System.out.println("c = " + h.completed.size());
//		}
//		h.waitThreads();
//		for(AlgorithmExecution e : h.completed) {
//			System.out.println(e.nextOutputText());
//		}
//		}
//	}
}
