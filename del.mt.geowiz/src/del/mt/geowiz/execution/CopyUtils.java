package del.mt.geowiz.execution;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;

public class CopyUtils {

		  public static void copyFile(File in, File out) throws Exception {
//			  if(out.exists()) {
//				  if(in.length() == out.length()) return;
//			  }
			  System.out.println("COPYING FILE " + in.getAbsolutePath() + "TO " + out.getAbsolutePath());
		    FileInputStream fis  = new FileInputStream(in);
		    
		    FileOutputStream fos = new FileOutputStream(out);
		    try {
		        byte[] buf = new byte[1024];
		        int i = 0;
		        while ((i = fis.read(buf)) != -1) {
		            fos.write(buf, 0, i);
		        }
		    } 
		    catch (Exception e) {
		        throw e;
		    }
		    finally {
		        if (fis != null) fis.close();
		        if (fos != null) fos.close();
		    }
		  }
		  
		  public static void copyFiles(File in, ArrayList<String> directories) throws Exception {
			    FileInputStream fis  = new FileInputStream(in);
			    ArrayList<FileOutputStream> fos = new ArrayList<FileOutputStream>();
			    for (String f : directories) {
			    	File dir = new File(f);
			    	dir.mkdirs();
			    	File outputFile = new File(f+ in.getName());
			    	if(!outputFile.exists())
			    		fos.add(new FileOutputStream(outputFile));
			    }
			    
			    try {
			        byte[] buf = new byte[1024];
			        int i = 0;
			        while ((i = fis.read(buf)) != -1) {
			        	for (int j = 0 ; j< fos.size(); j++) {
			        		fos.get(j).write(buf, 0, i);
			        	}
			        }
			    } 
			    catch (Exception e) {
			        throw e;
			    }
			    finally {
			        if (fis != null) fis.close();
			        for (int j = 0 ; j < fos.size() ; j++) {
			        	if (fos.get(j) != null) 
			        		fos.get(j).close();
			        }			       
			    }
			  }



}
