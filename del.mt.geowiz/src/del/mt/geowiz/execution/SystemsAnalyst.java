package del.mt.geowiz.execution;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

public class SystemsAnalyst {
		
	public static native long getProcessCPUTime ();
	public static final int MAC = 2;
	public static final int LINUX = 1;
	public static final int WINDOWS = 0;
	
	public static String getOSName() {
		return System.getProperty("os.name");
	}
		
	public static int getNumberOfAvailableProcessors() {
		 int processors = java.lang.Runtime.getRuntime().availableProcessors();	 
		 return processors;
	}
	
	public static String getReverseDate() {
		 Date date = new Date();
		 DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		return dateFormat.format(date); 
	}
	
	public static String getFormatDate(String format) {
		 Date date = new Date();
		 DateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(date); 
	}
	
	public static String getDate() {
		 Date date = new Date();
		 DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		return dateFormat.format(date); 
	}
	
	public static double getFreeMemory() {
		 double freeMemory = java.lang.Runtime.getRuntime().freeMemory();	
		 Set<String> env = java.lang.System.getenv().keySet();

		 return freeMemory;
	}
	
	public static String getProcessorArchitecture() {
		return java.lang.System.getenv().get("PROCESSOR_ARCHITECTURE");
	}
	
	public static String getProcessorIdentifier() {
		return java.lang.System.getenv().get("PROCESSOR_IDENTIFIER");
	}
	
	public static String getOS() {
		return java.lang.System.getenv().get("OS");
	}
	
	public static double getUsedMemory() {
		double usedMemory = java.lang.Runtime.getRuntime().totalMemory()-getFreeMemory();	 	 
		 return usedMemory;
	}
	
	public static double getTotalMemory() {
		double totalMemory = java.lang.Runtime.getRuntime().totalMemory();	 	 
		 return totalMemory;
	}

	public static String getUser() {
		return java.lang.System.getenv().get("USERNAME");
	}

	public static Dimension getSreenSize() {
		return Toolkit.getDefaultToolkit().getScreenSize();	
	}
	 
	
	public static String getSystemInfo() {
		String s = "Optimising System....\n";
		s += ("\t..." + "Operating system " + getOS() + "\n");
		s += ("\t..." + "OS Name " + getOSName() + "\n");
		s += ("\t..." + "Processor architecture " + getProcessorIdentifier() + "\n");
		
		s += ("\t..." + getNumberOfAvailableProcessors() + " cores present" + "\n");
		s += ("\t..." + getFreeMemory()/10000 + "\tJVM Free Memory  (Mb)" + "\n");
		s += ("\t..." + getUsedMemory()/10000 + "\tJVM Used Memory  (Mb)" + "\n");
		s += ("\t..." + getTotalMemory()/10000 + "\tJVM Total Memory (Mb)") + "\n\n\n";
		
		Set <String> keys = java.lang.System.getenv().keySet();
		for (String st : keys) {
			s += (st + "\t" + java.lang.System.getenv().get(st) + "\n");
		}
		return s;
	}
	
	public static void printSystemInfo() {
		
		System.out.println("\t..." + "Operating system " + getOS());
		System.out.println("\t..." + "OS Name " + getOSName());
		System.out.println("\t..." + "Processor architecture " + getProcessorIdentifier());
		
		System.out.println("\t..." + getNumberOfAvailableProcessors() + " cores present");
		System.out.println("\t..." + getFreeMemory()/10000 + "\tJVM Free Memory  (Mb)");
		System.out.println("\t..." + getUsedMemory()/10000 + "\tJVM Used Memory  (Mb)");
		System.out.println("\t..." + getTotalMemory()/10000 + "\tJVM Total Memory (Mb)");
		
		Set <String> keys = java.lang.System.getenv().keySet();
		for (String s : keys) {
			System.out.println(s + "\t\t" + java.lang.System.getenv().get(s));
		}
	}

	public static void main(String[] args) {
		printSystemInfo();
	}


}

