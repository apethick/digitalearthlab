import matplotlib.pyplot as plt
import math
import cmath
import time
import mt1dfwd as mt
import mtdat as dat
import mtearth as earth;
import mtinvert as inv
import datetime
import time
import numpy as np
import mtglobal as mtglobal

if(mtglobal.verbosity >= 1) :
    print('====================================='); 
    print('        1DMT Inversion v1.0          ');
    print('           Developed by              ');
    print('          Andrew Pethick             ');
    print('                and                  ');
    print('          Stefan Spears              ');
    print('             July 2015               ');        
    print('=====================================');
    print('');
    print('Starting Program');

###########SET GLOBAL PARAMETERS##############
mtglobal.verbosity = 2;
#mtglobal.dir = 'c:/Temp/MT_Benchmark/';
mtglobal.dir = '/Users/spearzo/Documents/Thesis/MT_Benchmark/';

###########INITIALISE DATESTAMP##############
datestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d%H%M%S')



##### INITIALISE EARTH MODEL##################

resistivities = np.array([10.0, 16.0,13.0,12.0,1.0,11.0,14.0,1.60,41.0,101.0]);
thicknesses = np.array([10.0, 15.0,30.0,50.0,70.0,100.0,150.0,160.0,190.0]);
startingEarth = earth.MTEarth(resistivities,thicknesses);

##### LOAD IN DATA ###########################
filename = 'example.data';
fieldData = dat.load(filename);
#fieldData.plotData(dir + "Field_"  + datestamp + ".png");
#fieldData.printData();

##### FORWARD MODEL TO GET STARTING RESULT####
syntheticData = mt.mt1dfwdalt(startingEarth, fieldData.frequencies);

##### OUTPUT DATA ############################
#syntheticData.printData();
#syntheticData.plotData(dir + "Forward_"  + datestamp + ".png");

###### INVERT WITH THE GIVEN EARTH MODEL #####
maxIterations = 10;
tolerance = 1.00;
bet=0.01;#Damping factor
pf = 3/100.0; #peturbation factor
armin = 0.0001;
armax = 10000;
    
inversion = inv.mtinversion(startingEarth, fieldData, maxIterations, tolerance, bet,pf,armin,armax);
inversion.invert()


#####################PLOT DATA########################
dpires = 150
inversion.exportAllResults(mtglobal.dir,datestamp,dpires)

if(mtglobal.verbosity >= 1) : print(str(mt.ntimes) + ' 1D forward models completed')    

