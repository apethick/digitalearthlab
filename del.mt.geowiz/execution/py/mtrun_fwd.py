import matplotlib.pyplot as plt
import math
import cmath
import time
import mt1dfwd as mt
import mtdat as dat
import mtearth as earth;
import mtinvert as inv
import datetime
import time
import numpy as np
import mtglobal as mtglobal

import sys

if(mtglobal.verbosity >= 1) :
    print('====================================='); 
    print('        1DMT Inversion v1.0          ');
    print('           Developed by              ');
    print('          Andrew Pethick             ');
    print('                and                  ');
    print('          Stefan Spears              ');
    print('             July 2015               ');        
    print('=====================================');
    print('');
    print('Starting Program');

###########SET GLOBAL PARAMETERS##############
mtglobal.verbosity = 0;
#mtglobal.dir = 'c:/Temp/MT_Benchmark/';
mtglobal.dir = './';

###########INITIALISE DATESTAMP##############
datestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d%H%M%S')

resistivities,thicknesses = np.loadtxt(sys.argv[1] + "earth.txt", unpack=True) 

##### INITIALISE EARTH MODEL##################

#resistivities = np.array([10.0, 16.0,13.0,12.0,1.0,11.0,14.0,1.60,41.0,101.0]);
#thicknesses = np.array([10.0, 15.0,30.0,50.0,70.0,100.0,150.0,160.0,190.0]);
startingEarth = earth.MTEarth(resistivities,thicknesses);

##### LOAD IN DATA ###########################
filename = sys.argv[1] + 'data.txt';
fieldData = dat.load(filename);
syntheticData = mt.mt1dfwdalt(startingEarth, fieldData.frequencies);
syntheticData.save(sys.argv[1] + 'data_synth.txt');