import math
import cmath
import time
import mt1dfwd as mt
import mtdat as dat
import mtearth as earth
import numpy as np
import copy

class Iteration:
    #in every iteration we get these objects
    earthModel = None;
    syntheticData = None;
    iterationNumber = 1;
    fieldData = dat.MTData();
    
    
    def __init__(self, iterationNumber, earthModel, syntheticData, fieldData):
        self.earthModel = earthModel;
        self.syntheticData = syntheticData;
        self.iterationNumber = iterationNumber;
        self.fieldData = fieldData;
        
    