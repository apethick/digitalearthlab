###### INVERT WITH THE GIVEN EARTH MODEL #####
maxIterations = 10;
tolerance = 1.00;
beta=0.01;#Damping factor
pf = 3/100.0; #peturbation factor
armin = 0.0001;
armax = 10000;
#Values above are used to initialize their respective parameters#
#These values are then passed to the program to be used in calculation of the earth model#