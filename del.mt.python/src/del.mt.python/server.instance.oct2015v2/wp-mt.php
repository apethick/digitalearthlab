	<?php
	/**
	 * Plugin Name: Digital Earth Lab 1D MT Forward Modelling
	 * Plugin URI: http://digitalearthlab.com
	 * Description: 1D Magnetotelluric Forward Modelling
	 * Version: 0.0
	 * Author: Andrew Pethick
	 * Author URI: http://digitalearthlab.com
	 * License: WTFPL
	 */
	 
	
	echo initialise('');

	
	    $response = "";
	   
	  function initialise( $attributes ){     
			ob_start();
			global $response;

		include("form.php");
	 	switch ($_REQUEST['forward']){
			case "Forward Model":
			
			break;
			case "Select Case 1":
			select_case('1');
			
			break;
			case "Select Case 2":
			select_case('2');
			break;
		}
	 	
	 
	
		run();	 
		
		$output_string=ob_get_contents();;
		ob_end_clean();
		return $output_string;	

	}
	
	function run() {
		$resistivities = ($_GET['message_resistivities']);
		$thicknesses = filter_var($_GET['message_thicknesses']);
		$data = filter_var($_GET['message_data']);
	
	  	$string = $data.'_'.$resistivities.'_'.$thicknesses;
		
	  	
	  	fwd($string);		  
	}
	
	  
 	function select_case($input) {
		switch ($input){		
			case "1":
		     $_POST['message_resistivities'] = '1 10 100';
			 $_POST['message_thicknesses'] = '100 500';

			break;
			case "2":
		     $_POST['message_resistivities'] = '1000 10 1000';
			 $_POST['message_thicknesses'] = '50 100';

			break;
			case "3":
		     $_POST['message_resistivities'] = '1.5 30 1.5';
			 $_POST['message_thicknesses'] = '1000 50';

			break;			
		  }

	}

	
	function flip($arr){
	    $out = array();
	
	    foreach ($arr as $key => $subarr)
	    {
	            foreach ($subarr as $subkey => $subvalue)
	            {
	                 $out[$subkey][$key] = $subvalue;
	            }
	    }
	
	    return $out;
	}
	
	function fwd($string) {		
		 $string = str_replace('\t',',',$string);
		 $string = str_replace('\n',',',$string);
		 $string = str_replace('\r',',',$string);
		 $string = str_replace(' ',',',$string);
		 $string = preg_replace('#\s+#',',',trim($string));
			

	     global  $mt_output;
	     $file = 'mtrun.py';
	     $output = shell_exec('python ' . __DIR__ . '/' . $file . ' ' . $string);
	     $images = explode('./', $output);
	     $image1 = './' . $images[1];
	     $image2 = './' . $images[2];
	     #echo "$image1", '\r\n';
	     #echo "$image2", '\r\n';
	     echo '<img src="'.$image1.'"/>';
	     echo '<img src="'.$image2.'"/>';
	     ##echo $images[0];
	     
	   	 
	     
	    
	     
		
		 

 	}
