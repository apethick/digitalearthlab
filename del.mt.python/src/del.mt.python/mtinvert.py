import math
import cmath
import time
import mt1dfwd as mt
import mtdat as dat
import mtearth as earth
import numpy as np
import copy
import mtiteration as mtit
import mtglobal
import matplotlib.pyplot as plt
import mtinvparameters as mtinvparams

#Contains all the information needed to run the inversions#
#Data in this class will be passed to every instance of iteration#



class mtinversion:
    bet=0.01;#Damping factor
    pf = 3/100.0; #peturbation factor
    armin = 0.0001; #minimum apparent resistivity
    armax = 1000000; #maximum apparent resistivity
    iterations = []; #the iterations will be stored in an array
    maxIterations = 20; #the maximum amount of times inversion will be run
    tolerance = 1.00; 
       
    def __init__(self, startingEarth, startingData, mtinvparms): #Creates an array of the starting earth model using starting earth data and the inversion parameters#
        self.startingEarth = startingEarth;
        self.startingData = startingData;
        self.maxIterations = mtinvparams.maxIterations;
        self.tolerance = mtinvparams.tolerance;
        self.bet = mtinvparams.beta;
        self.pf = mtinvparams.pf;
        self.armin = mtinvparams.armin;
        self.armax = mtinvparams.armax;
        self.iterations = []
        

    def invert(self):
        if(mtglobal.verbosity >= 1) : #depending on the level of verbosity the info below will be printed to the screen#
            print('Starting inversion with the following parameters');
            print('...Max Iterations      = ' + str(self.maxIterations));
            print('...Tolerance           = ' + str(self.tolerance));
            print('...Beta Smoothing      = ' + str(self.bet));
            print('...Peturbation Factor  = ' + str(self.pf));
            print('...Minimum Resistivity = ' + str(self.armin));
            print('...Maximum Resistivity = ' + str(self.armax));          
        self.iterations = [];
        iteration = 1;
        syntheticData = mt.mt1dfwd(self.startingEarth,self.startingData.frequencies);
        startingIteration = mtit.Iteration(iteration, self.startingEarth, syntheticData,self.startingData)
        self.iterations.append(startingIteration);    
        currentIteration = startingIteration;
        while(not self.isFinished(self.iterations, self.maxIterations, self.tolerance)) :
            self.iterations.append(self.computeNextIteration(self.iterations[len(self.iterations)-1]));
            
    def isFinished(self,iterations, maxIterations, tolerance) : #checks to see if the iterations should be finished at this point#
        finalIteration = iterations[len(iterations)-1];    
        aresrel = finalIteration.fieldData.getApparentResistivityRelativePercentageError();
        phaserel = finalIteration.fieldData.getPhaseRelativePercentageError();
        if finalIteration.iterationNumber >= maxIterations :
            return True;
        elif finalIteration.fieldData.computeQError(finalIteration.syntheticData, aresrel,phaserel) < tolerance :
            return True;
        return False;
    
    def computeNextIteration(self,currentIteration): #if iterations are not max computes the next iteration#
        if(mtglobal.verbosity >= 2) : print('Computing Iteration #%d' % currentIteration.iterationNumber);
        A = self.computeJacobian(currentIteration); 
        bet = self.bet
        pf = self.pf
        armax = self.armax
        armin = self.armin
        
        D=np.diag(np.power(np.diag(np.dot(A.T,A)),-0.5));    
        DA1 = np.dot(D,A.T);
        DA2 = np.dot(A,D);
        DA = np.dot(DA1,DA2);
        
        #Check for infinite or NaN sensitivities 
        nanIndex = np.isnan(DA);
        DA[nanIndex] = 0.0001;
        infIndex = np.isinf(DA);
        DA[infIndex] = 1000000;
        
        
        
        M = 2*currentIteration.earthModel.getNumberOfLayers() - 1; #Number of unknowns
        beta = np.multiply(bet,np.eye(M)); #damping matrix
    
      
        aerr = currentIteration.fieldData.getApparentResistivityRelativePercentageError();
        perr = currentIteration.fieldData.getPhaseRelativePercentageError();
        err = currentIteration.fieldData.computeErrorArray(currentIteration.syntheticData,aerr,perr);    
        
    
        pin = np.linalg.pinv(np.add(DA,beta)); 
        dmg1 = np.dot(D,pin);
        dmg2 = np.dot(np.dot(D,A.T), err);    
        dmg = np.dot(dmg1,dmg2);
        
        isValid = True;
        while(isValid) :
            SCM = np.sqrt(np.divide(np.sum(np.power(dmg,2)),M));
            maxx = np.max(np.abs(dmg));
            if SCM > 1 or maxx > 3 :
                dmg = dmg * 0.9;
            else :
                isValid = False; 
        
        dmg = np.power(10,np.dot(pf,dmg));       
        
        ####UPDATE EARTH MODEL    
        dResistivities = dmg[0:currentIteration.earthModel.getNumberOfLayers()];
        dThicknesses = dmg[currentIteration.earthModel.getNumberOfLayers() : currentIteration.earthModel.getNumberOfLayers()*2-1];
        dResistivities = (dResistivities-1)*20.00+1; #DAMPING FACTOR PROBABLY SREWED UP SOMEWHERE, the *10 is a hackjob
        dThicknesses = (dThicknesses-1)*20.00+1;
        newResistivities = np.multiply(currentIteration.earthModel.resistivities,dResistivities);
        newThicknesses = np.multiply(currentIteration.earthModel.thicknesses,dThicknesses);
        
        #APPLY EARTH MODEL CONSTRAINTS
        
        cr = currentIteration.earthModel.resistivities;    
        newResistivities = ((armax*(cr - armin)*dResistivities+armin*(armax-cr))/((cr-armin)*dResistivities+(armax-armin)))
     
        #dmg=D*np.linalg.pinv(DA+bet*eye(M))*D*A'*dd0;
        #a = np.multiply(armax,np.subtract(currentIteration.earthModel.resistivities,armin));
        #b = np.multiply(a,dResistivities);
        #c = np.multiply(armin,np.subtract(armax,currentIteration.earthModel.resistivities))
        #d = np.sum(b,c);
        
        
        newEarth = earth.MTEarth(newResistivities,newThicknesses);
        iterationNumber = currentIteration.iterationNumber + 1;
        syntheticData = mt.mt1dfwd(newEarth,currentIteration.fieldData.frequencies);
        solvedIt = mtit.Iteration(iterationNumber, newEarth, syntheticData,currentIteration.fieldData)
        #rm2=(rmax.*(rm1-rmin).*dxr+rmin.*(rmax-rm1))./((rm1-rmin).*dxr+(rmax-rmin));
        #dm2=dm1.*dxd;
    
        #print(timeErr)
        return solvedIt
        
    
    def computeJacobian(self,iteration):        
        start = mtglobal.current_micro_time();
        nLayers = iteration.earthModel.getNumberOfLayers();  
        nFreqs = iteration.syntheticData.getNumberOfFrequencies();
           
        b1 = np.zeros((nFreqs,nLayers));    
        b2 = np.zeros((nFreqs,nLayers)); 
        b3 = np.zeros((nFreqs,nLayers-1)); 
        b4 = np.zeros((nFreqs,nLayers-1));
    
        
        
        tmpResArr = np.array(iteration.earthModel.resistivities);
        tmpThickArr = np.array(iteration.earthModel.thicknesses);
        tempEarth = earth.MTEarth(tmpResArr,tmpThickArr);
        
        
        
        for li in range(0,nLayers) :
            pf = self.pf;
            oldRes = tempEarth.resistivities[li];  
            newRes = copy.copy(oldRes * math.pow(10,pf));
            tempEarth.resistivities[li] = newRes;
            peturbed = mt.mt1dfwd(tempEarth,iteration.syntheticData.frequencies);
            arf = peturbed.apparentResistivities;
            ar = iteration.syntheticData.apparentResistivities;
            phf = peturbed.phases;
            ph = iteration.syntheticData.phases;
            arerr = iteration.fieldData.getApparentResistivityRelativePercentageError();     
            pherr = iteration.fieldData.getPhaseRelativePercentageError();
            #MATLAB b1(:,i) = log10(arf./ar)./ey1/fac;
            #MATLAB b2(:,i) = (phf - ph)./ey2/fac;
            b1[:,li] = np.divide(np.log10(np.divide(arf,ar)),arerr)/pf;
            b2[:,li] = np.divide(np.subtract(phf,ph),pherr)/pf;
            #print(iteration.fieldData.getApparentResistivityRelativePercentageError())
            #OLD b1[:,li] = np.divide(np.divide(np.log10(np.divide(peturbed.apparentResistivities,iteration.syntheticData.apparentResistivities)),iteration.fieldData.getApparentResistivityRelativePercentageError()),pf);
            #OLD b2[:,li] = np.divide(np.divide(np.subtract(peturbed.apparentResistivities,iteration.syntheticData.phases),iteration.fieldData.getPhaseRelativePercentageError()),pf);
            
            tempEarth.resistivities[li] = oldRes;
    
            
        for li in range(0, nLayers-1) :
            oldThick = iteration.earthModel.thicknesses[li];
            newThick = oldThick*math.pow(10,pf)
            tempEarth.resistivities[0] = tempEarth.resistivities[0] + 0.01; 
            tempEarth.thicknesses[li] = newThick;        
            peturbed = mt.mt1dfwd(tempEarth,iteration.syntheticData.frequencies);
            arf = peturbed.apparentResistivities;
            ar = iteration.syntheticData.apparentResistivities;
            phf = peturbed.phases;
            ph = iteration.syntheticData.phases;
            arerr = iteration.fieldData.getApparentResistivityRelativePercentageError();     
            pherr = iteration.fieldData.getPhaseRelativePercentageError();
            #MATLAB b1(:,i) = log10(arf./ar)./ey1/fac;
            #MATLAB b2(:,i) = (phf - ph)./ey2/fac;
            b3[:,li] = np.divide(np.log10(np.divide(arf,ar)),arerr)/pf;
            b4[:,li] = np.divide(np.subtract(phf,ph),pherr)/pf;
            
            #b3[:,li] = np.divide(np.divide(np.log10(np.divide(peturbed.apparentResistivities,iteration.syntheticData.apparentResistivities)),iteration.fieldData.getApparentResistivityRelativePercentageError()),pf);
            #b4[:,li] = np.divide(np.divide(np.subtract(peturbed.apparentResistivities,iteration.syntheticData.phases),iteration.fieldData.getPhaseRelativePercentageError()),pf);
            tempEarth.thicknesses[li] = oldThick;
            tempEarth.resistivities[0] = tempEarth.resistivities[0] - 0.01;
    
        
        a1 = np.hstack(([b1.T],[b3.T]))[0].T
        a2 = np.hstack(([b2.T],[b4.T]))[0].T    
        A = np.vstack(([a1, a2])); 
        timeJA = mtglobal.current_micro_time() - start;
        if(mtglobal.verbosity >= 3) : print("......Jacobian Time taken = " + str(timeJA/1000000) + "us")
        return A;
    
    def exportAllResultsASCII(self, dir, datestamp):
         filenameSVG = dir + datestamp + "_all.txt";
         #for it in iterations:
             #it.earth
             #it.data
             #export it 
         
    def exportAllResults(self, dir,datestamp, dpires):
        filenameSVG = dir + datestamp + "_inv_dat.svg";
        filenamePNG = dir + datestamp + "_inv_dat.png";
        
        filenameEarthSVG = dir + datestamp + "_inv_earth.svg";
        filenameEarthPNG = dir + datestamp + "_inv_earth.png";
        
        #filename = dir + datestamp + "_ear.png";        
        f, axarr  = plt.subplots(2, sharex=True);
        fearth, axarrearth  = plt.subplots(1, sharex=True);
        nth = 0.0;
        iterations = self.iterations;
        it = iterations[0]
        for it in iterations: 
            #plot all of our data
            nth = nth+1;
            ap = nth/float(len(iterations));
            axarr[0].set_title('Apparent Resistivity versus Frequency');
            axarr[0].set_ylabel('Apparent Resistivity (Ohm.m)');
            axarr[1].set_title('Phase (Deg) versus Frequency (Hz)');
            axarr[1].set_ylabel('Phase (Deg)');
            axarr[1].set_xlabel('Frequency (Hz)');
            #Apparent Resistivities
            axarr[0].loglog(it.syntheticData.frequencies, it.syntheticData.apparentResistivities, color='b', alpha=ap);
            #axarr[0].errorbar(it.syntheticData.frequencies, it.syntheticData.apparentResistivities,self.apparentResistivitiesError, color='b');
            axarr[0].scatter(it.syntheticData.frequencies, it.syntheticData.apparentResistivities,s=3, color='b', alpha=ap);
            #Phases
            axarr[1].semilogx(it.syntheticData.frequencies, it.syntheticData.phases, color='b', alpha=ap);
            #axarr[1].errorbar(it.syntheticData.frequencies, it.syntheticData.phases,self.phasesError, color='b');        
            axarr[1].scatter(it.syntheticData.frequencies, it.syntheticData.phases,s=3, color='b', alpha=ap); 
            #thicknesses
           # axarr[2].plot(it.syntheticData.apparentResistivities, it.syntheticData.currentDepth, color='b', alpha=ap); 
                                      
            
            axarr[0].grid(True,which="both");
            axarr[1].grid(True,which="both");
            axarrearth.grid(True,which="both");
            
            
            #Plot our earth
            axarrearth.set_title('Apparent Resistivity versus Depth');
            axarrearth.set_xlabel('Apparent Resistivity (Ohm.m)');
            axarrearth.set_ylabel('Depth (M)');
            #axarrearth.set_autoscaley_on(True);
            axarrearth.set_ylim([-2000,0]);         
            #START PLOTTING EARTH
            layerResistivites = it.earthModel.resistivities
            layerDepths = it.earthModel.getDepthsAndHalfspace(np.mean(it.earthModel.thicknesses));
            finalResistivites = np.array([]);
            finalDepths = np.array([]);
            #start at the top
            #each layer requires two points
            depth = 0;
            for i in range(0,len(layerResistivites)-1) :
                finalResistivites = np.append(finalResistivites, layerResistivites[i]);
                finalResistivites = np.append(finalResistivites, layerResistivites[i]);
                finalDepths = np.append(finalDepths,depth);
                depth = layerDepths[i]; 
                finalDepths = np.append(finalDepths,depth);
                
            axarrearth.semilogx(finalResistivites,finalDepths, color='b', alpha=ap/2);
            #FINISH PLOTTING EARTH
        
        axarr[0].set_title('Apparent Resistivity versus Frequency');
        axarr[1].set_title('Phase versus Frequency');
        axarrearth.set_title('Apparent Resistivity versus Depth');
        
        
        #Apparent Resistivities
        axarr[0].loglog(it.fieldData.frequencies, it.fieldData.apparentResistivities, color='g');
        axarr[0].errorbar(it.fieldData.frequencies, it.fieldData.apparentResistivities,it.fieldData.apparentResistivitiesError, color='g');
        axarr[0].scatter(it.fieldData.frequencies, it.fieldData.apparentResistivities,s=3, color='g');
        #Phases
        axarr[1].semilogx(it.fieldData.frequencies, it.fieldData.phases, color='g');
        axarr[1].errorbar(it.fieldData.frequencies, it.fieldData.phases,it.fieldData.phasesError, color='g');        
        axarr[1].scatter(it.fieldData.frequencies, it.fieldData.phases,s=3, color='g');                              
        
        #START PLOTTING EARTH
        layerResistivites = it.earthModel.resistivities
        layerDepths = it.earthModel.getDepthsAndHalfspace(np.mean(it.earthModel.thicknesses));
        finalResistivites = np.array([]);
        finalDepths = np.array([]);
        #start at the top
        #each layer requires two points
        depth = 0;
        for i in range(0,len(layerResistivites)-1) :
            finalResistivites = np.append(finalResistivites, layerResistivites[i]);
            finalResistivites = np.append(finalResistivites, layerResistivites[i]);
            finalDepths = np.append(finalDepths,depth);
            depth = layerDepths[i]; 
            finalDepths = np.append(finalDepths,depth);
        axarrearth.semilogx(finalResistivites,finalDepths, color='g', alpha=ap, linewidth = 2.0);
        #FINISH PLOTTING EARTH    
        
        axarr[0].grid(True,which="both");
        axarr[1].grid(True,which="both"); 
        axarrearth.grid(True,which="both"); 

    
        
        f.savefig(filenameSVG, dpi=dpires);
        f.savefig(filenamePNG, dpi=dpires);
        fearth.savefig(filenameEarthSVG, dpi=dpires);
        fearth.savefig(filenameEarthPNG, dpi=dpires);
        if(mtglobal.verbosity >= 2) : print("Exported SVG to " + filenameSVG);
        if(mtglobal.verbosity >= 2) : print("Exported PNG to " + filenamePNG);
        if(mtglobal.verbosity >= 2) : print("Exported SVG to " + filenameEarthSVG);
        if(mtglobal.verbosity >= 2) : print("Exported PNG to " + filenameEarthPNG);            
