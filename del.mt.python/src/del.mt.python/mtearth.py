import numpy as np



class MTEarth:   #Contains all the information needed to calculate and plot an earth model# 
    #Variables
    resistivities = np.array([]);
    thicknesses = np.array([]);
    nlayers = -1;


    def __init__(self, resistivities, thicknesses): #Passes resistivities and thicknesses  from the array to be used in calculation#
        self.resistivities = resistivities;
        self.thicknesses = thicknesses;

    def getDepthsAndHalfspace(self, halfspaceDistance):
        depthsNoHalfspace = self.getDepths();
        return np.append(depthsNoHalfspace,depthsNoHalfspace[len(depthsNoHalfspace)-1] - halfspaceDistance); #returns depth to final layer without halfspace distance#
        
        
    def getDepths(self) : #gets individual depths of each layer#
        depths = np.array([]);    
        currentDepth = 0
        for i in range(0, self.getNumberOfLayers()-1) :
            currentDepth = currentDepth - self.thicknesses[i];
            depths = np.append(depths,currentDepth);
            
        return depths;
            
    def getNumberOfLayers(self) : #gets the number of layers present in the earth model#
        if self.nlayers == -1 :
            self.nlayers = len(self.resistivities);
        return self.nlayers;
        
    def printEarth(self) : #prints the earth using current depths, number of layers and the halfspace distance#
        for i in range(0,self.getNumberOfLayers()) :
            currentDepth = 0;            
            if(i != (self.getNumberOfLayers() - 1)) :
                print("%f : %f Ohm.m (%f m)" % (currentDepth,self.resistivities[i],self.thicknesses[i]));
                currentDepth = currentDepth + self.thicknesses[i];
            else :                
                print("%f : %f Ohm.m (halfspace)" % (currentDepth,self.resistivities[i])); 
    def plotEarth(self) : #Plots the earth model so that we can view on screen#
        f, axarr  = plt.subplots(3, sharex=True);
        axarr[0].set_title('Apparent Resistivity (Ohm.m) versus Frequency (Hz)');
        axarr[1].set_title('Phase (Deg) versus Frequency (Hz)');
        axarr[2].set_title('Apparent Resistivity (Ohm.m) versus Depth (M)');
        #Apparent Resistivities
        axarr[0].loglog(it.fieldData.frequencies, it.fieldData.apparentResistivities, color='g');
        axarr[0].errorbar(it.fieldData.frequencies, it.fieldData.apparentResistivities,it.fieldData.apparentResistivitiesError, color='g');
        axarr[0].scatter(it.fieldData.frequencies, it.fieldData.apparentResistivities,s=3, color='g');
        #Phases
        axarr[1].semilogx(it.fieldData.frequencies, it.fieldData.phases, color='g');
        axarr[1].errorbar(it.fieldData.frequencies, it.fieldData.phases,it.fieldData.phasesError, color='g');        
        axarr[1].scatter(it.fieldData.frequencies, it.fieldData.phases,s=3, color='g');
        #Thicknesses
        axarr[2].plot(it.fieldData.apparentResistivities, it.fieldData.currentDepth, color='g');
                                    
        axarr[0].grid(True,which="both");
        axarr[1].grid(True,which="both");
        axarr[2].grid(True,which="both");
        def exportSVG(self, filenameEarthSVG,dpires) :  #data exported as a SVG file#
         print("Exported "  + filenameEarthSVG);
                  
    def exportPNG(self, filenameEarthPNG,dpires) :      #data exported as a PNG file#
         print("Exported "   +  filenameEarthPNG);
         
            
def load(filename): #reads in the file of the earth model - contains resistivities  and thicknesses and returns the information to earth#
        f = open(filename, 'r');
        earth = MTEarth();
        for line in f.readlines():
            lineSplit = line.strip().split();
            res = float(lineSplit[0]);            
            earth.resistivities.append(res);
            if len(lineSplit) > 1 :
                thick = float(lineSplit[1]);            
                earth.thicknesses.append(thick);        
        f.close()
        return earth;
        
