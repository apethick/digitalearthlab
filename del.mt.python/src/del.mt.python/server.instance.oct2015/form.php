<?php

?>
	<h2> Geo-Electrical Model</h2>
	
	 	<div id="respond">
		  <?php echo $response; ?>
		  <form>
		    
		    <table>
		    <tr>
		    <td>
		    <p><label for="message_data">Data: <span>*</span> <br><textarea type="text" name="message_data" value="[1,100]">[1,100]</textarea></label></p>		   
		    <p><label for="message_resistivities">Resistivities: <span>*</span> <br><textarea type="text" name="message_resistivities" value="[1,10,100]">[1,10,100]</textarea></label></p>
		    <p><label for="message_thicknesses">Thicknesses: <span>*</span> <br><textarea type="text" name="message_thicknesses" value="[1,100]">[1,100]</textarea></label></p>		   
		    <p><input type="submit" name="forward" value="Forward Model"></p>
		    </td>
		    <td>		    		    		    		   
		    <input type="hidden" name="submitted" value="1">
		    </td>
			</tr>
			</table><br>	
			<table>
		    <tr>
		    <td>
		    <p><img id="Case1" src="Case_1.png" alt="Case 1" width="165"  height="110"></p>
		    <p><input type="submit" name="forward" value="Select Case 1"></p>	
		    </td>
		    <td>		    		   	    
		    <p><img id="Case2" src="Case_2.png" alt="Case 2" width="165"  height="110"></p>
		    <p><input type="submit" name="forward" value="Select Case 2"></p>
		    </td>
			</tr>
			</table>		    
		  </form>		  
		</div>
	
	 	  
<?php
	
?>