	<?php
	/**
	 * Plugin Name: Digital Earth Lab 1D MT Forward Modelling
	 * Plugin URI: http://digitalearthlab.com
	 * Description: 1D Magnetotelluric Forward Modelling
	 * Version: 0.0
	 * Author: Andrew Pethick
	 * Author URI: http://digitalearthlab.com
	 * License: WTFPL
	 */
	 
	echo 'hello you!';
	echo initialise('');

	
	    $response = "";
	   
	  function initialise( $attributes ){     
			ob_start();
			global $response;


	 	switch ($_REQUEST['forward']){
			case "Forward Model":
		
			break;
			case "Select Case 1":
			select_case('1');
			
			break;
			case "Select Case 2":
			select_case('2');
			break;
		}
	 	include("form.php");
	 	include("google_chart.php");
	
		run();	 
		
		$output_string=ob_get_contents();;
		ob_end_clean();
		return $output_string;	

	}
	
	function run() {
		$resistivities = filter_var($_POST['message_resistivities']);
		$thicknesses = filter_var($_POST['message_thicknesses']);
		$frequencies = '[0.0014,0.0017,0.0020,0.0023,0.0028,0.0034,0.0040,0.0046,0.0055,0.0067,0.0079,0.0092,0.0110,0.0134,0.0159,0.0183,0.0220,0.0269,0.0320,0.0370,0.0440,0.0540,0.0630,0.0730,0.0880,0.1070,0.1270,0.1460,0.1760,0.2150,0.2540,0.2930,0.3500,0.4300,0.5100,0.5900,0.7000,0.8600,1.0200,1.1700,1.4100,1.7200,2.0300,2.3400,2.8100,3.4000,4.1000,4.7000,5.6000,6.9000,8.1000,9.4000,11.2000,13.7000,16.2000,18.8000,22.5000,27.5000,33.0000,40.0000,49.0000,57.0000,66.0000,79.0000,97.0000,115.0000,132.0000,159.0000,194.0000,229.0000,265.0000,320.0000,390.0000,460.0000,530.0000,640.0000,780.0000,900.0000,1100.0000,1300.0000,1500.0000,1800.0000,2200.0000,2600.0000,3000.0000,3600.0000,4400.0000,5200.0000,6000.0000,7200.0000,8800.0000,10400.0000]';

	  	$string = $frequencies.' '.$resistivities.' '.$thicknesses;

	  	
	  	fwd($string);		  
	}
	
	  
 	function select_case($input) {
		switch ($input){		
			case "1":
		     $_POST['message_resistivities'] = '[1,10,100]';
			 $_POST['message_thicknesses'] = '[100,500]';

			break;
			case "2":
		     $_POST['message_resistivities'] = '[1000,10,1000]';
			 $_POST['message_thicknesses'] = '[50,100]';

			break;
			case "3":
		     $_POST['message_resistivities'] = '[1.5,30,1.5]';
			 $_POST['message_thicknesses'] = '[1000,50]';

			break;			
		  }

	}

	
	function flip($arr){
	    $out = array();
	
	    foreach ($arr as $key => $subarr)
	    {
	            foreach ($subarr as $subkey => $subvalue)
	            {
	                 $out[$subkey][$key] = $subvalue;
	            }
	    }
	
	    return $out;
	}
	
	function fwd($string) {			
	     global  $mt_output;
	     $file = 'mtrun.py';
	     $output = shell_exec('python ' . __DIR__ . '/' . $file . ' ' . $string); 
	     echo '<img src="'.$output.'"/>';
		
		 

 	}
