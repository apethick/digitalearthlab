	<?php
	/**
	 * Plugin Name: Digital Earth Lab 1D MT Inversion
	 * Plugin URI: http://digitalearthlab.com
	 * Description: 1D Magnetotelluric Inversion Software
	 * Version: 0.0
	 * Author: Stefan Spears & Andrew Pethick
	 * Author URI: http://digitalearthlab.com
	 * License: WTFPL
	 */

	$GLOBALS['resistivities'] = '10.0 16.0 13.0 12.0 1.0 11.0 14.0 1.60 41.0 199.0';
	$GLOBALS['thicknesses'] = '10.0 15.0 30.0 50.0 70.0 100.0 150.0 160.0 190.0';
	$GLOBALS['data'] = '0.00020128	9.6283	-0.41352	43.261	-1.858
0.00051152	9.4989	-0.56779	42.641	-2.5488
0.0012999	10.056	-0.050151	45.077	-0.2248
0.0033036	10.78	0.61016	48.207	2.7285
0.0083956	10.186	-0.087326	45.37	-0.38898
0.021336	11.033	0.59422	48.828	2.6299
0.054222	10.573	-0.13437	46.292	-0.58829
0.1378	10.365	-0.78478	44.551	-3.3731
0.35019	12.342	0.45393	51.371	1.8893
0.88995	12.724	-0.42558	50.041	-1.6738
2.2617	12.981	-2.3852	46.218	-8.492
5.7477	19.439	0.034772	58.407	0.10448
14.607	28.104	1.086	64.271	2.4835
37.121	44.606	3.0105	68.169	4.6007
94.337	62.832	-4.9048	56.394	-4.4022
239.74	94.923	-4.1112	47.815	-2.0709
609.27	91.696	-2.2299	31.604	-0.76855
1548.4	57.048	3.7874	20.737	1.3767
3934.9	23.825	-1.0939	16.035	-0.73625
10000	12.293	-0.45195	21.92	-0.80587';
	echo initialise('');

	
	    $response = "";
	   
	  function initialise( $attributes ){     
			ob_start();
					

			global $response;
	
	

			include("form.php");

	
			run();	 
		
			$output_string=ob_get_contents();;
			ob_end_clean();
			return $output_string;	

	}
	
	function run() {
		$resistivities = ($_GET['message_resistivities']);
		$thicknesses = filter_var($_GET['message_thicknesses']);
		$data = filter_var($_GET['message_data']);
		$GLOBALS['resistivities'] = $resistivities;
		$GLOBALS['thicknesses'] = $thicknesses;
		$GLOBALS['data'] = $data;
	  	$string = $data.'_'.$resistivities.'_'.$thicknesses;
		
	  	
	  	fwd($string);		  
	}
	
	  
 	function select_case($input) {
		switch ($input){		
			case "1":
		     $_POST['message_resistivities'] = '1 10 100';
			 $_POST['message_thicknesses'] = '100 500';

			break;
			case "2":
		     $_POST['message_resistivities'] = '1000 10 1000';
			 $_POST['message_thicknesses'] = '50 100';

			break;
			case "3":
		     $_POST['message_resistivities'] = '1.5 30 1.5';
			 $_POST['message_thicknesses'] = '1000 50';

			break;			
		  }

	}

	
	function flip($arr){
	    $out = array();
	
	    foreach ($arr as $key => $subarr)
	    {
	            foreach ($subarr as $subkey => $subvalue)
	            {
	                 $out[$subkey][$key] = $subvalue;
	            }
	    }
	
	    return $out;
	}
	
	function fwd($string) {		
		 $string = str_replace('\t',',',$string);
		 $string = str_replace('\n',',',$string);
		 $string = str_replace('\r',',',$string);
		 $string = str_replace(' ',',',$string);
		 $string = preg_replace('#\s+#',',',trim($string));
			

	     global  $mt_output;
	     $file = 'mtrun.py';
	     $output = shell_exec('python ' . __DIR__ . '/' . $file . ' ' . $string);
	     $images = explode('./', $output);
	     $image1 = './' . $images[1];
	     $image2 = './' . $images[2];
	     #echo "$image1", '\r\n';
	     #echo "$image2", '\r\n';
	     echo '<div id="content"><section>';
	     echo '<img src="'.$image1.'"/>';
	     echo '<img src="'.$image2.'"/>';
	     echo '</div> </section>';
	     ##echo $images[0];
	     
	   	 
	     
	    
	     
		
		 

 	}
