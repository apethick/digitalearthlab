import math
import cmath
import time
import mtdat as dat
import numpy as np
import time
import mtglobal
#====================================#
#1D MAGNETOTELLURIC MODELLING PROGRAM#
#====================================#
#    LAST UPDATED 17TH DECEMBER 2013 #
#    DEVELOPED BY ANDREW PETHICK     #
#      WWW.DIGITIALEARTHLAB.COM      #
#====================================#
#licensed under WTFP                 #
#====================================#

start = time.clock();
ntimes = 0;
mu = 4*math.pi*1E-7; #Magnetic Permeability (H/m)

def mt1dfwd(earth, frequencies):
    start = mtglobal.current_micro_time();

    global ntimes
    fwd_dat = dat.MTData();
    ntimes = ntimes+1;
    for f in frequencies :
        a = mt1DFWDFreq(earth.resistivities, earth.thicknesses, f);
        fwd_dat.frequencies = np.append(fwd_dat.frequencies,f);
        fwd_dat.apparentResistivities = np.append(fwd_dat.apparentResistivities,a[1]);
        fwd_dat.apparentResistivitiesError = np.append(fwd_dat.apparentResistivitiesError,0.0);
        fwd_dat.phases = np.append(fwd_dat.phases,a[2]);
        fwd_dat.phasesError = np.append(fwd_dat.phasesError,0.0);
            
    end = mtglobal.current_micro_time() - start;
    if(mtglobal.verbosity >= 3) : print(".........1D MT FWD Time taken = " + str(float(end)/1E6) + "s") 
    return fwd_dat;


def mt1dfwdalt(earth, frequencies):
    start = mtglobal.current_micro_time();

    global ntimes
    fwd_dat = dat.MTData();
    ntimes = ntimes+1;
    for f in frequencies :
        a = mt1DFWDFreqAlt(earth.resistivities, earth.thicknesses, f);
        fwd_dat.frequencies = np.append(fwd_dat.frequencies,f);
        fwd_dat.apparentResistivities = np.append(fwd_dat.apparentResistivities,a[1]);
        fwd_dat.apparentResistivitiesError = np.append(fwd_dat.apparentResistivitiesError,0.0);
        fwd_dat.phases = np.append(fwd_dat.phases,a[2]);
        fwd_dat.phasesError = np.append(fwd_dat.phasesError,0.0);
            
    end = mtglobal.current_micro_time() - start;
    if(mtglobal.verbosity >= 3) : print(".........1D MT FWD Time taken = " + str(float(end)/1E6) + "s") 
    return fwd_dat;

def mt1DFWDFreqAlt(resistivities, thicknesses, frequency):
    n = len(resistivities);
    w =  2*math.pi*frequency;  
    zn = list(range(n));
    #COMPUTE BASEMENT IMPEDANCE
    
    sig = 1.0/float(resistivities[n-1]);
    a = np.sqrt(complex(0,w * mu * sig));
   
    zn[n-1] = -a/sig;
    for k in range(n-2,-1,-1):
        sig = 1.0/float(resistivities[k])
        thick = thicknesses[k];
        a = np.sqrt(complex(0,w * mu * sig));
  
        p1 = a/sig;
        p2 = -np.tanh(a*thick) + (zn[k+1]/a*sig);
        p3 = (-(zn[k+1]*(np.tanh(a*(thick)))/a*sig))+(1.0);
        zn[k] = p1*(p2)/(p3);
    Z = zn[0];
    absZ = abs(Z);
    apparentResistivity = (absZ * absZ)/(mu * w);
    phase = math.degrees(math.atan2(Z.imag, Z.real));
    return [Z, apparentResistivity, phase];    

def mt1DFWDFreq(resistivities, thicknesses, frequency):
    n = len(resistivities);
    w =  2*math.pi*frequency;       
    impedances = list(range(n));
    #compute basement impedance
    impedances[n-1] = cmath.sqrt(w*mu*resistivities[n-1]*1j);
    for j in range(n-2,-1,-1):
        resistivity = resistivities[j];
        thickness = thicknesses[j];
        # 3. Compute apparent resistivity from top layer impedance
        #Step 2. Iterate from bottom layer to top(not the basement) 
        # Step 2.1 Calculate the intrinsic impedance of current layer
        dj = cmath.sqrt((w * mu * (1.0/resistivity))*1j);
        wj = dj * resistivity;
        # Step 2.2 Calculate Exponential factor from intrinsic impedance
        ej = cmath.exp(-2*thickness*dj);                     
        # Step 2.3 Calculate reflection coeficient using current layer
        #          intrinsic impedance and the below layer impedance
        belowImpedance = impedances[j + 1];
        rj = (wj - belowImpedance)/(wj + belowImpedance);
        re = rj*ej; 
        Zj = wj * ((1 - re)/(1 + re));
        impedances[j] = Zj;    
    # Step 3. Compute apparent resistivity from top layer impedance
    Z = impedances[0];
    absZ = abs(Z);
    apparentResistivity = (absZ * absZ)/(mu * w);
    phase = math.degrees(math.atan2(Z.imag, Z.real));
    return [Z, apparentResistivity, phase];