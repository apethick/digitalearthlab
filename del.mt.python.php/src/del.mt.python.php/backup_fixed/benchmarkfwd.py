import math
import cmath
import time
import mt1dfwd as mt
import mtdat as dat
import mtearth as earth;
import mtinvert as inv
import datetime
import time
import numpy as np
import random
import matplotlib.pyplot as plt
print('Starting program');

dir = '/Users/spearzo/Documents/Thesis/Temp_files/';
datestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d%H%M%S')

##### INITIALISE EARTH MODEL##################

#resistivities = np.array([1.0, 1.0]);
#thicknesses = np.array([10.0]);
#startingEarth = earth.MTEarth(resistivities,thicknesses);

##### LOAD IN DATA ###########################
filename = 'example.data';
fieldData = dat.load(filename);
#fieldData.plotData(dir + "Field_"  + datestamp + ".png");
#fieldData.printData();
##### FORWARD MODEL TO GET STARTING RESULT####

#syntheticData = mt.fwd(startingEarth, fieldData.frequencies);

current_micro_time = lambda: int(round(time.time() * 1000000));

###### TEST FOR NUMBER OF LAYERS #############
print('Running N Layer Benchmark Test')
nlayers = 6
niterations = 10000
la = []; #array that has the the number of iterations
ta = []; #array that has the time to forward model & time to create the earth model
for i in range(1000, 1000+1):
    time_create_start = current_micro_time();
    earthCreation = 0;
    for j in range(1,i) :       
        resistivities = range(1,nlayers+1);
        thicknesses = range(1,nlayers);
        #for j in range(1, i+1):
        #    resis
        #    resistivities.append(random.randrange(1,1000,1));
        #    if j != (i) : thicknesses.append(random.randrange(1,1000,1));
        #print len(resistivities), len(thicknesses)        
        startingEarth = earth.MTEarth(resistivities,thicknesses);              
        mt.mt1dfwd(startingEarth,[1,2,3,4,5,6,7,8,9,10]); #[1] is your frequency array 
        
    
    time_fwd = current_micro_time() - time_create_start;
    print('niterations = ' + str(i) + ' = ' + str(time_fwd) + "us");
    
for i in range(1000, 1000+1):
    time_create_start = current_micro_time();
    earthCreation = 0;
    for j in range(1,i) :       
        resistivities = range(1,nlayers+1);
        thicknesses = range(1,nlayers);
        #for j in range(1, i+1):
        #    resis
        #    resistivities.append(random.randrange(1,1000,1));
        #    if j != (i) : thicknesses.append(random.randrange(1,1000,1));
        #print len(resistivities), len(thicknesses)        
        startingEarth = earth.MTEarth(resistivities,thicknesses);              
        mt.mt1dfwdalt(startingEarth,[1,2,3,4,5,6,7,8,9,10]); #[1] is your frequency array 
        
    
    time_fwd = current_micro_time() - time_create_start;
    print('niterationsalt = ' + str(i) + ' = ' + str(time_fwd) + "us");

    la.append(i);
    ta.append(time_fwd);
    #print len(resistivities), time_create, time_fwd;
##### OUTPUT DATA ############################
#syntheticData.printData();
#syntheticData.plotData(dir + "Forward_"  + datestamp + ".png");

###### BENCHMARK TEST FOR NUMBER OF FREQUECNIES ##########
#print('Running N Frequency Benchmark Test');
#for i in range(1, nfreqs+1) :
    #do your loop here for frequencies
fig = plt.figure();
plt.plot(la,ta);
fig.suptitle('Number of Layers vs Time')
plt.xlabel('Number of Layers')
plt.ylabel('Time to create layers (Micro-seconds)')
plt.show(fig);

