<?php

?>
	
	<section class="row">
	 	<div id="respond">
		  <?php echo $response; ?>
		  <form>
		  
	
		  
		  <p>
		  <hr>
		  <p>
		  <p>
		  <p><div style='text-align:left' id="content">
		  	
		  <p style="color: red">Note on Usage:<br> 
		  <li type="disc">Input data in the text box labelled "Data" as is specified in the example dataset<br>
		  <li type="disc">Input a reasonable starting model into the text boxes labelled "Resistivities" & "Thicknesses"<br>
		  <li type="disc">PS:Ensure the number of Thicknesses entered = (Number of Resistivities - 1)<br> 
		  <li type="disc">When finished entering all the required information click on "INVERT" ---- Models will be displayed below<br>
		  
		  <p>
		  <div style='text-align:center'>
		  <hr>
		  
		    <p><label for="message_data"><div style='text-align:left'><p style="color: red">Data:<br><textarea rows="25" type="text" name="message_data" style="width:100%;"><?php print $GLOBALS['data'];?></textarea></label></p>	
		    <p><table border="1">
		    <tr><b>EXAMPLE DATASET<b></tr>
		    
<tr><td><b>Freq(Hz)</b></td><td><b>ARes(Ohm.m)</b></td><td><b>AResErr(Ohm.m)</b></td><td><b>Phase(Deg)</b></td><td><b>PhaseErr(Deg)</b></td></tr></p>
<p><tr><td>0.00020128</td><td>9.6283</td><td>-0.41352</td><td>43.261</td><td>-1.858</td></tr>
<tr><td>0.00051152</td><td>9.4989</td><td>-0.56779</td><td>42.641</td><td>-2.5488</td></tr>
<tr><td>0.0012999</td><td>10.056</td><td>-0.050151</td><td>45.077</td><td>-0.2248</td></tr>
<tr><td>0.0033036</td><td>10.78</td><td>0.61016</td><td>48.207</td><td>2.7285</td></tr>
<tr><td>0.0083956</td><td>10.186</td><td>-0.087326</td><td>45.37</td><td>-0.38898</td></tr>
<tr><td>0.021336</td><td>11.033</td><td>0.59422</td><td>48.828</td><td>2.6299</td></tr>
<tr><td>0.054222</td><td>10.573</td><td>-0.13437</td><td>46.292</td><td>-0.58829</td></tr>
<tr><td>0.1378</td><td>10.365</td><td>-0.78478</td><td>44.551</td><td>-3.3731</td></tr>
<tr><td>0.35019</td><td>12.342</td><td>0.45393</td><td>51.371</td><td>1.8893</td></tr>
<tr><td>0.88995</td><td>12.724</td><td>-0.42558</td><td>50.041</td><td>-1.6738</td></tr>
<tr><td>2.2617</td><td>12.981</td><td>-2.3852</td><td>46.218</td><td>-8.492</td></tr>
<tr><td>5.7477</td><td>19.439</td><td>0.034772</td><td>58.407</td><td>0.10448</td></tr>
<tr><td>14.607</td><td>28.104</td><td>1.086</td><td>64.271</td><td>2.4835</td></tr>
<tr><td>37.121</td><td>44.606</td><td>3.0105</td><td>68.169</td><td>4.6007</td></tr>
<tr><td>94.337</td><td>62.832</td><td>-4.9048</td><td>56.394</td><td>-4.4022</td></tr>
<tr><td>239.74</td><td>94.923</td><td>-4.1112</td><td>47.815</td><td>-2.0709</td></tr>
<tr><td>609.27</td><td>91.696</td><td>-2.2299</td><td>31.604</td><td>-0.76855</td></tr>
<tr><td>1548.4</td><td>57.048</td><td>3.7874</td><td>20.737</td><td>1.3767</td></tr>
<tr><td>3934.9</td><td>23.825</td><td>-1.0939</td><td>16.035</td><td>-0.73625</td></tr>
<tr><td>10000</td><td>12.293</td><td>-0.45195</td><td>21.92</td><td>-0.80587</td></tr></table></p>  
<form>	
</section>
<section class="row">
<div id="content">
<h2> Geo-Electrical Model</h2>
            
            <p><p style="color: red"><label for="message_resistivities">Resistivities: <span>*</span> <br><textarea rows="5" columns="200" type="text" name="message_resistivities" style="width:100%;" ><?php print $GLOBALS['resistivities'];?></textarea></label></p>
		    <p><p style="color: red"><label for="message_thicknesses">Thicknesses: <span>*</span> <br><textarea rows="5" type="text" name="message_thicknesses" style="width:100%;"><?php print $GLOBALS['thicknesses'];?></textarea></label></p>		   
		        

		    <p><input type="submit" name="Invert" value="INVERT" style="width:100%; background-color: red; font-size: 2.0em"></p>
		    </td>
		    <td>		    		    		    		   
		    <input type="hidden" name="submitted" value="1">
		    </td>
			</tr>
			</table><br>	
			<table>
		    <tr>
		    <td>
		    <!-- <tr><p><img id="Case1" src="Case_1.png" alt="Case 1" width="165"  height="110"></p>
		    <p><input type="submit" name="forward" value="Select Case 1"></p>	
		    </td>
		    <td>		    		   	    
		    <p><img id="Case2" src="Case_2.png" alt="Case 2" width="165"  height="110"></p>
		    <p><input type="submit" name="forward" value="Select Case 2"></p>-->
		    </td>
			</tr>
			</table>		    
		  </form>		  
		</div>
	</div>
	 </section>	  
<?php
	
?>