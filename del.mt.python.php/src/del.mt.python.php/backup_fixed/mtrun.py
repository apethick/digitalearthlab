import matplotlib.pyplot as plt
import math
import cmath
import time
import mt1dfwd as mt
import mtdat as dat
import mtearth as earth;
import mtinvert as inv
import datetime
import time
import numpy as np
import mtglobal as mtglobal
import mtinvparameters as mtinvparam
import sys

mtglobal.verbosity = 0;
if(mtglobal.verbosity >= 1) :
    print('====================================='); 
    print('        1DMT Inversion v1.0          ');
    print('           Developed by              ');
    print('          Andrew Pethick             ');
    print('                and                  ');
    print('          Stefan Spears              ');
    print('             July 2015               ');        
    print('=====================================');
    print('');
    print('Starting Program');

###########SET GLOBAL PARAMETERS##############



#mtglobal.dir = 'c:/Temp/MT_Benchmark/';
mtglobal.dir = './tmp/';

###########INITIALISE DATESTAMP##############
datestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d%H%M%S')



##### INITIALISE EARTH MODEL##################
if (len(sys.argv) > 1):
    arguments = sys.argv[1];
    dataString, resistivitiesString, thicknessesString = arguments.split('_');
    fieldData = dat.read(dataString);
    resistivities = np.array([float(x) for x in resistivitiesString.split(',')]);
    thicknesses =  np.array([float(x) for x in thicknessesString.split(',')]);
    startingEarth = earth.MTEarth(resistivities,thicknesses);
else :
    resistivities = np.array([10.0, 16.0,13.0,12.0,1.0,11.0,14.0,1.60,41.0,101.0]);
    thicknesses = np.array([10.0, 15.0,30.0,50.0,70.0,100.0,150.0,160.0,190.0]);

    startingEarth = earth.MTEarth(resistivities,thicknesses);
	##### LOAD IN DATA ###########################
    filename = 'example.data';
    fieldData = dat.load(filename);
    #fieldData.plotData(dir + "Field_"  + datestamp + ".png");
    #fieldData.printData();



##### FORWARD MODEL TO GET STARTING RESULT####
syntheticData = mt.mt1dfwdalt(startingEarth, fieldData.frequencies);

##### OUTPUT DATA ############################
#syntheticData.printData();
#syntheticData.plotData(dir + "Forward_"  + datestamp + ".png");


    
inversion = inv.mtinversion(startingEarth, fieldData, mtinvparam);
inversion.invert()


#####################PLOT DATA########################
dpires = 150
inversion.exportAllResults(mtglobal.dir,datestamp,dpires)

if(mtglobal.verbosity >= 1) : print(str(mt.ntimes) + ' 1D forward models completed')    


print(str(mtglobal.dir) + str(datestamp) + '_inv_dat.png');
print(str(mtglobal.dir) + str(datestamp) + '_inv_earth.png');








