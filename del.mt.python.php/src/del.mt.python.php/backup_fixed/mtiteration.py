import mtdat as dat
import mtearth as earth


class Iteration:
    #This represents all the data needed to define
    #a single iteration of an 1D MT Inversion
    #This class is the only input for running the next iteration
    earthModel = None;     #This is the final earth model
    syntheticData = None;  #This is the final synthetic data post-inversion
    iterationNumber = 1;   #What iteration cycle does this represent (<maxIterations)
    fieldData = dat.MTData(); #A link to the field data
    
    
    def __init__(self, iterationNumber, earthModel, syntheticData, fieldData):
        self.earthModel = earthModel;
        self.syntheticData = syntheticData;
        self.iterationNumber = iterationNumber;
        self.fieldData = fieldData;
        
    