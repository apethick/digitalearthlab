<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="shortcut icon" href="img/favicon.ico"> 
	<link rel="stylesheet" href="css/vendor/fluidbox.min.css">
	<link rel="stylesheet" href="css/main.css">

	<title>MT1Dnversion</title>

</head>

<body>


	<header>

		<div id="logo-container">
			<div id="logo">MT 1<sup>D</sup>nversion</div>
			<div id="subtitle">Electromagnetic Data in the Cloud</div>
		</div>
<nav>
			<ul>

				<li><a href="mailto:stefan.spears@student.curtin.edu.au" class="bordered">Contact</a></li>
			</ul>
		</nav> 

	</header>

	<div id="content">

		<section class="row">
			<h2>MT Data to Model in Seconds</h2>
			<p>
			This program was developed by Stefan Spears (student) and Andrew Pethick (Supervisor) 
		  as part of a project undertaken to complete a Master's Degree. <br><br> It is our hope that it serves 
		  as a base for further development in the field of geophysical cloud computing. 
		  </p>
		</section>




		


 
	</div>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script>
	if (!window.jQuery) 
	{
	    document.write('<script src="js/vendor/jquery.1.11.min.js"><\/script>');
	}
	</script>

	<script src="js/vendor/jquery.fluidbox.min.js"></script>
	<script src="js/main.js"></script>

	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'YOUR_GOOGLE_ANALYTICS_ID', 'auto');
	  ga('send', 'pageview');

	</script>

</body>
</html>


<?PHP

require_once("wp-mt.php");
?>
