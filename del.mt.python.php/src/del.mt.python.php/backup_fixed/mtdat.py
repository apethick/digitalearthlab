import matplotlib.pyplot as plt
import numpy as np
import time
import datetime
import math
import numpy as np

class MTData:
    frequencies = np.array([]);
    apparentResistivities = np.array([]);
    phases = np.array([]);
    apparentResistivitiesError = np.array([]);
    phasesError = np.array([]);
    #initialise on first compute
    apparentResistivitiesErrorNorm = np.array([]);
    phasesErrorNorm = np.array([]);
    nfreq = -1;
    
    def __init__(self, frequencies, apparentResistivities, phases, apparentResistivitiesError, phasesError, currentDepth):
        self.frequencies = self.frequencies;
        self.apparentResistivities = apparentResistivities;
        self.apparentResistivitiesError = apparentResistivitiesError;
        self.phasesError = phasesError;
        self.phases = phases;
        self.nfreq = len(self.frequencies);
        self.currentDepth = currentDepth;
   
        
    def __init__(self):
        self.frequencies = np.array([]);
        self.apparentResistivities = np.array([]);
        self.apparentResistivitiesError = np.array([]);
        self.phasesError = phasesError = np.array([]);
        self.phases = np.array([]);        
        self.nfreq = len(self.frequencies);
        self.currentDepth = np.array([]);
     

    def getNumberOfFrequencies(self):
        return len(self.frequencies);
        
    
    def getApparentResistivityRelativePercentageError(self):
        if len(self.apparentResistivitiesErrorNorm) != 0 :
            return self.apparentResistivitiesErrorNorm;
        else :                    
            self.apparentResistivitiesErrorNorm = self.apparentResistivitiesErrorNorm = np.log10(np.divide(np.add(self.apparentResistivities,self.apparentResistivitiesError),self.apparentResistivities));
            return self.apparentResistivitiesErrorNorm;
    
    def getPhaseRelativePercentageError(self):
        if len(self.phasesErrorNorm) != 0 :
            return self.phasesErrorNorm;
        else :
            self.phasesErrorNorm = self.phasesError;
            return self.phasesErrorNorm;
    
    def computeRelativeApparentResistivityError(self, data,fieldAResErr):
        #fieldAResErr = self.getApparentResistivityRelativePercentageError();        
        aResRatio = np.log10(np.divide(self.apparentResistivities,data.apparentResistivities));        
        aResRel = np.divide(aResRatio,fieldAResErr);              
        return aResRel;
    def computeRelativePhaseError(self,data, fieldPhaseErr):
        #fieldPhaseErr = self.getPhaseRelativePercentageError();                
        phaseDiff = np.subtract(self.phases, data.phases);        
        phaseRel = np.divide(phaseDiff,fieldPhaseErr);        
        return phaseRel;
    
    def computeErrorArray(self, data,fieldAResErr,fieldPhaseErr):
        aResRel = self.computeRelativeApparentResistivityError(data,fieldAResErr);
        phaseRel = self.computeRelativePhaseError(data,fieldPhaseErr)  
        errorArray =  np.concatenate([aResRel,phaseRel]); #append array
        return errorArray;
    
    def computeQError(self, data,fieldAResErr,fieldPhaseErr):                          
        errorArray = self.computeErrorArray(data,fieldAResErr,fieldPhaseErr);
        #print(errorArray)
        q = math.sqrt(np.inner(errorArray,np.divide(errorArray,self.getNumberOfFrequencies()*2)));
        return q;
    
        
         
    def printData(self):      

        print(len(self.frequencies));  
        for i in range(0,len(self.frequencies)) :
            f = self.frequencies[i];
            ares = self.apparentResistivities[i];
            ares_err = self.apparentResistivitiesError[i];
            ph = self.phases[i];
            ph_err = self.phasesError[i];
            output = "%d : %f Hz  rho=%f Ohm.m (%f)    phase=%f Deg (%f)" % (i, f, ares, ares_err, ph, ph_err);
            print(output);
                
    def plotData(self, filename):
        f, axarr  = plt.subplots(2, sharex=True);
        axarr[0].set_title('Apparent Resistivity (Ohm.m) versus Frequency (Hz)');
        axarr[1].set_title('Phase (Deg) versus Frequency (Hz)');
        axarr[2].set_title('Apparent Resistivity (Ohm.m) versus Depth (M)');
        #Apparent Resistivities
        axarr[0].loglog(self.frequencies, self.apparentResistivities, color='b');
        axarr[0].errorbar(self.frequencies, self.apparentResistivities,self.apparentResistivitiesError, color='b');
        axarr[0].scatter(self.frequencies, self.apparentResistivities,s=3, color='b');
        #Phases
        axarr[1].semilogx(self.frequencies, self.phases, color='b');
        axarr[1].errorbar(self.frequencies, self.phases,self.phasesError, color='b');        
        axarr[1].scatter(self.frequencies, self.phases,s=3, color='b');  
        #Thicknesses
        axarr[2].plot(it.fieldData.apparentResistivities, it.fieldData.currentDepth, color='g');
                                  
        axarr[0].grid(True,which="both");
        axarr[1].grid(True,which="both");
        axarr[2].grid(True,which="both");
        f.savefig(filename);
def read(dataString):
	    dat = MTData();
	    dataList = [float(x) for x in dataString.split(',')]
	    nEntries = (len(dataList)/5);
	    for i in range(0,nEntries):
	    	dat.frequencies = np.append(dat.frequencies, dataList[0+i*5]);
        	dat.apparentResistivities = np.append(dat.apparentResistivities, dataList[1+i*5]);
        	dat.apparentResistivitiesError = np.append(dat.apparentResistivitiesError, dataList[2+i*5]);
        	dat.phases = np.append(dat.phases,dataList[3+i*5]);
        	dat.phasesError = np.append(dat.phasesError,dataList[4+i*5]);   
	    return dat;
def load(filename):
        f = open(filename, 'r');
        dat = MTData();
        for line in f.readlines():
            line = line.strip().split();
            freq = float(line[0]);
            ares = float(line[1]);
            ares_err = float(line[2]);
            phase = float(line[3]);
            phase_err = float(line[4]);                    
            dat.frequencies = np.append(dat.frequencies, freq);
            dat.apparentResistivities = np.append(dat.apparentResistivities, ares);
            dat.apparentResistivitiesError = np.append(dat.apparentResistivitiesError, ares_err);
            dat.phases = np.append(dat.phases,phase);
            dat.phasesError = np.append(dat.phasesError,phase_err);        
        f.close()
        return dat;
        