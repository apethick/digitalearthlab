import math
import cmath
import time
import sys
import time


start = time.clock();

freqString = sys.argv[1][1:-1];
resString = sys.argv[2][1:-1];
thickString = sys.argv[3][1:-1];
resistivities = map(float,resString.split(','));
thicknesses = map(float,thickString.split(','));
frequencies = map(float,freqString.split(','));

frequenciesOut = [];
aresOut = [];
phaseOut = [];
output = [frequenciesOut,aresOut,phaseOut];
def fwd(frequency, resistivities, thicknesses): #returns the 1D forward response
		mu = 4*math.pi*1E-7; #Magnetic Permeability (H/m)
		n = len(resistivities);
		w =  2*math.pi*frequency;       
		impedances = list(range(n));
		impedances[n-1] = cmath.sqrt(w*mu*resistivities[n-1]*1j);
		for j in range(n-2,-1,-1):
			resistivity = resistivities[j];
			thickness = thicknesses[j];
	  
	        # 3. Compute apparent resistivity from top layer impedance
	        #Step 2. Iterate from bottom layer to top(not the basement) 
	        # Step 2.1 Calculate the intrinsic impedance of current layer
			dj = cmath.sqrt((w * mu * (1.0/resistivity))*1j);
			wj = dj * resistivity;
	        # Step 2.2 Calculate Exponential factor from intrinsic impedance
			ej = cmath.exp(-2*thickness*dj);                     
	    
	        # Step 2.3 Calculate reflection coeficient using current layer
	        #          intrinsic impedance and the below layer impedance
			belowImpedance = impedances[j + 1];
			rj = (wj - belowImpedance)/(wj + belowImpedance);
			re = rj*ej; 
			Zj = wj * ((1 - re)/(1 + re));
			impedances[j] = Zj;    
	
	    # Step 3. Compute apparent resistivity from top layer impedance
		Z = impedances[0];
		absZ = abs(Z);
		apparentResistivity = (absZ * absZ)/(mu * w);
		phase = math.atan2(Z.imag, Z.real);

		frequenciesOut.append(math.log10(frequency));
		aresOut.append(math.log10(apparentResistivity));
		phaseOut.append(phase);



		return [frequency, apparentResistivity, phase];
		
if len(resistivities) == 0:
	print 'ERROR! Must have more one or more resistivities <br>';
elif len(thicknesses) != (len(resistivities) - 1):
	print 'ERROR! nThicknesses must equal nResistivities-1 (NOTE: The halfspace does not have a thickness)<br>';
else: 		
	for frequency in frequencies:   
		fwd(frequency,resistivities,thicknesses);
	print output;
	


